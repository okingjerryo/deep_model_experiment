import est_component as estimator
import tensorflow as tf
import collections  # 定义容器
from args import a
import args
import dehazeExperment.op as op
from tensorflow.python import debug as tf_debug

Model = collections.namedtuple("Model",
                               "op,predict_real,predict_fake,discrim_loss,discrim_grads_and_vars,gen_loss_GAN,gen_loss_L1,gen_grads_and_vars,train")


def stack_CAGAN_main(input_X, input_Y):
    O_1 = estimator.unet_generator(input_X, name="O1_encoder")
    O_1_op = O_1.output_conv
    O_1_D = estimator.vgg19_descriminator(O_1_op, name="O1_discriminator")
    O_2 = estimator.unet_generator(O_1_op, name="O2_encoder")
    O_2_op = O_2.output_conv
    O_2_D = estimator.vgg19_descriminator(O_2_op, name="O2_discriminator")
    return Model(
        predict_real=tf.group(O_1_D.logist, O_2_D.logist),
        op=O_2_D
    )


def cGAN_main(features, labels, mode, params):
    # logging hook

    # vars
    input_X = features["noise_img"]
    input_Y = features["label"]
    # modeling
    with tf.variable_scope("generator"):
        out_channels = int(input_Y.get_shape()[-1])
        outputs = estimator.cgan_unet_g(input_X, out_channels)
        # 预测时，只需要调用生成器生成图片即可

    if mode == tf.estimator.ModeKeys.PREDICT:
        predictions = {
            'gen_image':outputs.output_conv
        }
        return tf.estimator.EstimatorSpec(mode,predictions=predictions)

    outputs_rgb = tf.image.yuv_to_rgb(outputs.output_conv)
    gt_rgb  = tf.image.yuv_to_rgb(input_Y)
    noise_rgb = tf.image.yuv_to_rgb(input_X)
    tf.summary.image("gen_output",outputs_rgb)
    tf.summary.image("ground_truth",gt_rgb)
    tf.summary.image("input_noise",noise_rgb)
    # create two copies of discriminator, one for real pairs and one for fake pairs
    # they share the same underlying variables
    with tf.name_scope("real_discriminator"):
        with tf.variable_scope("discriminator"):
            # 2x [batch, height, width, channels] => [batch, 30, 30, 1]
            predict_real = estimator.cgan_patch_d(input_X, input_Y)

    with tf.name_scope("fake_discriminator"):
        with tf.variable_scope("discriminator", reuse=True):
            # 2x [batch, height, width, channels] => [batch, 30, 30, 1]
            predict_fake = estimator.cgan_patch_d(input_X, outputs.output_conv)

    with tf.name_scope("discriminator_loss"):
        # minimizing -tf.log will try to get inputs to 1
        # predict_real => 1
        # predict_fake => 0
        # l2 regular

        discrim_loss = tf.reduce_mean(-(tf.log(predict_real.output_conv + args.EPS) + tf.log(1 - predict_fake.output_conv + args.EPS)))

    with tf.name_scope("generator_loss"):
        # predict_fake => 1
        # abs(targets - outputs) => 0
        gen_loss_GAN = tf.reduce_mean(-tf.log(predict_fake.output_conv + args.EPS))
        gen_loss_L1 = tf.reduce_mean(tf.abs(input_Y - outputs.output_conv))
        gen_loss = gen_loss_GAN * a.gan_weight + gen_loss_L1 * a.l1_weight
    ema = tf.train.ExponentialMovingAverage(decay=0.99)
    # psnr = op.calculate_psnr_tensor(input_Y,outputs.output_conv)
    with tf.name_scope("rgb_result"):
        out_rgb = tf.image.yuv_to_rgb(outputs.output_conv)
        org_rgb = tf.image.yuv_to_rgb(input_Y)
    with tf.name_scope("eval_method"):
        psnr = tf.image.psnr(input_Y,outputs.output_conv,100)
        psnr_avg = tf.reduce_mean(psnr)
        x_noise_psnr = tf.image.psnr(input_Y,input_X,100)
        noise_psnr = tf.reduce_mean(x_noise_psnr)
        out_rgb_psnr = tf.reduce_mean(tf.image.psnr(org_rgb,out_rgb,100))
        # race psnr
        race_psnr = op.calculate_psnr_tensor(input_Y,outputs.output_conv)
        tf.summary.scalar("psnr",psnr_avg)
        tf.summary.scalar("psnr_noise",noise_psnr)
        tf.summary.scalar("psnr_outrgb",out_rgb_psnr)
        tf.summary.scalar("psnr_race",race_psnr)

    loss = {
        'discrim_loss': tf.reduce_mean(discrim_loss),
        'gen_loss_GAN': tf.reduce_mean(gen_loss_GAN),
        'gen_loss_L1': tf.reduce_mean(gen_loss_L1),
        'psnr':psnr,
        'psnr_noise':noise_psnr,
        'out_rgb_psnr':out_rgb_psnr,
        'race_psnr':race_psnr
    }

    # hook = tf_debug.TensorBoardDebugHook("127.0.0.1:36064")
    with tf.name_scope("hooks"):
        pass
        # profile_hook = tf.train.ProfilerHook(save_steps=1,show_memory=True)
    if mode == tf.estimator.ModeKeys.EVAL:
        return tf.estimator.EstimatorSpec(
            mode,
            # training_hooks=[hook],
            eval_metric_ops=loss,
            loss = tf.reduce_mean(gen_loss),
        )
    # logging
    with tf.name_scope('losses'):
        tf.summary.scalar("D_Loss",discrim_loss)
        tf.summary.scalar("G_Loss_adv",gen_loss_GAN)
        tf.summary.scalar("G_loss_L1",gen_loss_L1)
        tf.summary.scalar("G_Loss_Total",gen_loss)


    assert mode == tf.estimator.ModeKeys.TRAIN

    with tf.name_scope("discriminator_train"):
        discrim_tvars = [var for var in tf.trainable_variables() if var.name.startswith("discriminator")]
        discrim_optim = tf.train.AdamOptimizer(a.d_lr, a.beta1)
        discrim_grads_and_vars = discrim_optim.compute_gradients(discrim_loss, var_list=discrim_tvars)
        discrim_train = discrim_optim.apply_gradients(discrim_grads_and_vars)

    with tf.name_scope("generator_train"):
        with tf.control_dependencies([discrim_train]):
            gen_tvars = [var for var in tf.trainable_variables() if var.name.startswith("generator")]
            gen_optim = tf.train.AdamOptimizer(a.g_lr,0)    # 关闭动量
            gen_grads_and_vars = gen_optim.compute_gradients(gen_loss, var_list=gen_tvars)
            gen_train = gen_optim.apply_gradients(gen_grads_and_vars)


    update_losses = ema.apply([discrim_loss, gen_loss_GAN, gen_loss_L1])

    # 滑动平均值
    global_step = tf.train.get_or_create_global_step()
    incr_global_step = tf.assign(global_step, global_step + 1)
    train_op = tf.group(update_losses, incr_global_step, gen_train)
    return tf.estimator.EstimatorSpec(
        mode,
        # training_hooks=[hook],
        train_op=train_op,
        loss = gen_loss,
        # training_hooks=[profile_hook]
    )


