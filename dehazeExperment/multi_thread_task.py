import os
from glob import glob
import queue
import threading
from dehazeExperment import dataset
from dehazeExperment import args
task = queue.Queue()
thread_num = 25
exitFlag = 0
qlocker = threading.Lock()


class mission:
    def __init__(self, name, script):
        self.script = script
        self.name = name


class TaskThread(threading.Thread):
    def __init__(self, thread_id):
        threading.Thread.__init__(self)
        self.thread_id = thread_id

    def run(self):
        print("start thread:{0}".format(self.thread_id))
        t_main(self.thread_id)
        print("end  thread:{0}".format(self.thread_id))


def t_main(thread_id):
    while not exitFlag:
        print("task last {0} elem".format(task.qsize()))
        qlocker.acquire()
        if not task.empty():
            miss = task.get()
            qlocker.release()
            filename = miss.name
            script = miss.script
            print("processing file: {0}".format(filename))
            os.system(script)

        else:
            qlocker.release()


def prepare_task():

    filelist = glob(args.NOISE_TRAIN_PATH)
    print("constract task: num {0}".format(len(filelist)))
    for path in filelist:
        thispath = os.path.normpath(path)
        thisfile = os.path.basename(thispath)
        thisfilename = thisfile.split(".")[0]
        # script = ('./TPGEncTest -i '+path+' -yuv n'+thisfilename+'.yuv -o '+thisfilename+'.yuv'+' -level 4'#-o '+thisfilename+'.yuv
        script = ('./TPGDecTest -i ' + path + ' -yuv ' + thisfilename + '.yuv')
        # ' -qp0 38')
        miss = mission(thisfile, script)
        task.put(miss)


if __name__ == '__main__':
    threads = []
    qlocker.acquire()
    prepare_task()
    qlocker.release()
    try:
        # 组建线程
        for i in range(thread_num):
            thread = TaskThread(i)
            thread.start()
            threads.append(thread)

        while not task.empty():
            pass

        exitFlag = 1

    except KeyboardInterrupt:
        exitFlag = 1
        raise
    finally:
        # 回收
        for t in threads:
            t.join()
        print("all clear")