import dataset
import args
import model
import tensorflow as tf



# 主函数中调用estimator的接口
def create_estimator():
    checkpointing_config = tf.estimator.RunConfig(
        save_checkpoints_secs=10 * 60,  # 10min save
        keep_checkpoint_max=3, # 存30min内的 占用2G附近
        save_summary_steps=10,
        log_step_count_steps=50
    )
    return tf.estimator.Estimator(
        model_fn=model.cGAN_main,
        model_dir="model/",
        config=checkpointing_config
    )

    # 读数据要放在main里面
def train_input_fn():
    data_op = dataset._init_handle(dataset_type=dataset.TRAIN_DATASET_TYPE)
    return data_op['data_set']

def eval_input_fn():
    data_op = dataset._init_handle(dataset_type=dataset.VAILD_DATASET_TYPE)
    return data_op['data_set']

def test_input_fn():
    data_op = dataset._init_handle(dataset_type=dataset.TEST_DATASET_TYPE)
    return data_op['data_set']

def main(argv):
    # 建立features 对应关系
    # feature_columns = dataset.get_feature_columns()
    # 建立模型函数
    cgan_estimator = create_estimator()
    # epoch
    # 训练模型
    with tf.Session(config=args.gpu_option()) as sess:
        for i in range(139):
            print('epoch {}'.format(i))
            cgan_estimator.train(
                input_fn=train_input_fn,steps=args.TRAIN_STEPS)
    cgan_estimator.predict(input_fn=test_input_fn)

if __name__ == '__main__':
    tf.logging.set_verbosity(tf.logging.INFO)
    tf.app.run(main)

