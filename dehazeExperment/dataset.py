import tensorflow as tf
from os import path
import op
import cv2
import args
from glob import glob
from tqdm import tqdm
from os import path
from args import a
import numpy as np
import struct

# 用于识别不同的操作图片集
TRAIN_DATASET_TYPE = 0
VAILD_DATASET_TYPE = 1
TEST_DATASET_TYPE = 2
# 可以feed的dataset handle
DATA_HANDEL = tf.placeholder(dtype=tf.string, shape=[], name='dataset_handle')
# IMG_LIST = tf.placeholder(dtype=tf.string, shape=[None], name='input_noise_image_list')
# record feature format
record_feature = {

}
main_data_type = None
# write record placeholder
NOISE_RAW = tf.placeholder(dtype=tf.string,shape=[])
GT_RAW = tf.placeholder(dtype=tf.string,shape=[])
WIDTH = tf.placeholder(dtype=tf.int64,shape=[])
HEIGHT = tf.placeholder(dtype=tf.int64,shape=[])

def _img_effect_process(noise_img, gt_img):
    # 归一化
    noise_img = op.max_min(noise_img)
    gt_img = op.max_min(gt_img)
    return noise_img,gt_img



def _get_tfrecord_structure():
    """
        定义tfrecord 以及trainning set features的结构
    :return: features 给tf.parse_single_example用
    """
    return {
        'noise_image': tf.FixedLenFeature((), tf.string, default_value=''),
        'gt_image': tf.FixedLenFeature((), tf.string, default_value=''),
        'image_w':tf.FixedLenFeature((),tf.int64),
        'image_h':tf.FixedLenFeature((),tf.int64)
    }


def _read_imgpair_from_noisepath(noisepath):
    """
        图像读取接口
    :param noisepath: noise文件对应的路径
    :return: noise 与 gt的图像对
    """

    filepath_os = path.normpath(noisepath.split(".")[0])
    file_main_name = str(path.basename(filepath_os)).split("_")[0]  # 只取前面主文件名
    gt_path = path.abspath(path.join(filepath_os, "../../../GT"))
    gt_img_path = str(gt_path) + '/' + file_main_name + ".yuv"

    # 读取gt图片
    gt_img = read_yuv_in_training_size(gt_img_path)
    gt_img_h = np.shape(gt_img)[0]
    gt_img_w = np.shape(gt_img)[1]
    noise_img = op.readYUVFile(noisepath, width=gt_img_w, height=gt_img_h)
    return noise_img, gt_img,gt_img_h,gt_img_w


def _read_img_process(img_example,data_type,img_list_gt = None):
    features = None
    # 注意注意！ features中的key value对一定要与tfrecord中的k-v相关对应！
    noise_img = None
    gt_img = None
    if data_type is 'image_list':
        # 从noise的路径读取两个文件，从noise读取只需要匹配文件名称即可
        noise_img=img_example
        gt_img = img_list_gt
    elif data_type is 'tfrecord':
        features = _get_tfrecord_structure()
        parse_features = tf.parse_single_example(img_example, features)
        noise_img = tf.decode_raw(parse_features['noise_image'],tf.uint8) # 注意核对输入输出的数据格式。图片小心是int8还是uint8
        gt_img = tf.decode_raw(parse_features['gt_image'],tf.uint8)
        noise_img = tf.cast(noise_img,tf.float32)
        gt_img = tf.cast(gt_img,tf.float32)
        img_w = tf.cast(parse_features['image_w'],dtype=tf.int32)   # tfRecord 存储常量用的是int64， 作为reshape参数时不支持
        img_h = tf.cast(parse_features['image_h'],dtype=tf.int32)
        noise_img = tf.reshape(noise_img,[img_h,img_w,3])
        gt_img = tf.reshape(gt_img,[img_h,img_w,3])
    # 使得两个图片的分割区域一致
    noise_img,gt_img = _img_effect_process(noise_img,gt_img)
    # 这里关联着inputFun
    return {'noise_img':noise_img,
            'label':gt_img}


# def _read_trainimg_to_string():
#
#     # 使用placeholder 防止内存溢出
#
#     return {
#         'noise_img': t_noise_str,
#         'gt_img': t_gt_str,
#         'image_w': t_w,
#         'image_h':t_h
#     }


def _bytes_feature(bytes):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[bytes]))
def _int64_feature(nums):
    return tf.train.Feature(int64_list = tf.train.Int64List(value = [nums]))




def write_img_record(filelist, dataset_type):
    print('file convert start')
    print('detect', len(filelist), 'files')
    record_path = None
    record_name = None
    if dataset_type == TRAIN_DATASET_TYPE:
        record_path = args.PATH2RECORD +'/'+ args.RECORD_TRAIN_NAME+'.tfrecord'
        record_name = args.RECORD_TRAIN_NAME
    elif dataset_type == TEST_DATASET_TYPE:
        record_path = args.PATH2RECORD +'/'+ a.record_test_name+'.tfrecord'
        record_name = a.record_test_name
    elif dataset_type == VAILD_DATASET_TYPE:
        record_path = args.PATH2RECORD +'/'+a.record_vaild_name+'.tfrecord'
        record_name = a.record_vaild_name
    writer = tf.python_io.TFRecordWriter(record_path)

    with tf.Session(config=args.gpu_option()) as sess:
        # 从文件中分别将几个图片读出来，格式即使Test也存gt（只是实际用不用的问题）
        counter = 1
        for thisfile in tqdm(filelist, ascii=True, desc='write img to tfrecord'):
            if counter % 200 is 0:
                writer.close()
                t_path = args.PATH2RECORD +'/'+ record_name +str(counter//200)+'.tfrecord'
                writer = tf.python_io.TFRecordWriter(t_path)
            t_noise, t_gt, t_h, t_w = _read_imgpair_from_noisepath(noisepath=thisfile)
            # random crop to small pieces with 30000
            for i in range(30):
                t_noise_crop,t_gt_crop = op.random_crop(t_noise,t_gt,args.IMG_CROP_SIZE[0])
                # t_noise_str = tf.compat.as_bytes(t_noise_crop.tostring(),encoding='iso-8599-1')
                # t_gt_str = tf.compat.as_bytes(t_gt_crop.tostring(),encoding='iso-8599-1')
                example = tf.train.Example(features=tf.train.Features(feature={
                    'noise_image': _bytes_feature(t_noise_crop.tostring()),
                    'gt_image': _bytes_feature(t_gt_crop.tostring()),
                    'image_w': _int64_feature(args.IMG_CROP_SIZE[0]),
                    'image_h': _int64_feature(args.IMG_CROP_SIZE[0])

                }))
                writer.write(example.SerializeToString())
            counter = counter+1
        writer.close()
    print('save complete,path:', record_path)


def _read_img_list_pipeline(img_path_list):
    noise = []
    gt = []
    h = []
    w = []
    for i_path in img_path_list:
        t_noise, t_gt, t_h, t_w = _read_imgpair_from_noisepath(noisepath=i_path)
        t_noise = tf.convert_to_tensor(t_noise, dtype=tf.int32)
        t_gt = tf.convert_to_tensor(t_gt, dtype=tf.int32)
        noise.append(t_noise)
        gt.append(t_gt)
        h.append(t_h)
        w.append(t_w)
    min_h = np.array(h).min()
    min_w = np.array(w).min()
    # 把图片切成list中最小的
    for i in range(len(noise)):
        noise[i] = tf.random_crop(noise[i], size=[min_h, min_w, 3])
        gt[i] = tf.random_crop(gt[i], size=[min_h, min_w, 3])
    noise = tf.stack(noise)
    gt = tf.stack(gt)
    return noise,gt


def _init_handle(dataset_type,record_path=None, data_type=None,img_path_list = None,get_itor = None):
    '''
        初始化DataSet Handle
    :return:
    '''
    # 默认参数
    if get_itor is None:
        get_itor = False
    if data_type is None:
        data_type = a.input_data_type
    if img_path_list is None:
        if dataset_type is TRAIN_DATASET_TYPE:
            img_path_list = glob(args.NOISE_TRAIN_PATH)
            if record_path is None:
                record_path =tf.data.Dataset.list_files(args.PATH2RECORD + '/' + args.RECORD_TRAIN_NAME+'*.tfrecord')
        elif dataset_type is VAILD_DATASET_TYPE:
            img_path_list = glob(args.NOISE_VAILD_PATH)
            if record_path is None:
                record_path = tf.data.Dataset.list_files(args.PATH2RECORD + '/' + args.RECORD_VAILD_NAME+'*.tfrecord')
        elif dataset_type is TEST_DATASET_TYPE:
            img_path_list = glob(args.NOISE_TEST_PATH)
            if record_path is None:
                record_path = tf.data.Dataset.list_files(args.PATH2RECORD + '/' + args.RECORD_TEST_NAME+'*.tfrecord')

    train_dataset = None
    main_data_type = data_type # 总数据类型注入
    if main_data_type is 'image_list':
        noise,gt = _read_img_list_pipeline(img_path_list=img_path_list)
        train_dataset = tf.data.Dataset.from_tensor_slices((noise,gt))
        train_dataset = train_dataset.map(
            lambda x,y: _read_img_process(x,main_data_type,img_list_gt=y)
        )
    elif main_data_type is 'tfrecord':
        train_dataset = record_path.interleave(tf.data.TFRecordDataset,cycle_length=args.NUM_PARALLEL_READERS)
        #record_path.apply(tf.contrib.data.parallel_interleave(tf.data.TFRecordDataset,cycle_length=args.NUM_PARALLEL_READERS))
        train_dataset = train_dataset.map(
            map_func=lambda x: _read_img_process(x,main_data_type),
            num_parallel_calls=args.NUM_PARALLEL_CALLS
        )
    train_dataset = train_dataset.shuffle(buffer_size=1000)
    train_dataset = train_dataset.repeat()  # 组成一个epoch
    train_dataset = train_dataset.batch(args.TRAIN_BATCH)

    # 优化 预处理pipeline
    train_dataset = train_dataset.prefetch(buffer_size=args.NUM_PREFATCH)

    if get_itor is False:
        return {
            'data_set':train_dataset
        }
    #init handle
    iterator = tf.data.Iterator.from_string_handle(DATA_HANDEL, train_dataset.output_types,
                                                train_dataset.output_shapes)
    get_next_batch = iterator.get_next()

    train_itor = train_dataset.make_initializable_iterator()
    return {
        'train_itor': train_itor,
        'get_next_batch': get_next_batch,
        'data_set':train_dataset
    }



def read_yuv_in_training_size(filepath):
    filepath_os = path.normpath(filepath.split(".")[0])
    file_main_name = path.basename(filepath_os)  # 主文件名
    org_path = path.abspath(path.join(filepath_os, "../../../train_img"))
    org_img_path = str(org_path) + '/' + file_main_name + ".jpg"
    img = cv2.imread(org_img_path)
    img_h = np.shape(img)[0]
    img_w = np.shape(img)[1]
    return op.readYUVFile(filepath, width=img_w, height=img_h)


def vaildate_data_useable(noise_img_list, input_type=None, action_type=None):
    '''
    获取一个batch数据，并取出头第一对图片。看其是否有效
    :return: null
    '''
    # 默认参数区
    if input_type is None:
        input_type = a.input_data_type
    if action_type is None:
        action_type = TRAIN_DATASET_TYPE

    data_op = _init_handle(data_type=input_type,dataset_type=action_type,img_path_list=noise_img_list,get_itor=True)

    get_batch = data_op['get_next_batch']
    train_itor = data_op['train_itor']

    with tf.Session(config=args.gpu_option()) as sess:
        print('vailding data...')
        data_handle = sess.run(train_itor.string_handle())

        for i in range(2):
            train_feed = {DATA_HANDEL: data_handle}
            sess.run(train_itor.initializer, feed_dict=train_feed)
            batch_feature = sess.run(get_batch, feed_dict=train_feed)
            print('noise batch', i, 'shape', batch_feature['noise_img'][0].shape)
            print('noise batch', i, 'data', batch_feature['noise_img'][0])
            print('noise batch', i, 'shape', batch_feature['label'][0].shape)
            print('noise batch', i, 'data', batch_feature['label'][0])

# brige feature to estimator
def get_feature_columns():
    feature_columns = [tf.feature_column.numeric_column(key="noise_img"),
                       tf.feature_column.numeric_column(key="label")]
    return feature_columns


if __name__ == '__main__':
    # list = glob(args.NOISE_VAILD_PATH)
    # write_img_record(list,VAILD_DATASET_TYPE)
    # list = glob(args.NOISE_TEST_PATH)
    # write_img_record(list,TEST_DATASET_TYPE)
    vaildate_data_useable(None,'tfrecord')
