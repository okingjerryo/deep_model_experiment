import torch
from scipy.stats import pearsonr
from torch.nn import functional as F


def PLCC(pred, label_score, add_softmax=False, input_logist=True):
    '''
    带batch size 的的PLCC计算
    :param pred:
    :param label:
    :param add_softmax:
    :return:
    '''
    loss = pred
    if add_softmax:
        loss = F.softmax(pred, dim=1)
    # score = 1 * loss[0] + 2 * loss[1] + 3 * loss[2] + 4 * loss[3] + 5 * loss[4] + 6 * loss[5] + 7 * \
    #         loss[6] + 8 * loss[7] + 9 * loss[8] + 10 * loss[9]
    if input_logist:
        pre_score = torch.sum(torch.arange(1, 11).float() * loss)
    else:
        pre_score = loss
    pre_item = pre_score.detach().numpy()
    lab_item = label_score.detach().numpy()
    r, p = pearsonr(pre_item, lab_item)  # r 为相关系数（-1，1） p为指标值 越小越好
    return r, p



def score_to_2class_with_threshold(score, threshold=6):
    result = torch.gt(score, threshold)
    return result


def nclass_pred_to_2class(pred, threshold=6):
    false_pred = torch.sum(pred[:, :threshold - 1], dim=1)
    true_pred = torch.sum(pred[:, threshold - 1:], dim=1)
    result = torch.zeros(pred.size(0), 2)
    result[:, 0] = false_pred
    result[:, 1] = true_pred
    return result
