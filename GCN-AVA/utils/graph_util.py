import torch


def convert_adj_to_edge_index(adj, threshold=0.5, first_dim=0):
    '''
    将邻接矩阵，可能存在feature。分离或重组为edge_index 矩阵
    edge_index 矩阵格式[1,2](node 0->node 3)
    :param adj: torch 邻接阵 shape n*n
    :param first_dim: int todo 邻接矩阵开始的维度（预防batchsize 为第一维度的情况）
    :return: torch edge_index 格式的矩阵 shape 2*m,m为连接数
    '''

    assert adj.size(0) == adj.size(1)
    if len(adj.size()) == 3:
        index_adj = adj[:, :, 0]  # 取第一维度的切片 为相连关系
    else:
        index_adj = adj
    index_adj = torch.squeeze(index_adj)  # 压缩维度为1的
    shape = list(index_adj.size())
    row, col = shape[0], shape[1]
    assert len(shape) is 2  # 目标adj阵的维度应该只为2
    # 将矩阵化为一维阵
    seq_mat = index_adj.view(1, row * col)
    # 计算所有元素个数生成mask
    ind_last = seq_mat.size(1)
    # 对邻接阵序列中所有等于1的做mask
    mask = torch.squeeze(torch.gt(seq_mat, threshold))
    # 得到有邻接关系的下标阵
    id = torch.arange(0, ind_last)[mask]
    # 生成edge_index阵
    source = id // row
    target = id % row
    # 合并
    edge_index_mat = torch.ones(2, len(source))
    edge_index_mat[0] = source
    edge_index_mat[1] = target
    # 提前释放多于变量，控制内存开销
    del id, source, target, seq_mat, mask, index_adj
    # 最后化为longTensor
    edge_index_mat = edge_index_mat.long()
    return edge_index_mat
