from structures.bounding_box import BoxList


class AestheticsData(object):
    def __init__(self, id=None, x=None, score=None, score_p=None):
        self.id = id
        self.x = x
        self.score = score
        self.score_p = score_p


class AestheticsFeatureData(AestheticsData):
    def __init__(self, id=None, x=None, score=None, score_p=None, bbox=None):
        super().__init__(id, x, score, score_p)
        if bbox is not None:
            assert isinstance(bbox, BoxList)
        self.bbox = bbox
