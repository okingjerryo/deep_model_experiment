import os.path as osp
from math import ceil

import torch
import torch.nn.functional as F
from torch_geometric.datasets import TUDataset
import torch_geometric.transforms as T
from torch_geometric.data import DenseDataLoader, DataLoader

from configs.default_args import *
from model.GCN.GAT_Diffpool import Diffpool_with_GAT

max_nodes = 100


class MyFilter(object):
    def __call__(self, data):
        return data.num_nodes <= max_nodes


path = osp.join(osp.dirname(osp.realpath(__file__)), '..', 'data', 'ENZYMES_d')
dataset = TUDataset(
        path,
        name='ENZYMES',
        transform=T.ToDense(max_nodes),
        pre_filter=MyFilter())
dataset = dataset.shuffle()
n = (len(dataset) + 9) // 10
test_dataset = dataset[:n]
val_dataset = dataset[n:2 * n]
train_dataset = dataset[2 * n:]
test_loader = DenseDataLoader(test_dataset, batch_size=1, num_workers=8,
                              pin_memory=True)
val_loader = DenseDataLoader(val_dataset, batch_size=1, num_workers=8,
                             pin_memory=True)
train_loader = DenseDataLoader(train_dataset, batch_size=1, num_workers=8, shuffle=True,
                               pin_memory=True)

# (max_num_nodes,input_dim, args.hidden_dim, args.output_dim, args.num_classes, args.num_gc_layers,
# args.hidden_dim, assign_ratio = args.assign_ratio, num_pooling = args.num_pool,
# bn = args.bn, dropout = args.dropout, linkpred = args.linkpred, args = args,
# assign_input_dim = assign_input_dim)
model = Diffpool_with_GAT(max_nodes, input_dim=3, hidden_dim=20, embedding_dim=20, label_dim=6,
                          num_layers=3
                          , assign_hidden_dim=20, assign_ratio=0.1, num_pooling=1, bn=True,
                          )

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(model)
model.set_dev(device)

optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, model.parameters()),
                             weight_decay=1e-4, lr=0.001)


def train(epoch):
    model.set_mode('train')
    loss_all = torch.FloatTensor().to(device)
    now_i = 0
    correct = 0
    nan_id = []
    nan_num = 0
    update_loss = torch.FloatTensor().to(device)
    print_loss = torch.FloatTensor().to(device)
    optimizer.zero_grad()
    for data in train_loader:
        now_i += 1
        data.to(device)
        y = data.y[0]
        x = data.x[0]
        node_num = x.size(0)
        # 创建一个全连接邻接图
        adj = data.adj[0]
        if adj.size(0) is 0:
            print("warn! adj size 0!")
        # 对data的处理

        logist, reg_loss = model.forward(x, adj)  # 输入

        pred = logist.max(dim=0)[1]  # 输入pred = pred.max(dim=0)[1]  # 输入
        correct += pred.eq(y.view(-1)).sum().item()
        # loss = model.loss(logist.view(1, 10), y.view(-1), adj)
        pred_loss = model.calc_pred_loss(logist.view(1, 6), y.view(-1))
        loss = pred_loss + reg_loss
        # 等待运算的结束
        update_loss = torch.cat((update_loss, loss.view(1)), 0)  # 将结果concat起来

        if now_i % ACCUMULATION_STEP is 0:
            torch.cuda.synchronize()
            mask = update_loss != update_loss  # 获得nan的数量
            nan_num += torch.sum(mask)
            update_loss[mask] = 0
            avg_loss = torch.mean(update_loss)
            loss_all = torch.cat((loss_all, update_loss), 0)
            print_loss = torch.cat((print_loss, avg_loss.view(1)), 0)
            avg_loss.backward()
            # loss.backward()
            torch.nn.utils.clip_grad_norm(model.parameters(), GRADIENT_TRUNCATION)  # 梯度截断
            optimizer.step()
            optimizer.zero_grad()
            update_loss = torch.FloatTensor().to(device)

        if now_i % 100 is 0:
            this_acc = 0
            if now_i != 0:
                this_acc = correct / now_i
            print(
                    "\nepoch{:03d} - training : ({:03d}/{}) loss:{:.4f} nan_percent {:.2f}%  epoch now train acc:{:.4f}".format(
                            epoch, now_i,
                            len(train_loader), torch.mean(print_loss), nan_num * 100 / PRINT_PREIOD,
                            this_acc))
            print_loss = torch.FloatTensor().to(device)
            nan_num = 0

    t_correct = correct / len(train_dataset)
    return torch.sum(loss_all) / len(train_dataset), t_correct


def test(loader):
    model.set_mode('eval')
    correct = 0

    for data in loader:
        data = data.to(device)
        pred, _ = model(data.x[0], data.adj[0], data.mask[0])
        pred = pred.max(dim=0)[1]  # 输入
        correct += pred.eq(data.y.view(-1)).sum().item()
    return correct / len(loader.dataset)


best_test_epoch = best_test = best_val_acc = test_acc = 0
for epoch in range(1, 2001):
    train_loss, t_correct = train(epoch)
    val_acc = test(val_loader)
    if val_acc > best_val_acc:
        best_val_acc = val_acc
    test_acc = test(test_loader)
    if test_acc > best_test:
        best_test = test_acc
        best_test_epoch = epoch
    print('Epoch: {:03d}, Train Loss: {:.4f}, Tra Acc: {:.4f} '
          'Val Acc: {:.4f}, Test Acc: {:.4f}, Best Test:{:.4f} best epoch{:03d}'.format(epoch,
                                                                                        train_loss,
                                                                                        t_correct,
                                                                                        val_acc,
                                                                                        test_acc,
                                                                                        best_test,
                                                                                        best_test_epoch))

# dataset = 'Cora'
# path = osp.join(osp.dirname(osp.realpath(__file__)), '..', 'data', dataset)
# dataset = Planetoid(path, dataset, T.NormalizeFeatures())
# data = dataset[0]
#
# device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
# model = GB.GATNetWork(dataset.num_features, 500, 64, num_layers=3, add_self=True, dropout=0.5,
#                       normalize=True)
# pred_model = GB.GATBlock(1064, dataset.num_classes, add_self=True, heads=1, dropout=0.5).to(device)
# model.set_dev(device)
#
# optimizer = torch.optim.Adamax(model.parameters(), lr=0.0001, weight_decay=1e-5)
# optimizer2 = torch.optim.Adamax(pred_model.parameters(), lr=0.0001, weight_decay=1e-5)
# data = data.to(device)
