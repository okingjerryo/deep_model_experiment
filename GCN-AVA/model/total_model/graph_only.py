import itertools

import torch

from configs.default_args import BATCH_SIZE, GRADIENT_TRUNCATION
from utils.image_evaluator import score_to_2class_with_threshold, PLCC
from ..GCN.GAT_Diffpool import Diffpool_with_GAT
from ..multi_task_cnn.liner_pred import liner_pred_with_hidden
from model.loss.emd_loss import EMDLoss


class GAT_Diffpool_multi_pred(object):
    def __init__(self,
                 max_num_nodes, input_dim, hidden_dim, embedding_dim, label_dim, num_layers,
                 assign_hidden_dim, assign_ratio=0.25,
                 assign_num_layers=-1, num_pooling=3,
                 pred_hidden_dims=[50], concat=True, bn=True, dropout=0.0, linkpred=True,
                 assign_input_dim=-1, args=None, lr=0.001, device=torch.device('cuda')
                 ):
        # 超参
        ## 通用
        self.device = device
        self.input_dim = input_dim
        ## GCN
        self.max_num_nodes = max_num_nodes
        self.gcn_hidden_dim = hidden_dim
        self.gcn_embedding_dim = embedding_dim
        self.gcn_num_layers = num_layers
        self.gcn_assign_hidden_dim = assign_hidden_dim
        self.assign_ratio = assign_ratio
        self.assign_num_layers = assign_num_layers
        self.gcn_num_pooling = num_pooling
        self.gcn_concat = concat
        self.gcn_bn = bn,
        self.gcn_dropout = dropout
        self.linkpred = linkpred
        self.gcn_assign_input_dim = assign_hidden_dim
        ## multi pred
        self.pred_hidden_dims = pred_hidden_dims
        self.num_aggs = 1
        # 结构部分
        ## GCN 结构
        self.GCN = Diffpool_with_GAT(self.max_num_nodes, self.input_dim, self.gcn_hidden_dim,
                                     self.gcn_embedding_dim, self.gcn_num_layers,
                                     self.gcn_assign_hidden_dim, assign_ratio=self.assign_ratio,
                                     assign_num_layers=self.assign_num_layers,
                                     num_pooling=self.gcn_num_pooling,
                                     pred_hidden_dims=self.pred_hidden_dims, concat=self.gcn_concat,
                                     bn=self.gcn_bn, dropout=self.gcn_dropout,
                                     linkpred=self.linkpred,
                                     assign_input_dim=-1, args=None)

        ## 预测结构
        self.class_2_pred = liner_pred_with_hidden(self.GCN.pred_input_dim * (num_pooling + 1),
                                                   pred_hidden_dims,
                                                   2, num_aggs=self.num_aggs)

        self.score_p_pred = liner_pred_with_hidden(self.GCN.pred_input_dim * (num_pooling + 1),
                                                   pred_hidden_dims,
                                                   10, num_aggs=self.num_aggs)

        self.score_pred = liner_pred_with_hidden(self.GCN.pred_input_dim * (num_pooling + 1),
                                                 pred_hidden_dims,
                                                 1, num_aggs=self.num_aggs, use_softmax=False)
        # loss
        self.class_2_loss = torch.nn.CrossEntropyLoss()
        self.emd_loss = EMDLoss()
        self.score_loss = torch.nn.MSELoss()
        self.opti_para = itertools.chain(self.GCN.parameters(), self.class_2_pred.parameters(),
                                         self.score_p_pred.parameters(),
                                         self.score_pred.parameters())
        # optimizer
        self.optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, self.opti_para), lr=lr)

        self.set_device(device)

    # 设置参数设备
    def set_device(self, device):
        self.GCN.set_dev(device)
        self.class_2_pred.to(device)
        self.score_p_pred.to(device)
        self.score_pred.to(device)

    def set_mode(self, mode='train'):
        self.GCN.set_mode(mode)

    def pred_process(self, x):
        self.optimizer.zero_grad()
        node_num = x.size(0)
        # 创建一个全连接邻接图
        adj = torch.full((node_num, node_num), 1., requires_grad=False).to(self.device)
        # 对data的处理

        gcn_feature, reg = self.GCN.forward(x, adj)  # 输入

        pred_2_class = self.class_2_pred(gcn_feature)
        pred_score_p = self.score_p_pred(gcn_feature)
        pred_score = self.score_pred(gcn_feature)
        return pred_2_class, pred_score, pred_score_p, reg

    def loss_calc(self, score, score_p, pred_2_class, pred_score, pred_score_p, reg_loss):
        # 二分类预测
        # 将6分以下作为false可能性 6分以上做true onehot
        # 二分类的门限值为 5.5
        # 将分数转为2分类
        y_2class_label = score_to_2class_with_threshold(score, threshold=5.5)
        # 算预测部分的label 2分类当前

        # 将score 转onehot ： 这里这里用 5.5做门限

        # 2分类loss
        two_class_loss = self.class_2_loss(pred_2_class.view(BATCH_SIZE, -1),
                                           y_2class_label.long().view(
                                               -1).detach())  # 需要注意 pred class 的shape
        # EDM loss 这里面要预先读出10种分数的分布
        emdloss = self.emd_loss(p_target=pred_score_p.view(BATCH_SIZE, -1),
                                p_estimate=score_p.view(BATCH_SIZE, -1))
        # score MSE loss
        mseloss = self.score_loss(pred_score, score.float())
        # total loss
        total_loss = two_class_loss + emdloss + reg_loss + mseloss
        return two_class_loss.item(), emdloss.item(), reg_loss.item(), mseloss.item(), total_loss

    def eval_quota(self, score, score_p, pred_2_class, pred_score_p):
        y_2class_label = score_to_2class_with_threshold(score, threshold=5.5)
        pred_lable = torch.argmax(pred_2_class).byte()
        accuency = pred_lable.eq(y_2class_label).sum().item() / BATCH_SIZE
        plcc_r, plcc_p = PLCC(pred_score_p.cpu(), score_p.view(-1).detach().cpu(),
                              input_logist=False)
        return accuency, plcc_p  # plcc_p 是个numpy矩阵

    def train_process(self, total_loss):
        total_loss.backward()
        torch.nn.utils.clip_grad_norm(self.opti_para, GRADIENT_TRUNCATION)  # 梯度截断
        self.optimizer.step()

    def print_model(self):
        print("GCN:")
        print(self.GCN)
        print("Pred-2 class")
        print(self.class_2_pred)
        print("Pred-10 score p")
        print(self.score_p_pred)
        print("Pred-score")
        print(self.score_pred)

    def get_paradic(self):
        return {
            'gcn'         : self.GCN.state_dict(),
            'class_2_pred': self.class_2_pred.state_dict(),
            'score_p_pred': self.score_p_pred.state_dict(),
            'score_pred'  : self.score_pred.state_dict()
        }
