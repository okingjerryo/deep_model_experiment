import torch


def liner_pred_with_hidden(pred_input_dim, pred_hidden_dims, label_dim, act=torch.nn.ReLU(),
                           num_aggs=1, use_softmax=True):
    pred_input_dim = pred_input_dim * num_aggs
    if len(pred_hidden_dims) == 0:
        pred_model = torch.nn.Linear(pred_input_dim, label_dim)
    else:
        pred_layers = []
        for pred_dim in pred_hidden_dims:
            pred_layers.append(torch.nn.Linear(pred_input_dim, pred_dim))
            pred_layers.append(act)
            pred_input_dim = pred_dim
        pred_layers.append(torch.nn.Linear(pred_dim, label_dim))
        if use_softmax:
            pred_layers.append(torch.nn.Softmax())
        pred_model = torch.nn.Sequential(*pred_layers)

    return pred_model
