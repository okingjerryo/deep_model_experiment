import torch
import torch.nn.functional as F

from model.multi_task_cnn.liner_pred import liner_pred_with_hidden
from ..Blocks import GraphBlock as GB
from torch_geometric.nn import dense_diff_pool
import numpy as np
from utils.graph_util import convert_adj_to_edge_index
max_nodes = 100


class Diffpool_with_GAT(torch.nn.Module):

    def __init__(self, max_num_nodes, input_dim, hidden_dim, embedding_dim, num_layers,
                 assign_hidden_dim, assign_ratio=0.25,
                 assign_num_layers=-1, num_pooling=3,
                 pred_hidden_dims=[50], concat=True, bn=True, dropout=0.0, linkpred=True,
                 assign_input_dim=-1, label_dim=None, args=None):
        super(Diffpool_with_GAT, self).__init__()

        ## 超参数
        add_self = not concat
        self.num_pooling = num_pooling
        self.linkpred = linkpred
        self.assign_ent = True
        self.num_aggs = 1
        self.label_dim = label_dim
        self.act = torch.nn.ReLU()
        self.concat = concat
        ## loss
        self.pred_loss = None
        self.link_loss = None
        self.assign_tensor = None
        # GC 用于loss
        self.conv_after_pool = []
        self.conv_assign = []
        self.assign_pred = []
        # assignment
        assign_dims = []
        if assign_num_layers == -1:
            assign_num_layers = num_layers
        if assign_input_dim == -1:
            assign_input_dim = input_dim

        assign_dim = int(max_num_nodes * assign_ratio)
        # start embeding
        self.s_embeding = GB.GATNetWork(input_dim, hidden_dim, embedding_dim,
                                        num_layers,
                                        add_self, normalize=bn, dropout=dropout)
        for i in range(num_pooling):
            if concat:
                self.pred_input_dim = hidden_dim * (num_layers - 1) + embedding_dim
            else:
                self.pred_input_dim = embedding_dim

            # use self to register the modules in self.modules()
            # gcn after pool
            self.gcn_after_pool = GB.GATNetWork(self.pred_input_dim, hidden_dim, embedding_dim,
                                                num_layers,
                                                add_self, normalize=bn, dropout=dropout)
            self.conv_after_pool.append(self.gcn_after_pool)
            # assignment
            assign_dims.append(assign_dim)
            # 第二层的input 就需要另算而不是直接使用input dim 了
            if i > 0:
                assign_input_dim = self.pred_input_dim * i

            # diff s-gcn
            self.gcn_assign = GB.GATNetWork(assign_input_dim, assign_hidden_dim, assign_dim,
                                            assign_num_layers, add_self,
                                            normalize=bn)

            assign_pred_input_dim = assign_hidden_dim * (
                    num_layers - 1) + assign_dim if concat else assign_dim
            # gcn pred
            self.assign_pred_layer = liner_pred_with_hidden(assign_pred_input_dim, [], assign_dim,
                                                            num_aggs=1, use_softmax=False)
            # next pooling layer
            assign_input_dim = embedding_dim
            assign_dim = int(assign_dim * assign_ratio)

            self.conv_assign.append(self.gcn_assign)
            self.assign_pred.append(self.assign_pred_layer)




    def set_dev(self, device):
        self.to(device)
        self.s_embeding.set_dev(device)
        for i in range(self.num_pooling):
            self.conv_after_pool[i].set_dev(device)
            self.conv_assign[i].set_dev(device)
            self.assign_pred[i].to(device)
    def set_mode(self, mode='train'):
        if mode is 'train':
            self.train()
            self.s_embeding.set_mode('train')
            for i in range(self.num_pooling):
                self.conv_after_pool[i].set_mode('train')
                self.conv_assign[i].set_mode('train')
        elif mode is 'eval':
            self.eval()
            self.s_embeding.set_mode('eval')
            for i in range(self.num_pooling):
                self.conv_after_pool[i].set_mode('eval')
                self.conv_assign[i].set_mode('eval')

    def forward(self, x, adj, mask=None, batch_num_nodes=None,
                **kwargs):  # fixme batch size 只能为1 出在gat不能用batch计算
        # gcn edge_index generate

        edge_index = convert_adj_to_edge_index(adj)
        edge_index = edge_index.cuda().detach()
        total_reg = 0
        # 对特征做l2范式
        # x = F.normalize(x)
        if 'assign_x' in kwargs:
            x_a = kwargs['assign_x']
        else:
            x_a = x  # 实际的x输入

        # S_mask 给embedding 网络的
        max_num_nodes = adj.size()[0]  # 这里因为只考虑了bsize 为1的情况。所以为0维度
        if batch_num_nodes is not None:
            embedding_mask = self.construct_mask(max_num_nodes, batch_num_nodes)
        elif mask is not None:
            embedding_mask = mask
        else:
            embedding_mask = None
        # 每一层的output

        # # dense_diffpool 的 S tensor
        # embedding_tensor = (x, adj,self.conv_first, self.conv_block, self.conv_last,
        #                                     embedding_mask)

        # 1st layer - embedding gcn forward
        embedding_tensor = self.s_embeding.forward(x, edge_index)

        # out 选择（max 还是sum）
        out_all = []
        out, _ = torch.max(embedding_tensor, dim=0)
        out_all.append(out)  # out all
        if self.num_aggs == 2:
            out = torch.sum(embedding_tensor, dim=0)
            out_all.append(out)

        # 中间层
        for i in range(self.num_pooling):
            ##### GCN - BigBlock
            # mask 引用
            if batch_num_nodes is not None:
                embedding_mask = self.construct_mask(max_num_nodes, batch_num_nodes)

            else:
                embedding_mask = None

            # self.assign_tensor = self.conv_pool[i].forward(x,edge_index)  # embedding mask 每层都变 fixme 60 vs 3
            assign_tensor = self.conv_assign[i].forward(x_a, edge_index)  # embedding mask 每层都变
            assign_tensor = self.assign_pred[i](
                assign_tensor)  # 在diffpooling 中有一个softmax,这里就不加softmax了
            if embedding_mask is not None:
                assign_tensor = assign_tensor * embedding_mask
            ##### pooling #####
            # diff Net 的Reg 是用来计算loss的
            # mask 引用
            # embedding == x , assign tensor == s adj==adj mask==mask
            x, adj, reg = dense_diff_pool(
                embedding_tensor.view(1, embedding_tensor.size(0), embedding_tensor.size(1)),
                adj=adj.view(1, adj.size(0), adj.size(1)),
                s=assign_tensor.view(1, assign_tensor.size(0), assign_tensor.size(1)),
                mask=embedding_mask)  # edge_feature 有可能变为index cat feature
            # linkloss 计算
            total_reg += reg
            x_a = x[0]

            ###########
            # after GCNPooling
            edge_index = convert_adj_to_edge_index(adj[0]).cuda()
            embedding_tensor = self.conv_after_pool[i].forward(x_a, edge_index)

            out, _ = torch.max(embedding_tensor, dim=0)
            out_all.append(out)
            if self.num_aggs == 2:
                out = torch.sum(embedding_tensor, dim=0)
                out_all.append(out)

        if self.concat:
            output = torch.cat(out_all, dim=0)
        else:
            output = out
        return output, total_reg

    def calc_pred_loss(self, pred, label, type='softmax'):
        # softmax + CE
        if type == 'softmax':
            pred_loss = torch.nn.CrossEntropyLoss()(pred, label)
        elif type == 'margin':
            batch_size = 1
            label_onehot = torch.zeros(batch_size, self.label_dim).long().cuda()
            label_onehot.scatter_(1, label.view(-1, 1), 1)
            pred_loss = torch.nn.MultiLabelMarginLoss()(pred, label_onehot)
        return pred_loss

    def construct_mask(self, max_nodes, batch_num_nodes):
        ''' For each num_nodes in batch_num_nodes, the first num_nodes entries of the
        corresponding column are 1's, and the rest are 0's (to be masked out).
        Dimension of mask: [batch_size x max_nodes x 1]
        '''
        # masks
        packed_masks = [torch.ones(batch_num_nodes)]
        batch_size = batch_num_nodes
        out_tensor = torch.zeros(batch_size, max_nodes)
        for i, mask in enumerate(packed_masks):
            out_tensor[i:batch_num_nodes] = mask
        return out_tensor.unsqueeze(1).cuda()
    def _calc_diffpool_loss(self, adj=None, batch_num_nodes=None, adj_hop=1):
        eps = 1e-7
        if self.linkpred:
            max_num_nodes = adj.size()[0]
            pred_adj0 = self.assign_tensor @ torch.transpose(self.assign_tensor, 0, 1)
            tmp = pred_adj0
            pred_adj = pred_adj0
            for adj_pow in range(adj_hop - 1):
                tmp = tmp @ pred_adj0
                pred_adj = pred_adj + tmp
            pred_adj = torch.min(pred_adj, torch.Tensor(1).cuda())
            link_loss = -adj * torch.log(pred_adj + eps) - (1 - adj) * torch.log(
                    1 - pred_adj + eps)
            if batch_num_nodes is None:
                num_entries = max_num_nodes * max_num_nodes * 1  # adj.size()[0]
                # print('Warning: calculating link pred loss without masking')
            else:
                num_entries = np.sum(batch_num_nodes * batch_num_nodes)
                embedding_mask = self.construct_mask(max_num_nodes, batch_num_nodes)
                adj_mask = embedding_mask @ torch.transpose(embedding_mask, 1, 2)
                link_loss[1 - adj_mask.byte()] = 0.0
            total_link = torch.sum(link_loss)
            link_loss = total_link / float(num_entries)
            return link_loss

    def loss(self, pred, label, adj):
        pred_loss = self.calc_pred_loss(pred, label)
        link_loss = self._calc_diffpool_loss(adj)
        t_loss = pred_loss + link_loss
        return t_loss

    # todo : EMloss 计算。
    # todo：plcc 指标计算 放在util中
