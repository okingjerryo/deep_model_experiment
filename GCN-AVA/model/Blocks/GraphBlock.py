import torch
from torch_geometric.nn import GATConv, dense_diff_pool, DenseSAGEConv
import torch.nn.functional as F
import numpy as np


# diff pooling中的图推理单个模块
class GNNConv(torch.nn.Module):
    def __init__(self, input_dim, output_dim, add_self=False, normalize=False,
                 dropout=0.0, activation=None):
        super(GNNConv, self).__init__()
        ## 基本参数
        self.add_self = add_self
        self.dropout = dropout
        if dropout > 0.001:
            self.dropout_layer = torch.nn.Dropout(p=dropout)
        self.normalize_embedding = normalize
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.activation = activation
        ## 网络元件
        # 选择图的结构 GraphGASE
        self.conv = DenseSAGEConv(input_dim, output_dim, self.normalize)

    def forward(self, x, adj, mask=None):
        if self.dropout > 0.001:
            hidden = self.dropout_layer(x)
            hidden = self.conv(hidden, adj, mask, self.add_self)
            if self.activation is not None:
                hidden = self.activation(hidden)
            if self.normalize_embedding:
                hidden = F.normalize(hidden, p=2, dim=2)
            return hidden


class GATBlock(torch.nn.Module):
    def __init__(self, input_dim, output_dim, add_self=False, normalize=False,
                 dropout=0.0, activation=None, use_concat=False, heads=8):
        super(GATBlock, self).__init__()
        ## 基本参数
        self.add_self = add_self
        self.dropout = dropout
        self.normalize_embedding = normalize
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.activation = activation
        ## 网络元件
        # 使用GATConv做GNN原子操作
        self.conv = GATConv(input_dim, output_dim, heads=heads, concat=use_concat, dropout=dropout)

        # 权值初始化
        for m in self.modules():
            if isinstance(m, GATConv):
                m.weight.data = torch.nn.init.xavier_uniform(m.weight.data,
                                                             gain=torch.nn.init.calculate_gain(
                                                                     'relu'))
                if m.bias is not None:
                    m.bias.data = torch.nn.init.constant(m.bias.data, 0.0)
    def forward(self, x, adj, mask=None):
        ##
        # 注意 mask未在gatBlock中使用
        ##
        hidden = self.conv.forward(x, adj)
        if self.activation is not None:
            hidden = self.activation(hidden)
        if self.normalize_embedding:
            hidden = F.normalize(hidden, p=2, dim=1)
        return hidden


class GATNetWork(torch.nn.Module):  # out feature = layer * outdim
    ## 这里不考虑任何的batch size
    def __init__(self, input_dim, hidden_dim, output_dim, num_layers, add_self,
                 normalize=True, dropout=0.0, activation=torch.nn.functional.relu,
                 first_dim_max=False):
        super(GATNetWork, self).__init__()
        self.eggs = 1
        self.first_dim_max = first_dim_max
        self.activation = activation
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.output_dim = output_dim

        self.conv_first = GATBlock(input_dim=input_dim, output_dim=hidden_dim, add_self=add_self,
                                   normalize=normalize, activation=self.activation, dropout=dropout)
        self.conv_block = [GATBlock(input_dim=hidden_dim, output_dim=hidden_dim, add_self=add_self,
                                   normalize=normalize, dropout=dropout, activation=self.activation)
                           for i in range(num_layers - 2)]
        self.conv_last = GATBlock(input_dim=hidden_dim, output_dim=output_dim, add_self=add_self,
                                  dropout=dropout, activation=self.activation)

    def forward(self, x, edge_index, batch_num_nodes=None):

        edge_index = edge_index.long()
        max_num_nodes = x.size()[0]
        if batch_num_nodes is not None:
            embedding_mask = self.construct_mask(max_num_nodes, batch_num_nodes)
        else:
            embedding_mask = None

        hidden = self.conv_first.forward(x, edge_index, embedding_mask)
        x_all = [hidden]

        for layer in self.conv_block:
            hidden = layer.forward(hidden, edge_index)
            x_all.append(hidden)

        hidden = self.conv_last.forward(hidden, edge_index, embedding_mask)
        x_all.append(hidden)
        out_tensor = torch.cat(x_all, dim=1)
        if embedding_mask is not None:
            out_tensor = out_tensor * embedding_mask
        # 使用max
        if self.first_dim_max:
            out_tensor, _ = torch.max(out_tensor, dim=0)

        return out_tensor

    def set_dev(self, device):
        self.to(device)
        self.conv_first.to(device)
        self.conv_last.to(device)
        for conv in self.conv_block:
            conv.to(device)
    def construct_mask(self, max_nodes, batch_num_nodes):
        ''' For each num_nodes in batch_num_nodes, the first num_nodes entries of the
        corresponding column are 1's, and the rest are 0's (to be masked out).
        Dimension of mask: [batch_size x max_nodes x 1]
        '''
        # masks
        packed_masks = [torch.ones(int(num)) for num in batch_num_nodes]
        batch_size = len(batch_num_nodes)
        out_tensor = torch.zeros(batch_size, max_nodes)
        for i, mask in enumerate(packed_masks):
            out_tensor[i, :batch_num_nodes[i]] = mask
        return out_tensor.unsqueeze(2).cuda()

    def set_mode(self, key='train'):
        if key is 'train':
            self.train()
            self.conv_first.train()
            self.conv_last.train()
            for conv in self.conv_block:
                conv.train()
        elif key is 'eval':
            self.eval()
            self.conv_first.eval()
            self.conv_last.eval()
            for conv in self.conv_block:
                conv.eval()


class GAT(torch.nn.Module):
    def __init__(self, in_f, out_f, multi_heading=False, dropout=0.6):
        super(GAT, self).__init__()
        # 如果是最后一层的GAT 就不加多头Attenction
        self.multi_heading = multi_heading
        self.do_p = dropout
        if not self.multi_heading:
            self.conv1 = GATConv(in_f, out_f, heads=8, dropout=self.do_p)
        else:
            self.conv1 = GATConv(in_f, out_f, dropout=self.do_p)

    def forward(self, feature, edge, training):
        if not self.multi_heading:
            x = F.dropout(feature, p=0.6, training=self.training)
            x = F.elu(self.conv1(x, edge))
        else:
            x = F.dropout(feature, p=0.6, training=self.training)
            x = self.conv2(x, edge)
        return x
