import torch
from torch.utils.data import DataLoader
from tqdm import tqdm
from data.datasets import FeatureAVAFullDataset
from model.total_model.graph_only import GAT_Diffpool_multi_pred
from configs.default_args import *
from utils.checkpoint import Checkpointer
from tensorboardX import SummaryWriter
from utils.image_evaluator import PLCC, score_to_2class_with_threshold, nclass_pred_to_2class
from model.loss.emd_loss import EMDLoss
import os

# GPU config
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
## dataset
test_dataset = FeatureAVAFullDataset(root=DATASET_ROOT_DIR, data_dir=DATASET_DATA_DIR
                                     , data_list_txt=DATASET_TEST_DATA_LIST_TXT
                                     )
train_dataset = FeatureAVAFullDataset(root=DATASET_ROOT_DIR, data_dir=DATASET_DATA_DIR
                                      , data_list_txt=DATASET_TRAIN_DATA_LIST_TXT
                                      )
val_dataset = FeatureAVAFullDataset(root=DATASET_ROOT_DIR, data_dir=DATASET_DATA_DIR
                                    , data_list_txt=DATASET_VAL_DATA_LIST_TXT
                                    )
test_loader = DataLoader(test_dataset, batch_size=1, num_workers=NUM_WORKERS)
val_loader = DataLoader(val_dataset, batch_size=BATCH_SIZE, num_workers=NUM_WORKERS)
train_loader = DataLoader(train_dataset, batch_size=BATCH_SIZE, num_workers=NUM_WORKERS,
                          shuffle=True)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
## model&opti init
model = GAT_Diffpool_multi_pred(MAX_NODES, input_dim=INPUT_DIM, hidden_dim=HIDDEN_DIM,
                                embedding_dim=EMBEDDING_DIM, label_dim=LABEL_DIM,
                                num_layers=NUM_LAYERS
                                , assign_hidden_dim=ASSIGN_HIDDEN_DIM, assign_ratio=ASSIGN_RATIO,
                                num_pooling=NUM_POOLING, bn=BATCH_SIZE,
                                dropout=DROPOUT, lr=0.001, device=device)

model.print_model()

# checkpoint
checkpoint = Checkpointer(model=model, optimizer=model.optimizer, save_dir=MODEL_DIR,
                          save_to_disk=True)
checkpoint.load()

# logger:
logger = SummaryWriter(MODEL_DIR + "/" + MODEL_NAME + "-log")



def train(epoch):
    model.set_mode('train')
    loss_all = 0
    now_i = 0
    acc_total = 0
    plcc_total = 0
    print_loss = 0
    for data in tqdm(train_loader):
        now_i += 1
        # fixme 注意这里是因为batchsize 为1 才可这样写
        elem = train_dataset.constract_data_from_dic(data, catLocatfeature=True)[0]
        score = elem.score.to(device)
        x = elem.x.to(device)
        score_p = elem.score_p.to(device)
        del elem

        # 预测步
        pred_2_class, pred_score, pred_score_p, reg = model.pred_process(x)

        # loss 计算步
        two_class_loss, emdloss, reg_loss, mseloss, total_loss = \
            model.loss_calc(score=score, score_p=score_p, pred_2_class=pred_2_class,
                            pred_score=pred_score, pred_score_p=pred_score_p, reg_loss=reg)

        # 指标评测步
        accuency, plcc_p = model.eval_quota(score, score_p, pred_2_class, pred_score_p)

        # 训练步
        model.train_process(total_loss)

        # 2分类正确率
        acc_total += float(accuency)
        # PLCC综合率
        plcc_total += float(plcc_p)

        # 清除显存占用
        torch.cuda.empty_cache()

        # 给print 统计用
        loss_all += total_loss.item()
        print_loss += total_loss.item()


        if now_i != 0:
            div = (now_i / BATCH_SIZE)
            this_acc = acc_total / div
            this_plcc = plcc_total / div
        if now_i % PRINT_PREIOD is 0:

            print(
                    "\nepoch{:03d} - training : ({:03d}/{}) loss:{:.6f}  epoch now train acc:{:.4f}; plcc_p: {:.4f}".format(
                        epoch, now_i,
                            len(train_loader), print_loss / PRINT_PREIOD,
                            this_acc, this_plcc))
            print_loss = 0

        if now_i % CHECKPOINT_PERIOD is 0 or now_i + 1 is len(train_dataset):
            checkpoint.save(MODEL_NAME)
        # 记录
        if now_i % LOG_PERIOD is 0:
            this_step = (epoch - 1) * len(train_dataset) + now_i
            logger.add_scalar('train/loss/2class_loss', two_class_loss, this_step)
            logger.add_scalar('train/loss/linked_loss', reg_loss, this_step)
            logger.add_scalar('train/loss/edm_loss', emdloss, this_step)
            logger.add_scalar('train/loss/total_loss', total_loss.item(), this_step)
            logger.add_scalar('train/accurency', this_acc, this_step)
            logger.add_scalar('train/plcc_p', this_plcc, this_step)
            logger.add_scalar('train/score_loss', mseloss, this_step)

    t_correct = acc_total / len(train_dataset)
    t_plcc = plcc_total / len(train_dataset)
    return loss_all / len(train_dataset), t_correct, t_plcc


def test(loader, epoch, bsize):
    model.set_mode('eval')
    correct = 0
    plcc = 0
    for data in tqdm(loader):
        elem = train_dataset.constract_data_from_dic(data, catLocatfeature=True)[0]
        score = elem.score.to(device)
        x = elem.x.to(device)
        score_p = elem.score_p.to(device)
        del elem

        # 预测步
        pred_2_class, pred_score, pred_score_p, _ = model.pred_process(x)

        # 指标评测步
        accuency, plcc_p = model.eval_quota(score, score_p, pred_2_class, pred_score_p)

        # 2分类正确率
        correct += accuency
        # PLCC综合率
        plcc += plcc_p
        now_i = 1
        # 清除显存占用
        torch.cuda.empty_cache()
        if now_i % 1000 is 0:
            this_acc = 0
            if now_i != 0:
                this_acc = correct / (now_i / bsize)
                this_plcc = plcc / (now_i / bsize)
            print("\nepoch{:03d} - testing : ({:03d}/{}) now test acc:{:.4f}; plcc:{:.4f}".format(
                epoch, now_i,
                len(loader),

                this_acc, this_plcc))
        now_i += 1
    return correct / (len(loader.dataset) / bsize), plcc / (len(loader.dataset) / bsize)


best_test_epoch = best_test = best_val_acc = test_acc = 0
for epoch in range(1, 51):
    train_loss, t_correct, _ = train(epoch)
    val_acc, _ = test(val_loader, epoch, BATCH_SIZE)
    if val_acc > best_val_acc:
        best_val_acc = val_acc
    test_acc, test_plcc = test(test_loader, epoch, 1)
    if test_acc > best_test:
        best_test = test_acc
        best_test_epoch = epoch
        # 存储最好的模型
        checkpoint.save(MODEL_NAME + "best")
    print('Epoch: {:03d}, Train Loss: {:.4f}'
          'Val Acc: {:.4f}, Test Acc: {:.4f} PLCC:{:.4f}, Best Test:{:.4f} best epoch{:03d}'.format(
        epoch,
        train_loss,
        val_acc,
        test_acc,
        test_plcc,
        best_test,
        best_test_epoch))

    this_step = epoch * len(train_dataset)
    logger.add_scalar('val/accurency', float(val_acc), this_step)
    logger.add_scalar('test/accurency', float(test_acc), this_step)
    logger.add_scalar('test/plcc', float(test_plcc), this_step)
