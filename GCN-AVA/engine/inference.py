# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved.
import logging
import time
import os
import numpy as np
import torch
from tqdm import tqdm
import cv2
from data.datasets.evaluation import evaluate
from utils.comm import is_main_process, get_world_size
from utils.comm import all_gather
from utils.comm import synchronize
from utils.timer import Timer, get_time_str
import torchvision.transforms.functional as F


def draw_rectanglar(img_id, img_map, box_list):
    save_root = '/home/huangfei/Project/deep_model_experiment/GCN-AVA/output/'
    save_path = os.path.join(save_root, img_id)
    img_map = F.to_pil_image(img_map)
    img_map = np.asarray(img_map)
    box_np = box_list.bbox.numpy().astype(int)  # 获取个数
    for elem in box_np:
        xy_head = tuple(elem[:2])
        xy_bottom = tuple(elem[2:])
        cv2.rectangle(img_map, xy_head, xy_bottom, (0, 0, 255), 2)
    cv2.imwrite(save_path, img_map)


def compute_on_dataset(model, data_loader, device, timer=None):
    model.eval()
    results_dict = {}
    cpu_device = torch.device("cpu")
    idx = 0
    for _, batch in enumerate(tqdm(data_loader)):
        img_id, images, label = batch
        images = images.to(device)
        with torch.no_grad():  # 防止梯度更新
            if timer:
                timer.tic()
            output = model(images)
            if timer:
                # 等待全部子线程结束运行
                torch.cuda.synchronize()
                timer.toc()
            # output后 直接迁移到cpu 不占用显存
            output = [o.to(cpu_device) for o in output]
            images = images.to(cpu_device)
            for i in range(len(output)):
                draw_rectanglar(img_id[i], images.tensors[i], output[i])
                results_dict.update(
                        {img_id[i]: output[i].bbox.shape[0]}
                )
    return results_dict


def extract_on_dataset(model, data_loader, device, timer=None):
    model.eval()
    results_dict = {}
    cpu_device = torch.device("cpu")
    for _, batch in enumerate(tqdm(data_loader)):
        img_id, images, label = batch
        images = images.to(device)
        with torch.no_grad():  # 防止梯度更新
            if timer:
                timer.tic()
            output = model(images)
            if timer:
                # 等待全部子线程结束运行
                torch.cuda.synchronize()
                timer.toc()
            # output后 直接迁移到cpu 不占用显存
            output = [o.to(cpu_device) for o in output]

            results_dict.update(
                    {img_id: output}
            )
    return results_dict
def _accumulate_predictions_from_multiple_gpus(predictions_per_gpu):
    all_predictions = all_gather(predictions_per_gpu)
    if not is_main_process():
        return
    # merge the list of dicts
    predictions = {}
    for p in all_predictions:
        predictions.update(p)
    # convert a dict where the key is the index in a list
    image_ids = list(sorted(predictions.keys()))
    if len(image_ids) != image_ids[-1] + 1:
        logger = logging.getLogger("maskrcnn_benchmark.inference")
        logger.warning(
            "Number of images that were gathered from multiple processes is not "
            "a contiguous set. Some images might be missing from the evaluation"
        )

    # convert to a list
    predictions = [predictions[i] for i in image_ids]
    return predictions


def inference(
        model,
        data_loader,
        dataset_name,
        iou_types=("bbox",),
        box_only=False,
        device="cuda",
        expected_results=(),
        expected_results_sigma_tol=4,
        output_folder=None,
):
    # convert to a torch.device for efficiency
    device = torch.device(device)
    num_devices = get_world_size()
    logger = logging.getLogger("maskrcnn_benchmark.inference")
    dataset = data_loader.dataset
    logger.info("Start evaluation on {} dataset({} images).".format(dataset_name, len(dataset)))
    total_timer = Timer()
    inference_timer = Timer()
    total_timer.tic()
    # predictions = compute_on_dataset(model, data_loader, device, inference_timer)
    output_features = extract_on_dataset(model, data_loader, device, inference_timer)
    # todo 对feature 存储
    # wait for all processes to complete before measuring the time
    synchronize()
    total_time = total_timer.toc()
    total_time_str = get_time_str(total_time)
    logger.info(
        "Total run time: {} ({} s / img per device, on {} devices)".format(
            total_time_str, total_time * num_devices / len(dataset), num_devices
        )
    )
    total_infer_time = get_time_str(inference_timer.total_time)
    logger.info(
        "Model inference time: {} ({} s / img per device, on {} devices)".format(
            total_infer_time,
            inference_timer.total_time * num_devices / len(dataset),
            num_devices,
        )
    )

    # predictions = _accumulate_predictions_from_multiple_gpus(predictions)
    if not is_main_process():
        return

