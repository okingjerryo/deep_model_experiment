from .ava_evaluation import do_ava_evaluation


def ava_evaluation(
        dataset,
        predictions,
        output_folder,
        expected_results,
):
    return do_ava_evaluation(
            dataset=dataset,
            predictions=predictions,
            output_folder=output_folder,
            expected_results=expected_results
    )
