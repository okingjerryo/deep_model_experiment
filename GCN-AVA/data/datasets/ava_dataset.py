from os import path as f_path
from PIL import Image
from torch.utils.data import Dataset as _Dataset


class AVAFullDataset(_Dataset):

    def __init__(self, root, data_dir, data_list_txt, transforms=None):
        super(AVAFullDataset, self).__init__()
        self.root = root
        self.transforms = transforms
        self.data_list_txt = data_list_txt
        self.data_root = f_path.join(self.root, data_dir)
        # 通过文件读入的文件名，例如xxxxx.jpg
        _data_list_path = f_path.join(self.root, self.data_list_txt)
        self.img_list = [i.split(' ')[0] for i in open(_data_list_path, 'r')]
        # 分数读入并四舍五入为整数
        self.img_score = [round(float(i.split(' ')[1])) for i in open(_data_list_path, 'r')]

    def __getitem__(self, idx):
        item_id = self.img_list[idx]
        img_path = f_path.join(self.data_root, item_id)
        img_map = Image.open(img_path).convert("RGB")
        score = self.img_score[idx]
        if self.transforms is not None:
            img_map = self.transforms(img_map)
        return item_id, img_map, score

    def __len__(self):
        return len(self.img_list)

    def get_img_info(self, index):
        img_path = str(f_path.join(self.data_root, self.img_list[index]))
        img_map = Image.open(img_path).convert("RGB")
        size = img_map.size
        return {
            'height': size[0],
            'width' : size[1]
        }
