from os import path as f_path
import cv2
import torch
import os
import scipy.io as sio
import torch.nn.functional as F
from PIL import Image
import numpy as np
from torch.utils.data import Dataset as _Dataset, DataLoader
from torch_geometric.data import DenseDataLoader
from tqdm import tqdm
from torchvision.transforms import ToTensor
from structures.aesthetics_data import AestheticsFeatureData
from structures.bounding_box import BoxList


class FeatureAVAFullDataset(_Dataset):

    def __eff_score__(self, elem):
        score = float(elem.split(' ')[1])
        return score
    def __init__(self, root, data_dir, data_list_txt, transforms=None, transpose_feature=True):
        super(FeatureAVAFullDataset, self).__init__()
        self.root = root  # 数据集根目录
        self.transforms = transforms
        self.data_list_txt = data_list_txt  # 文件列表名, 使用mat 后为数据mat的地址
        self.data_root = f_path.join(self.root, data_dir)  # feature目录位置
        # 通过文件读入的文件名，例如xxxxx.jpg
        _data_list_path = f_path.join(self.root, self.data_list_txt)
        # 打开mat文件
        mat = sio.loadmat(_data_list_path)
        id_list = mat['id']
        self.feature_list = [i.strip() + '.npz' for i in id_list]  # 在字符串文件中容易出现文件名后缀空格

        self.img_score = mat['score'][0]  # mat 套了一层 这里取0位置
        self.img_score_p = mat['score_p']
        self.transpose_feature = transpose_feature

    def __getitem__(self, idx):
        item_id = self.feature_list[idx]
        feature_path = f_path.join(self.data_root, item_id)
        # 读取npz矩阵
        bbox_loc, img_h, img_w, feature_tensor = self._read_feature_from_pth(feature_path)
        score = self.img_score[idx]
        score_p = self.img_score_p[idx]
        return {'id'   : item_id, 'feature': feature_tensor, 'box_loc': bbox_loc,
                'img_h': img_h, 'img_w': img_w, 'score': score, 'score_p': score_p}

    def __len__(self):
        return len(self.feature_list)

    def _read_feature_from_pth(self, feature_path):
        dic = np.load(feature_path)
        bbox_loc = dic['bbox']
        img_h = dic['image_h']
        img_w = dic['image_w']
        # bbox = BoxList(bbox_loc, (img_w, img_h))
        x = dic['x']
        x = torch.from_numpy(x)
        if self.transpose_feature:
            x = torch.transpose(x, 0, 1)
        return bbox_loc, img_h, img_w, x

    def draw_rectanglar(self, img_id, img_path, box_list):
        save_root = '/home/huangfei/Project/deep_model_experiment/GCN-AVA/output/'
        save_path = os.path.join(save_root, img_id)
        img_map = cv2.imread(img_path)
        # img_map = np.asarray(img_map)
        box_np = box_list.bbox.numpy().astype(int)  # 获取个数
        for elem in box_np:
            xy_head = tuple(elem[:2])
            xy_bottom = tuple(elem[2:])
            cv2.rectangle(img_map, xy_head, xy_bottom, (0, 0, 255), 2)
        cv2.imwrite(save_path, img_map)

    def constract_data_from_dic(self, data_dic, catLocatfeature=False):
        '''
        返回一个组装好的Aethetics 类的list， 大小为batchsize

        :param data_dic:字典格式的data矩阵
        :param catLocatfeature: 如果为True ，那么在特征向量的最后 cat上[x/W,y/H,w/W,h/H]四个相对位置变量
        :return:
        '''
        id = data_dic['id']
        feature_tensor = data_dic['feature']
        box_loc = data_dic['box_loc']
        img_h = data_dic['img_h']
        img_w = data_dic['img_w']
        score = data_dic['score']
        score_p = data_dic['score_p']
        if catLocatfeature:
            feature_tensor = self._cat_locate_feature(feature_tensor, img_w, img_h, box_loc)
        bbox = [BoxList(box_loc[i], (img_w[i], img_h[i])) for i in range(len(box_loc))]
        # 组装成batch的Aesthetics
        elem = [AestheticsFeatureData(id[i], feature_tensor[i], score[i], score_p[i], bbox[i]) for i
                in
                range(len(box_loc))]
        return elem

    def _cat_locate_feature(self, feture, W, H, BBOX):
        '''
        将 框节点的相对关系 作为特征放在最后
        :param feture: 特征
        :param W: 图片宽
        :param H: 图片高
        :param BBOX: node框
        :return: 拼完的feature
        '''
        # 类型转换
        W = W.float()
        H = H.float()
        # 提不同的列向量
        x1 = BBOX[:, :, 0]
        x2 = BBOX[:, :, 2]
        y1 = BBOX[:, :, 1]
        y2 = BBOX[:, :, 3]
        # 算目标向量
        w = x2 - x1
        h = y2 - y1
        x = x1 + w / 2
        y = x2 + h / 2
        # 算结果向量
        rel_x = x / W
        rel_y = y / H
        rel_w = w / W
        rel_h = h / H
        # 准备拼接
        re = torch.FloatTensor(feture.size(0), feture.size(1), 4)
        re[:, :, 0] = rel_x
        re[:, :, 1] = rel_y
        re[:, :, 2] = rel_w
        re[:, :, 3] = rel_h
        re = torch.cat((feture, re), dim=2)
        return re

if __name__ == '__main__':

    # 画框
    dataset = FeatureAVAFullDataset(root="/home/huangfei/db/ava-all", data_dir="feature"
                                    , data_list_txt="list/Ava_code_test.txt"
                                    )
    loader = DataLoader(dataset, batch_size=1)
    data_root = '/home/huangfei/db/ava-all/data'
    for data in tqdm(loader):
        elem_list = dataset.constract_data_from_dic(data)
        for elem in elem_list:
            name_list = elem.id.split('.')[:-1]
            img_name = ".".join(name_list)
            this_path = os.path.join(data_root, img_name)
            dataset.draw_rectanglar(img_name, this_path, elem.bbox)
