

# Dataset
DATASET_ROOT_DIR = '/home/huangfei/db/ava-all'  # ST
# DATASET_ROOT_DIR = '/data/uryuo/ava-all' # DT
DATASET_DATA_DIR = 'feature'
DATASET_TRAIN_DATA_LIST_TXT = 'list/Ava_train_20.mat'
DATASET_VAL_DATA_LIST_TXT = 'list/Ava_train_20_val.mat'
DATASET_TEST_DATA_LIST_TXT = 'list/Ava_test_20.mat'

# dataset
NUM_WORKERS = 8


## model
MAX_NODES = 200
# GAT
INPUT_DIM = 2048 + 4
HIDDEN_DIM = 512
EMBEDDING_DIM = 512
LABEL_DIM = 10
NUM_LAYERS = 3
DROPOUT = 0
# DIFFPOOL
ASSIGN_HIDDEN_DIM = 512
ASSIGN_RATIO = 0.1
NUM_POOLING = 1
BATCH_NORM = True

# TRAIN
BATCH_SIZE = 1

# checkpoint 位置
MODEL_DIR = "/home/huangfei/Project/deep_model_experiment/GCN-AVA/checkpoint_soft"  # ST
# MODEL_DIR = "/home/uryuo/Project/deep_model_experiment/GCN-AVA/checkpoint" # DT

GRADIENT_TRUNCATION = 2.

# 做梯度累加
ACCUMULATION_STEP = 10
PRINT_PREIOD = 1000
CHECKPOINT_PERIOD = 10000
MODEL_NAME = "GCN-graphonly-soft"

LOG_PERIOD = 100
