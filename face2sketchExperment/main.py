import dataset
import args
import os
import model as model
import tensorflow as tf
import op as op
import cv2
import numpy as np
os.environ['CUDA_VISIBLE_DEVICES'] = "0"


# 主函数中调用estimator的接口
def create_estimator():
    checkpointing_config = tf.estimator.RunConfig(
        save_checkpoints_secs=10 * 60,  # 10min save
        keep_checkpoint_max=1, # 存30min内的 占用2G附近
        save_summary_steps=10,
        log_step_count_steps=50
    )
    return tf.estimator.Estimator(
        model_fn=model.cGAN_main,
        model_dir="model_pix2pix_inL2/",
        config=checkpointing_config,params={
            'pre_train':65 * args.TRAIN_STEPS
        }
    )

# 读数据要放在main里面
# 三个数据流
def train_input_fn():
    data_op = dataset._init_handle(dataset_type=dataset.TRAIN_DATASET_TYPE)
    return data_op['data_set']

def eval_input_fn():
    data_op = dataset._init_handle(dataset_type=dataset.VAILD_DATASET_TYPE)
    return data_op['data_set']

def test_input_fn():
    data_op = dataset._init_handle(dataset_type=dataset.TEST_DATASET_TYPE)
    return data_op['data_set']

def write_to_disk(img_dic_list,path):
    if not os.path.exists(path):
        os.mkdir(path)
    counter = 1
    for img_dic in img_dic_list:
        gen_bgr = op.de_max_min_rgb(img_dic['gen_image'])
        org_bgr = op.de_max_min_rgb(img_dic['org_image'])
        ske_bgr = op.de_max_min_rgb(img_dic['ske_image'])
        gen_rgb = np.array(cv2.cvtColor(gen_bgr[:250,:200],cv2.COLOR_GRAY2RGB))
        org_rgb = np.array(org_bgr[:250,:200])
        ske_rgb = np.array(cv2.cvtColor(ske_bgr[:250,:200],cv2.COLOR_GRAY2RGB)[:250,:200])

        result_rgb = np.stack([org_rgb,ske_rgb,gen_rgb],axis=1)
        result_rgb = np.reshape(result_rgb,[result_rgb.shape[0],result_rgb.shape[1]*result_rgb.shape[2],result_rgb.shape[3]])

        filepath = path+'/'+str(counter)+'.jpg'
        cv2.imwrite(filepath,result_rgb,[int(cv2.IMWRITE_JPEG_QUALITY),100])
        counter+=1

def main(argv):
    # 建立模型函数
    adaIN_estimator = create_estimator()
    # epoch
    # 训练模型
    with tf.Session(config=args.gpu_option()) as sess:
        for i in range(65):
            print('=======- pretrain epoch {}=========='.format((i+1)*10))
            adaIN_estimator.train(
                input_fn=train_input_fn,steps=args.TRAIN_STEPS  #100
            )

            adaIN_estimator.evaluate(
                    input_fn=eval_input_fn,steps=10
                )
        # for i in range(600):
        #     print('=======-training epoch {}=========='.format(i + 1))
        #     adaIN_estimator.train(
        #         input_fn=train_input_fn, steps=args.TRAIN_STEPS
        #     )
        #     if i % 5 is 0:
        #         adaIN_estimator.evaluate(
        #             input_fn=eval_input_fn, steps=10
        #         )
        pridict = list(adaIN_estimator.predict(
            input_fn=test_input_fn
        ))
        write_to_disk(pridict, 'pix2pix_result_inL2')
    # 测试模型

if __name__ == '__main__':
    tf.logging.set_verbosity(tf.logging.INFO)
    tf.app.run(main)

