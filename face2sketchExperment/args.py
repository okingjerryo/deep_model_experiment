import tensorflow as tf
import argparse
import os

# limit gpu mem useage
def gpu_option():
    gpu_options = tf.GPUOptions(allow_growth=True)
    return tf.ConfigProto(gpu_options=gpu_options)



parser = argparse.ArgumentParser()

# tf args
parser.add_argument("--image_size", default=256, type=int, help="input img size")
parser.add_argument("--img_crop_size", default=256, type=int, help="crop target size")


parser.add_argument("--batch_size", type=int, default=30, help="number of images in batch")

parser.add_argument("--g_lr", type=float, default=0.0004, help="initial learning rate for adam")
parser.add_argument("--detail_lambda", type=float, default=1, help="detail loss lambda")
parser.add_argument("--d_lr", type=float, default=0.0002, help="initial learning rate for adam")
parser.add_argument("--l1_weight", type=float, default=100.0, help="weight on L1 term for generator gradient")
parser.add_argument("--gan_weight", type=float, default=1.0, help="weight on GAN term for generator gradient")
parser.add_argument("--ngf", type=int, default=64, help="number of generator filters in first conv layer")
parser.add_argument("--ndf", type=int, default=64, help="number of discriminator filters in first conv layer")
# export options
parser.add_argument("--output_filetype", default="png", choices=["png", "jpeg"])
# dataset options
parser.add_argument("--input_data_type", default="tfrecord", choices=["image_list", "tfrecord"]
                    , help="data input type when init data handle")
parser.add_argument("--path_to_record", default="/home/uryuo/photosketch/tfrecord",
                    help="the path navgate to tfrecord dir")
parser.add_argument("--img_gt_path", default="/home/uryuo/db/ChinaMM/ppchallenge2018/dataset/train_img_tpg/GT/*.*",
                    help="path to ground true images")
parser.add_argument("--noise_train_path",
                    default='/home/uryuo/db/ChinaMM/ppchallenge2018/dataset/train_img_tpg/Composed/*/*.*',
                    help="path to noise path")
parser.add_argument("--noise_vaild_path",
                    default='/home/uryuo/db/ChinaMM/ppchallenge2018/dataset/vaild_img_tpg/Composed/*/*.*',
                    help="path to vaild path")
parser.add_argument("--noise_test_path",
                    default='/home/uryuo/db/ChinaMM/ppchallenge2018/dataset/test_img_tpg/Composed/*/*.*',
                    help="path to vaild path")
parser.add_argument("--record_train_name", default="photosketch_train", help="train tfrecord file name")
parser.add_argument("--record_vaild_name", default="photosketch_vaild", help="vaild tfrecord file name")
parser.add_argument("--record_test_name", default="photosketch_test", help="test tfrecord file name")
parser.add_argument("--txt_path_prefix", default='/home/jacob/dataset/photosketch', help='path to txt path list')

#cgan
parser.add_argument("--separable_conv", action="store_true", help="use separable convolutions in the generator")
parser.add_argument("--beta1", type=float, default=0.5, help="momentum term of adam")
a = parser.parse_args()

# 数据集相关
PATH2RECORD = a.path_to_record  # 将数据集转 tfrecord 位置
PATH2TXTLIST = a.txt_path_prefix
# noise图片 glob 路径
NOISE_TRAIN_PATH = a.noise_train_path
NOISE_VAILD_PATH = a.noise_vaild_path
NOISE_TEST_PATH = a.noise_test_path

# tfrecord 命名
RECORD_TRAIN_NAME = a.record_train_name
RECORD_VAILD_NAME = a.record_vaild_name
RECORD_TEST_NAME = a.record_test_name
# record验证图片存储LUJING
RECORD_ALIABLE_PATH = 'resource/'
# network
EPS = 1e-12
TRAIN_BATCH = a.batch_size
IMG_CROP_SIZE = [a.img_crop_size, a.img_crop_size, 3]
INPUT_SIZE = a.image_size
TRAIN_STEPS = (330//TRAIN_BATCH)*10+1  # 根据bachsize 对应的一个epoch的量
NUM_PARALLEL_CALLS = 7
NUM_PARALLEL_READERS = 7
NUM_PREFATCH = int(200)
# checkpoint config
