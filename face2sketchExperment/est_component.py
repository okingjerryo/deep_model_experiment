import tensorflow as tf
import args
import collections
import face2sketchExperment.op as op
from args import a

# normalize interface
E_Component = collections.namedtuple("E_Component", "output_conv,conv_detail")

# from cGAN
def cgan_unet_g(input_X, final_output_channals,mode):
    layers = []

    # encoder_1: [batch, 256, 256, in_channels] => [batch, 128, 128, ngf]
    with tf.variable_scope("encoder_1"):
        output = op.gen_conv(input_X, a.ngf,kernel=4)
        layers.append(output)

    layer_specs = [
        a.ngf * 2,  # encoder_2: [batch, 128, 128, ngf] => [batch, 64, 64, ngf * 2]
        a.ngf * 4,  # encoder_3: [batch, 64, 64, ngf * 2] => [batch, 32, 32, ngf * 4]
        a.ngf * 8,  # encoder_4: [batch, 32, 32, ngf * 4] => [batch, 16, 16, ngf * 8]
        a.ngf * 8,  # encoder_5: [batch, 16, 16, ngf * 8] => [batch, 8, 8, ngf * 8]
        a.ngf * 8,  # encoder_6: [batch, 8, 8, ngf * 8] => [batch, 4, 4, ngf * 8]
        a.ngf * 8,  # encoder_7: [batch, 4, 4, ngf * 8] => [batch, 2, 2, ngf * 8]
        a.ngf * 8,  # encoder_8: [batch, 2, 2, ngf * 8] => [batch, 1, 1, ngf * 8]
    ]

    for out_channels in layer_specs:
        with tf.variable_scope("encoder_%d" % (len(layers) + 1)):
            rectified = op.lrelu(layers[-1], 0.2)
            # [batch, in_height, in_width, in_channels] => [batch, in_height/2, in_width/2, out_channels]
            convolved = op.gen_conv(rectified, out_channels,kernel=4)
            output = op.instance_norm(convolved)
            layers.append(output)

    layer_specs = [
        (a.ngf * 8, 0.5),  # decoder_8: [batch, 1, 1, ngf * 8] => [batch, 2, 2, ngf * 8 * 2]
        (a.ngf * 8, 0.5),  # decoder_7: [batch, 2, 2, ngf * 8 * 2] => [batch, 4, 4, ngf * 8 * 2]
        (a.ngf * 8, 0.5),  # decoder_6: [batch, 4, 4, ngf * 8 * 2] => [batch, 8, 8, ngf * 8 * 2]
        (a.ngf * 8, 0.0),  # decoder_5: [batch, 8, 8, ngf * 8 * 2] => [batch, 16, 16, ngf * 8 * 2]
        (a.ngf * 4, 0.0),  # decoder_4: [batch, 16, 16, ngf * 8 * 2] => [batch, 32, 32, ngf * 4 * 2]
        (a.ngf * 2, 0.0),  # decoder_3: [batch, 32, 32, ngf * 4 * 2] => [batch, 64, 64, ngf * 2 * 2]
        (a.ngf, 0.0),  # decoder_2: [batch, 64, 64, ngf * 2 * 2] => [batch, 128, 128, ngf * 2]
    ]

    num_encoder_layers = len(layers)
    for decoder_layer, (out_channels, dropout) in enumerate(layer_specs):
        skip_layer = num_encoder_layers - decoder_layer - 1
        with tf.variable_scope("decoder_%d" % (skip_layer + 1)):
            if decoder_layer == 0:
                # first decoder layer doesn't have skip connections
                # since it is directly connected to the skip_layer
                input = layers[-1]
            else:
                input = tf.concat([layers[-1], layers[skip_layer]], axis=3)

            rectified = tf.nn.relu(input)
            # [batch, in_height, in_width, in_channels] => [batch, in_height*2, in_width*2, out_channels]
            output = op.gen_deconv(rectified, out_channels,kernel=4)
            output = op.instance_norm(output)

            if dropout > 0.0:
                output = tf.nn.dropout(output,keep_prob=1-dropout)

            layers.append(output)

    # decoder_1: [batch, 128, 128, ngf * 2] => [batch, 256, 256, ngf]
    with tf.variable_scope("decoder_1"):
        input = tf.concat([layers[-1], layers[0]], axis=3)
        rectified = tf.nn.relu(input)
        # output = op.gen_deconv(rectified, final_output_channals) change in ver0.2
        out_feature = op.gen_deconv(rectified,1,kernel=4)
        output = tf.tanh(out_feature)
        # out_feature = op.instance_norm(out_feature)
        layers.append(output)  # bn后结果


    # with tf.variable_scope('decoder_to_rgb'):
    #     output = out_rgb(layers[-1],1)


    return {
        'output_rgb':output,
        'output_feature':out_feature # 变为使用128*128*32 的 featuremap
    }
    # return layers[-1]

# simple double covlayer
def simple_double_conv(feature,final_output_channals):
    net = tf.layers.conv2d(feature,a.ngf//2,kernel_size=7,padding='same')
    net = op.instance_norm(net)
    net = tf.nn.leaky_relu(net)

    net = tf.layers.conv2d(net,final_output_channals,kernel_size=3,padding='same')
    net = op.instance_norm(net)
    output = tf.nn.leaky_relu(net)
    return output

def enhance_out_conv(feature,final_output_channals):
    #u32
    net = tf.layers.conv2d(feature,32,kernel_size=3,padding='same')
    net = op.instance_norm(net)
    net = tf.nn.relu(net)
    #c7s1-1
    net = tf.layers.conv2d(net,final_output_channals,kernel_size=7,padding='same')
    net = op.instance_norm(net)
    output = tf.nn.relu(net)
    return output

def org_encode_redisual(feature,final_output_channals):
    pass

def out_rgb(feature,final_output_channals):
    out = tf.nn.relu(feature)
    out = tf.layers.conv2d(out,final_output_channals,kernel_size=3,padding='same',activation=tf.nn.tanh)
    return out
# from cGAN
def cgan_patch_d(input_X, input_Y):
    n_layers = 3
    layers = []

    # 2x [batch, height, width, in_channels] => [batch, height, width, in_channels * 2]
    input = tf.concat([input_X, input_Y], axis=3)

    # layer_1: [batch, 256, 256, in_channels * 2] => [batch, 128, 128, ndf]
    with tf.variable_scope("layer_1"):
        convolved = op.discrim_conv(input, a.ndf, stride=2)
        rectified = op.lrelu(convolved, 0.2)
        layers.append(rectified)

    # layer_2: [batch, 128, 128, ndf] => [batch, 64, 64, ndf * 2]
    # layer_3: [batch, 64, 64, ndf * 2] => [batch, 32, 32, ndf * 4]
    # layer_4: [batch, 32, 32, ndf * 4] => [batch, 31, 31, ndf * 8]
    for i in range(n_layers):
        with tf.variable_scope("layer_%d" % (len(layers) + 1)):
            out_channels = a.ndf * min(2 ** (i + 1), 8)
            stride = 1 if i == n_layers - 1 else 2  # last layer here has stride 1
            convolved = op.discrim_conv(layers[-1], out_channels, stride=stride)
            normalized = op.batchnorm(convolved)
            rectified = op.lrelu(normalized, 0.2)
            layers.append(rectified)

    # layer_5: [batch, 31, 31, ndf * 8] => [batch, 30, 30, 1]
    with tf.variable_scope("layer_%d" % (len(layers) + 1)):
        convolved = op.discrim_conv(rectified, out_channels=1, stride=1)
        output = tf.sigmoid(convolved)
        layers.append(output)

    return layers[-1]


