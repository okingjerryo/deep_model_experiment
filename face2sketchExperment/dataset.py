import tensorflow as tf
import op as op
import cv2
import os
import args as args
import collections
from glob import glob
from tqdm import tqdm
from args import a
import data_struct as struct
import numpy as np
# 用于识别不同的操作图片集
TRAIN_DATASET_TYPE = 0
VAILD_DATASET_TYPE = 1
TEST_DATASET_TYPE = 2
# 可以feed的dataset handle
DATA_HANDEL = tf.placeholder(dtype=tf.string, shape=[], name='dataset_handle')

def _img_effect_process(org_picblock,ske_picblock):
    # 归一化
    # 使用 min_max算法归一化
    with tf.variable_scope('iamge_affect'):
        # 随机变换对比光照
        # org_picblock = tf.image.random_saturation(org_picblock,0.2,1)
        # org_picblock = tf.image.random_brightness(org_picblock,0.1)
        # org_picblock = tf.image.random_contrast(org_picblock,0.2,1)
        # org_picblock = tf.image.random_hue(org_picblock,0.1)
        # 归一化（-1~1）
        org_picblock = op.min_max_regular_rgb(org_picblock)
        ske_picblock = op.min_max_regular_rgb(ske_picblock)

    return org_picblock,ske_picblock

def _get_tfrecord_structure():
    """
        定义tfrecord 以及trainning set features的结构
    :return: features 给tf.parse_single_example用
    """
    return {
        'org_image': tf.FixedLenFeature((), tf.string, default_value=''),
        'ske_image': tf.FixedLenFeature((), tf.string, default_value=''),
    }


def read_path_from_text(path_to_txt):
    '''
        从txt中读取两个图像的路径
    :param path_to_txt: 到这个文件的路径
    :return: 返回的两个
    '''
    img_list_col = []
    f = open(path_to_txt, 'r')
    for line in f.readlines():
        arr = line.split('||')
        a_img_col = struct.img_collection()
        a_img_col.ske_img = args.PATH2TXTLIST+'/'+arr[0]
        a_img_col.org_img = args.PATH2TXTLIST+'/'+arr[1]
        img_list_col.append(a_img_col)
    f.close()
    return img_list_col


def _read_imgpair_from_noisepath(path_to_txt) -> (list(),list()):
    """
        图像读取接口
    :return: 原图像与画像图像的对
    """
    # read path list
    file_path_list = read_path_from_text(path_to_txt)
    org_img = []
    ske_img = []
    for a_path in file_path_list:
        org_path = a_path.org_img
        ske_path = a_path.ske_img
        org_block = cv2.imread(org_path)
        ske_block = cv2.imread(ske_path)
        # 图像位置进行预处理
        org_block = op.resize_img_with_padding(org_block,size = [args.INPUT_SIZE,args.INPUT_SIZE])
        ske_block = op.resize_img_with_padding(ske_block,size = [args.INPUT_SIZE,args.INPUT_SIZE])
        org_img.append(org_block)
        ske_img.append(ske_block)

    return org_img,ske_img


def _read_img_process(img_example,data_type,img_list_gt = None):
    features = None
    # 注意注意！ features中的key value对一定要与tfrecord中的k-v相关对应！
    org_img = None
    ske_img = None
    if data_type is 'image_list':
        # 从noise的路径读取两个文件，从noise读取只需要匹配文件名称即可
        org_img=img_example
        ske_img = img_list_gt
    elif data_type is 'tfrecord':
        features = _get_tfrecord_structure()
        parse_features = tf.parse_single_example(img_example, features)
        org_img = tf.decode_raw(parse_features['org_image'],tf.uint8) # 注意核对输入输出的数据格式。图片小心是int8还是uint8
        ske_img = tf.decode_raw(parse_features['ske_image'],tf.uint8)
        org_img = tf.cast(org_img,tf.float32)
        ske_img = tf.cast(ske_img,tf.float32)
        org_img = tf.reshape(org_img,[args.INPUT_SIZE,args.INPUT_SIZE,3])
        ske_img = tf.reshape(ske_img,[args.INPUT_SIZE,args.INPUT_SIZE,3])
    # tf.Print(org_img,[tf.reduce_max(org_img),tf.reduce_min(org_img)])
    # tf.Print(ske_img,[tf.reduce_max(ske_img),tf.reduce_min(ske_img)])
    org_img,ske_img = _img_effect_process(org_img,ske_img)
    ske_img = tf.image.rgb_to_grayscale(ske_img)
    # 这里关联着inputFun
    return {'org_image':org_img,
            'ske_image':ske_img}

def _bytes_feature(bytes):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[bytes]))
def _int64_feature(nums):
    return tf.train.Feature(int64_list = tf.train.Int64List(value = [nums]))




def write_img_record(txt_path, dataset_type):
    print('file convert start')
    record_path = None
    record_name = None
    if dataset_type == TRAIN_DATASET_TYPE:
        record_path = args.PATH2RECORD +'/'+ args.RECORD_TRAIN_NAME+'.tfrecord'
        record_name = args.RECORD_TRAIN_NAME
    elif dataset_type == TEST_DATASET_TYPE:
        record_path = args.PATH2RECORD +'/'+ a.record_test_name+'.tfrecord'
        record_name = a.record_test_name
    elif dataset_type == VAILD_DATASET_TYPE:
        record_path = args.PATH2RECORD +'/'+a.record_vaild_name+'.tfrecord'
        record_name = a.record_vaild_name
    writer = tf.python_io.TFRecordWriter(record_path)

    ske_img,org_img = _read_imgpair_from_noisepath(txt_path)
    end = round(len(org_img))
    start = round(end*0.85)+1
    if dataset_type == TRAIN_DATASET_TYPE:
        itor = range(start)
    elif dataset_type == VAILD_DATASET_TYPE:
        itor = range(start,end)
    else:
        itor = range(end)
    for i in tqdm(itor):  # 0.8 倍训练集
        example = tf.train.Example(features=tf.train.Features(feature={
            'org_image': _bytes_feature(org_img[i].tostring()),
            'ske_image': _bytes_feature(ske_img[i].tostring()),
        }))
        writer.write(example.SerializeToString())
    writer.close()
    print('save complete,path:', record_path)


def _init_handle(dataset_type,record_path=None, data_type=None,img_path_list = None,get_itor = None):
    '''
        初始化DataSet Handle
    :return:
    '''
    # 默认参数
    if get_itor is None:
        get_itor = False
    if data_type is None:
        data_type = a.input_data_type
    if img_path_list is None:
        if dataset_type is TRAIN_DATASET_TYPE:
            img_path_list = glob(args.NOISE_TRAIN_PATH)
            if record_path is None:
                record_path =tf.data.Dataset.list_files(args.PATH2RECORD + '/' + args.RECORD_TRAIN_NAME+'*.tfrecord')
        elif dataset_type is VAILD_DATASET_TYPE:
            img_path_list = glob(args.NOISE_VAILD_PATH)
            if record_path is None:
                record_path = tf.data.Dataset.list_files(args.PATH2RECORD + '/' + args.RECORD_VAILD_NAME+'*.tfrecord')
        elif dataset_type is TEST_DATASET_TYPE:
            img_path_list = glob(args.NOISE_TEST_PATH)
            if record_path is None:
                record_path = tf.data.Dataset.list_files(args.PATH2RECORD + '/' + args.RECORD_TEST_NAME+'*.tfrecord')

    train_dataset = None
    main_data_type = data_type # 总数据类型注入
    if main_data_type is 'image_list':
        noise,gt = None # pass  #_read_img_list_pipeline(img_path_list=img_path_list)
        train_dataset = tf.data.Dataset.from_tensor_slices((noise,gt))
        train_dataset = train_dataset.map(
            lambda x,y: _read_img_process(x,main_data_type,img_list_gt=y)
        )
    elif main_data_type is 'tfrecord':
        train_dataset = record_path.interleave(tf.data.TFRecordDataset,cycle_length=args.NUM_PARALLEL_READERS)
        train_dataset = train_dataset.map(
            map_func=lambda x: _read_img_process(x,main_data_type),
            num_parallel_calls=args.NUM_PARALLEL_CALLS
        )
    if dataset_type is TEST_DATASET_TYPE:
        train_dataset = train_dataset.batch(args.TRAIN_BATCH)
    else:
        train_dataset = train_dataset.shuffle(buffer_size=1000)
        train_dataset = train_dataset.repeat()  # 组成一个epoch
        train_dataset = train_dataset.batch(args.TRAIN_BATCH)

    # 优化 预处理pipeline
    train_dataset = train_dataset.prefetch(buffer_size=args.NUM_PREFATCH)

    if get_itor is False:
        return {
            'data_set':train_dataset
        }
    #init handle
    iterator = tf.data.Iterator.from_string_handle(DATA_HANDEL, train_dataset.output_types,
                                                train_dataset.output_shapes)
    get_next_batch = iterator.get_next()

    train_itor = train_dataset.make_initializable_iterator()
    return {
        'train_itor': train_itor,
        'get_next_batch': get_next_batch,
        'data_set':train_dataset
    }





def vaildate_data_useable(noise_img_list, input_type=None, action_type=None):
    '''
    获取一个batch数据，并取出头第一对图片。看其是否有效
    :return: null
    '''
    # 默认参数区
    if input_type is None:
        input_type = a.input_data_type
    if action_type is None:
        action_type = TRAIN_DATASET_TYPE

    data_op = _init_handle(data_type=input_type,dataset_type=action_type,img_path_list=noise_img_list,get_itor=True)

    get_batch = data_op['get_next_batch']
    train_itor = data_op['train_itor']

    with tf.Session(config=args.gpu_option()) as sess:
        print('vailding data...')
        data_handle = sess.run(train_itor.string_handle())

        for i in range(2):
            train_feed = {DATA_HANDEL: data_handle}
            sess.run(train_itor.initializer, feed_dict=train_feed)
            batch_feature = sess.run(get_batch, feed_dict=train_feed)
            path = 'data/test/'
            print('ske batch data max:{}'.format(np.max(batch_feature['ske_image'])))
            print('ske batch data min:{}'.format(np.min(batch_feature['ske_image'])))
            print('org batch data max:{}'.format(np.max(batch_feature['org_image'])))
            print('org batch data min:{}'.format(np.min(batch_feature['org_image'])))
            for j in range(batch_feature['org_image'].shape[0]):

                ske_rgb = op.de_max_min_rgb(batch_feature['ske_image'][j])
                org_rgb = op.de_max_min_rgb(batch_feature['org_image'][j])
                if not os.path.exists(path+'org/'+str(i)+'/'):
                    os.makedirs(path+'org/'+str(i))
                if not os.path.exists(path+'ske/'+str(i)+'/'):
                    os.makedirs(path+'ske/'+str(i))
                cv2.imwrite(path+'org/'+str(i)+'/'+str(j)+'.jpg',org_rgb)
                cv2.imwrite(path+'ske/'+str(i)+'/'+str(j)+'.jpg',ske_rgb)



if __name__ == '__main__':
    # write_img_record("/data/uryuo/db/photosketch/list_train.txt",TRAIN_DATASET_TYPE)
    # write_img_record("/data/uryuo/db/photosketch/list_train.txt",VAILD_DATASET_TYPE)
    # write_img_record("/data/uryuo/db/photosketch/list_test.txt",TEST_DATASET_TYPE)
    # print('====================================')
    # vaildate_data_useable(None,'tfrecord',TRAIN_DATASET_TYPE)
    # print('====================================')
    # vaildate_data_useable(None,'tfrecord',VAILD_DATASET_TYPE)
    print('====================================')
    vaildate_data_useable(None,'tfrecord',TEST_DATASET_TYPE)
    print('====================================')
    vaildate_data_useable(None, 'tfrecord', TRAIN_DATASET_TYPE)