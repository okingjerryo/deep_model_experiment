import est_component as compon
import tensorflow as tf
from args import a
import args
import op as op

def cGAN_main(features, labels, mode, params):

    # logging hook
    mode_str = 'test'
    if mode == tf.estimator.ModeKeys.TRAIN:
        mode_str = 'train'
    elif mode == tf.estimator.ModeKeys.EVAL:
        mode_str = 'eval'
    # vars
    input_X = features["org_image"]
    input_Y = features["ske_image"]
    if len(input_X.shape) < 4:
        input_X = tf.expand_dims(input_X, axis=0)
        input_Y = tf.expand_dims(input_Y, axis=0)
    # model
    with tf.variable_scope("generator"):
        out_channels = int(input_Y.get_shape()[-1])
        outputs = compon.cgan_unet_g(input_X, out_channels,mode_str=='train')['output_rgb']
        # 预测时，只需要调用生成器生成图片即可
    if mode == tf.estimator.ModeKeys.PREDICT:
        predictions = {
            'gen_image':outputs,
            'org_image':input_X,
            'ske_image':input_Y
        }
        return tf.estimator.EstimatorSpec(mode,predictions=predictions)

    gen_rgb_real = op.de_max_min_rgb(outputs)
    tf.summary.image('gen_image',gen_rgb_real)
    ske_rgb_real = op.de_max_min_rgb(input_Y)
    tf.summary.image('ske_image_gt',ske_rgb_real)
    input_rgb_real = op.de_max_min_rgb(input_X)
    tf.summary.image('org_image',input_rgb_real)
        #psnr
    # psnr = op.calculate_psnr_tensor(ske_rgb_real,gen_rgb_real)
    # psnr_me = tf.metrics.mean(psnr)
    # psnr = tf.reduce_mean(psnr)
    # tf.summary.scalar('psnr',psnr)
    # create two copies of discriminator, one for real pairs and one for fake pairs
    # they share the same underlying variables
    with tf.name_scope("real_discriminator"):
        with tf.variable_scope("discriminator"):
            # 2x [batch, height, width, channels] => [batch, 30, 30, 1]
            predict_real = compon.cgan_patch_d(input_X, input_Y)

    with tf.name_scope("fake_discriminator"):
        with tf.variable_scope("discriminator", reuse=True):
            # 2x [batch, height, width, channels] => [batch, 30, 30, 1]
            predict_fake = compon.cgan_patch_d(input_X, outputs)

    with tf.name_scope("discriminator_loss"):
        # minimizing -tf.log will try to get inputs to 1
        # predict_real => 1
        # predict_fake => 0
        d_vars = [var for var in tf.trainable_variables() if var.name.startswith("discriminator")]
        d_regular = tf.contrib.layers.l2_regularizer(scale=5e-5)
        d_l2_loss = tf.contrib.layers.apply_regularization(d_regular, d_vars)
        discrim_loss = tf.reduce_mean(-(tf.log(predict_real + args.EPS) + tf.log(1 - predict_fake + args.EPS)))+d_l2_loss

    with tf.name_scope("generator_loss"):
        # predict_fake => 1
        # abs(targets - outputs) => 0
        g_vars = [var for var in tf.trainable_variables() if var.name.startswith("generator")]
        g_regular = tf.contrib.layers.l2_regularizer(scale=5e-5)
        g_l2_loss = tf.contrib.layers.apply_regularization(g_regular, g_vars)
        gen_loss_GAN = tf.reduce_mean(-tf.log(predict_fake + args.EPS))
        gen_loss_L1 = tf.reduce_mean(tf.abs(input_Y - outputs))
        # l2 regular

        gen_loss = gen_loss_GAN * a.gan_weight + gen_loss_L1 * a.l1_weight+g_l2_loss
    ema = tf.train.ExponentialMovingAverage(decay=0.99)
    # psnr = op.calculate_psnr_tensor(input_Y,outputs.output_conv)
    eval_metics = {
        # 'psnr':psnr_me,
        'D_Loss':tf.metrics.mean(-(tf.log(predict_real + args.EPS) + tf.log(1 - predict_fake + args.EPS))),
        'G_Loss_adv':tf.metrics.mean(-tf.log(predict_fake + args.EPS)),
        'G_loss_L1':tf.metrics.mean(tf.abs(input_Y - outputs)),
        'G_Loss_Total':tf.metrics.mean(gen_loss),
        'D_l2_loss':tf.metrics.mean(d_l2_loss),
        'G_l2_loss':tf.metrics.mean(g_l2_loss)
    }


    # logging


    if mode == tf.estimator.ModeKeys.EVAL:
        return tf.estimator.EstimatorSpec(
            mode,
            # training_hooks=[hook],
            eval_metric_ops=eval_metics,
            loss = gen_loss,
        )

    tf.summary.scalar("D_Loss",discrim_loss)
    tf.summary.scalar("G_Loss_adv",gen_loss_GAN)
    tf.summary.scalar("G_loss_L1",gen_loss_L1)
    tf.summary.scalar("G_Loss_Total",gen_loss)
    tf.summary.scalar("D_l2_loss",d_l2_loss)
    tf.summary.scalar("G_l2_loss",g_l2_loss)
    assert mode == tf.estimator.ModeKeys.TRAIN
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

    with tf.name_scope("discriminator_train"):
        with tf.control_dependencies(update_ops):
            discrim_tvars = [var for var in tf.trainable_variables() if var.name.startswith("discriminator")]
            discrim_optim = tf.train.AdamOptimizer(a.d_lr, a.beta1)
            discrim_grads_and_vars = discrim_optim.compute_gradients(discrim_loss, var_list=discrim_tvars)
            discrim_train = discrim_optim.apply_gradients(discrim_grads_and_vars)

    with tf.name_scope("generator_train"):
        with tf.control_dependencies([discrim_train]):
            gen_tvars = [var for var in tf.trainable_variables() if var.name.startswith("generator")]
            gen_optim = tf.train.AdamOptimizer(a.g_lr, a.beta1)
            gen_grads_and_vars = gen_optim.compute_gradients(gen_loss, var_list=gen_tvars)
            gen_train = gen_optim.apply_gradients(gen_grads_and_vars)


    update_losses = ema.apply([discrim_loss, gen_loss_GAN, gen_loss_L1])

    # 滑动平均值
    global_step = tf.train.get_or_create_global_step()
    incr_global_step = tf.assign(global_step, global_step + 1)
    train_op = tf.group(update_losses, incr_global_step, gen_train)
    logging_hook = tf.train.LoggingTensorHook(
        tensors={'D_Loss':discrim_loss,
                 'G_Loss_Total':gen_loss,
                 # 'PSNR':psnr
                 },
        every_n_iter=10
    )
    return tf.estimator.EstimatorSpec(
        mode,
        train_op=train_op,
        loss = gen_loss,
        training_hooks=[logging_hook]
    )


def SRAttenFixGAN(features, labels, mode , params):
    '''
    D1 pretrain 关注
    G 同理
    :param features:
    :param labels:
    :param mode:
    :param params:
    :return:
    '''
    global_step = tf.train.get_global_step()
    with tf.variable_scope("input_pipeline"):
        org_img = features['org_image']
        ske_img = features['ske_image']
        ske_img = tf.expand_dims(ske_img,axis=-1)
        is_pretrain = params['pre_train']
        if len(org_img.shape)<4:
            org_img = tf.expand_dims(org_img,axis=0)
            ske_img = tf.expand_dims(ske_img,axis=0)
    # 确定模式
    # mode_str = 'test'
    # if mode == tf.estimator.ModeKeys.TRAIN:
    #     mode_str = 'train'
    # elif mode == tf.estimator.ModeKeys.EVAL:
    #     mode_str = 'eval'
    mode_str = 'train'


    # model
    with tf.variable_scope("generator"):
        with tf.name_scope('cgan_G'):
            out_channels = int(ske_img.shape[-1])
            cgan_G_out = compon.cgan_unet_g(org_img, out_channels,mode_str=='train')
            # change in ver0.2
            out_G_feature = cgan_G_out['output_feature']
            out_G_stop_gradent = tf.stop_gradient(out_G_feature)    # change in ver 0.3 防止更新P2P
            out_G_rgb = cgan_G_out['output_rgb']
        with tf.name_scope('input_conv'):
            org_conv_sim = compon.simple_double_conv(org_img,int(out_G_feature.get_shape()[-1]))
        # adding
        adder = (out_G_stop_gradent+org_conv_sim)/2
        with tf.name_scope('output_conv'):
            # output = compon.simple_double_conv(adder,out_channels)  # change in ver0.2
            output = tf.layers.conv2d(adder,1,kernel_size=5,strides=1,padding='same',activation=tf.nn.leaky_relu)
    # predict output
    if mode == tf.estimator.ModeKeys.PREDICT:
        predictions = {
            'gen_image': output,
            'org_image':org_img,
            'ske_image':ske_img,
            'pix2pix_out':out_G_rgb
        }
        return tf.estimator.EstimatorSpec(mode,predictions=predictions)
    # D
    with tf.name_scope("real_discriminator"):
        with tf.variable_scope("discriminator_2"):
            # 2x [batch, height, width, channels] => [batch, 30, 30, 1]
            D2_predict_real = compon.cgan_patch_d(org_img, ske_img)
        # change in ver0.2
        with tf.variable_scope("discriminator_1"):
            # 2x [batch, height, width, channels] => [batch, 30, 30, 1]
            D1_predict_real = compon.cgan_patch_d(org_img, ske_img)
    with tf.name_scope("fake_discriminator"):
        with tf.variable_scope("discriminator_2", reuse=True):
            # 2x [batch, height, width, channels] => [batch, 30, 30, 1]
            D2_predict_fake = compon.cgan_patch_d(org_img, output)
        with tf.variable_scope("discriminator_1", reuse=True):
            # 2x [batch, height, width, channels] => [batch, 30, 30, 1]
            D1_predict_fake = compon.cgan_patch_d(org_img, out_G_rgb)
    # calc loss
    with tf.name_scope("discriminator_loss"):
        # minimizing -tf.log will try to get inputs to 1
        # predict_real => 1
        # predict_fake => 0
        discrim_loss_2 = tf.reduce_mean(-(tf.log(D2_predict_real + args.EPS) + tf.log(1 - D2_predict_fake + args.EPS)))
        discrim_loss_1 = tf.reduce_mean(-(tf.log(D1_predict_real + args.EPS) + tf.log(1 - D1_predict_fake + args.EPS)))

    with tf.name_scope("generator_loss"):
        # predict_fake => 1
        # abs(targets - outputs) => 0
        gen_loss_GAN_2 = tf.reduce_mean(-tf.log(D2_predict_fake + args.EPS))
        gen_loss_L1_2 = tf.reduce_mean(tf.abs(ske_img - output))
        gen_loss_total_2 = gen_loss_GAN_2 * a.gan_weight + gen_loss_L1_2 * a.l1_weight
        # change in ver0.2
        gen_loss_GAN_1 = tf.reduce_mean(-tf.log(D1_predict_fake + args.EPS))
        gen_loss_L1_1 = tf.reduce_mean(tf.abs(ske_img - out_G_rgb))
        gen_loss_total_1 = gen_loss_GAN_2 * a.gan_weight + gen_loss_L1_1 * a.l1_weight
        def loss_total_true(): return gen_loss_total_1
        def loss_total_false(): return gen_loss_total_2
        gen_loss_total = tf.cond( is_pretrain > global_step,loss_total_true,loss_total_false)
    ema = tf.train.ExponentialMovingAverage(decay=0.99)

    # summary
    def summary_true(): return output
    def summary_false(): return out_G_rgb
    gen_img = tf.cond(global_step>is_pretrain,true_fn=summary_true,false_fn=summary_false )
    ## img
    tf.summary.image('gen_image',op.de_max_min_rgb(gen_img))
    tf.summary.image('org_image',op.de_max_min_rgb(org_img))
    tf.summary.image('ske_image',op.de_max_min_rgb(ske_img))
    ## scalar
    tf.summary.scalar('gen_loss_2',gen_loss_GAN_2)
    tf.summary.scalar('l1_loss_2',gen_loss_L1_2)
    tf.summary.scalar('gen_loss_total',gen_loss_total)
    tf.summary.scalar('gen_loss_1', gen_loss_GAN_1)
    tf.summary.scalar('l1_loss_1', gen_loss_L1_1)
    tf.summary.scalar('disc_loss_2',discrim_loss_2)
    tf.summary.scalar('disc_loss_1', discrim_loss_1)
    evaling_tensors={
            'gen_loss_2': gen_loss_GAN_2,
            'l1_loss_2': gen_loss_L1_2,
            'disc_loss_2': discrim_loss_2,
            'gen_loss_1': gen_loss_GAN_1,
            'l1_loss_1': gen_loss_L1_1,
            'gen_loss_total': gen_loss_total,
            'disc_loss_1': discrim_loss_1,
        }
    # evaling
    if mode==tf.estimator.ModeKeys.EVAL:
        metrics = {
            'gen_loss_2':tf.metrics.mean(gen_loss_GAN_2),
            'l1_loss_2':tf.metrics.mean(gen_loss_L1_2),
            'gen_loss_total_2':tf.metrics.mean(gen_loss_total_2),
            'disc_loss_2':tf.metrics.mean(discrim_loss_2),
            'gen_loss_1':tf.metrics.mean(gen_loss_GAN_1),
            'l1_loss_1':tf.metrics.mean(gen_loss_L1_1),
            'gen_loss_total_1':tf.metrics.mean(gen_loss_total_1),
            'disc_loss_1':tf.metrics.mean(discrim_loss_1),

        }
        eval_logging_hook = tf.train.LoggingTensorHook(tensors=evaling_tensors,at_end=True,every_n_iter=1)
        return tf.estimator.EstimatorSpec(mode,eval_metric_ops=metrics,loss=gen_loss_total_1+gen_loss_total_2,
                                          evaluation_hooks=[eval_logging_hook])

    # trainning
    assert mode == tf.estimator.ModeKeys.TRAIN
    update_losses = ema.apply([discrim_loss_1, gen_loss_GAN_1, gen_loss_L1_1,discrim_loss_2, gen_loss_GAN_2, gen_loss_L1_2])
    # 滑动learning rate

    with tf.name_scope("discriminator_train"):
        def train_fn():   # 非pretrain
            discrim_tvars = [var for var in tf.trainable_variables() if var.name.startswith("discriminator_2")] #更d2
            discrim_optim = tf.train.AdamOptimizer(a.d_lr, a.beta1)
            discrim_train = discrim_optim.minimize(loss=discrim_loss_2,var_list=discrim_tvars
                                                      ,global_step=global_step)
            return discrim_train
        def pre_train_fn():  #pretrain
            discrim_tvars = [var for var in tf.trainable_variables() if var.name.startswith("discriminator_1")] #更d1
            discrim_optim = tf.train.AdamOptimizer(a.d_lr, a.beta1)
            discrim_train = discrim_optim.minimize(loss=discrim_loss_1, var_list=discrim_tvars
                                                   , global_step=global_step)
            return discrim_train
        discrim_train = tf.cond(global_step>is_pretrain,true_fn=train_fn,false_fn=pre_train_fn)

    with tf.name_scope("generator_train"):
        with tf.control_dependencies([discrim_train]):
            gen_tvars = [var for var in tf.trainable_variables() if var.name.startswith("generator")]
            gen_optim = tf.train.AdamOptimizer(a.g_lr, a.beta1)
            # g的训练开关
            gen_train = gen_optim.minimize(loss=gen_loss_total,var_list=gen_tvars,
                                           global_step=global_step)



    train_op = tf.group(update_losses,gen_train)
    training_logging_hook = tf.train.LoggingTensorHook(
        tensors=evaling_tensors,
        every_n_iter=10
    )

    return tf.estimator.EstimatorSpec(
        mode,
        training_hooks=[training_logging_hook],
        train_op=train_op,
        loss=gen_loss_total
    )

def SRAttenFixGAN_with_con(features,labels,mode,params):
    '''
        D1 pretrain 关注
        G 同理
        :param features:
        :param labels:
        :param mode:
        :param params:
        :return:
        '''
    global_step = tf.train.get_global_step()
    with tf.variable_scope("input_pipeline"):
        org_img = features['org_image']
        ske_img = features['ske_image']
        ske_img = tf.expand_dims(ske_img, axis=-1)
        is_pretrain = params['pre_train']
        if len(org_img.shape) < 4:
            org_img = tf.expand_dims(org_img, axis=0)
            ske_img = tf.expand_dims(ske_img, axis=0)
    # 确定模式
    mode_str = 'test'
    if mode == tf.estimator.ModeKeys.TRAIN:
        mode_str = 'train'
    elif mode == tf.estimator.ModeKeys.EVAL:
        mode_str = 'eval'


    # model
    with tf.variable_scope("generator"):
        with tf.name_scope('cgan_G'):
            out_channels = int(ske_img.shape[-1])
            cgan_G_out = compon.cgan_unet_g(org_img, out_channels, mode_str == 'train')
            # change in ver0.2
            out_G_feature = cgan_G_out['output_feature']
            out_G_stop_gradent = tf.stop_gradient(out_G_feature)  # change in ver 0.3 防止更新P2P
            out_G_rgb = cgan_G_out['output_rgb']
        with tf.name_scope('input_conv'):
            org_conv_sim = compon.simple_double_conv(org_img, int(out_G_feature.get_shape()[-1]))
        # adding
        adder = out_G_stop_gradent + org_conv_sim

        with tf.name_scope('output_conv'):
            # output = compon.simple_double_conv(adder,out_channels)  # change in ver0.2
            # output = compon.out_rgb(adder,int(ske_img.get_shape()[-1]))
            output = compon.enhance_out_conv(adder,int(ske_img.get_shape()[-1]))
    # 旁路 判断插值
        with tf.variable_scope('detail_checker'):
            y_e = compon.out_rgb(org_conv_sim,ske_img.get_shape()[-1])


    # predict output
    if mode == tf.estimator.ModeKeys.PREDICT:
        predictions = {
            'gen_image': output,
            'org_image': org_img,
            'ske_image': ske_img,
            'y_e': y_e
        }
        return tf.estimator.EstimatorSpec(mode, predictions=predictions)
    # D
    with tf.name_scope("real_discriminator"):
        with tf.variable_scope("discriminator_2"):
            # 2x [batch, height, width, channels] => [batch, 30, 30, 1]
            D2_predict_real = compon.cgan_patch_d(org_img, ske_img)
        # change in ver0.2
        with tf.variable_scope("discriminator_1"):
            # 2x [batch, height, width, channels] => [batch, 30, 30, 1]
            D1_predict_real = compon.cgan_patch_d(org_img, ske_img)
    with tf.name_scope("fake_discriminator"):
        with tf.variable_scope("discriminator_2", reuse=True):
            # 2x [batch, height, width, channels] => [batch, 30, 30, 1]
            D2_predict_fake = compon.cgan_patch_d(org_img, output)
        with tf.variable_scope("discriminator_1", reuse=True):
            # 2x [batch, height, width, channels] => [batch, 30, 30, 1]
            D1_predict_fake = compon.cgan_patch_d(org_img, out_G_rgb)
    # calc loss
    with tf.name_scope("discriminator_loss"):
        # minimizing -tf.log will try to get inputs to 1
        # predict_real => 1
        # predict_fake => 0
        discrim_loss_2 = tf.reduce_mean(-(tf.log(D2_predict_real + args.EPS) + tf.log(1 - D2_predict_fake + args.EPS)))
        discrim_loss_1 = tf.reduce_mean(-(tf.log(D1_predict_real + args.EPS) + tf.log(1 - D1_predict_fake + args.EPS)))
        # discrim_loss_2 =(tf.reduce_mean((y_pred - torch.mean(y_pred_fake) - y) ** 2) + torch.mean((y_pred_fake - torch.mean(y_pred) + y) ** 2))/2

    with tf.name_scope("generator_loss"):
        # predict_fake => 1
        # abs(targets - outputs) => 0
        gen_loss_GAN_2 = tf.reduce_mean(-tf.log(D2_predict_fake + args.EPS))
        gen_loss_L1_2 = tf.reduce_mean(tf.abs(ske_img - output))
        gen_loss_total_2 = gen_loss_GAN_2 * a.gan_weight + gen_loss_L1_2 * a.l1_weight
        # change in ver0.2
        gen_loss_GAN_1 = tf.reduce_mean(-tf.log(D1_predict_fake + args.EPS))
        gen_loss_L1_1 = tf.reduce_mean(tf.abs(ske_img - out_G_rgb))
        gen_loss_total_1 = gen_loss_GAN_2 * a.gan_weight + gen_loss_L1_1 * a.l1_weight

        def loss_total_true(): return gen_loss_total_1

        def loss_total_false(): return gen_loss_total_2

        gen_loss_total = tf.cond(is_pretrain > global_step, loss_total_true, loss_total_false)

    with tf.name_scope("detail_loss"):
        out_G_rgb_stop = tf.stop_gradient(out_G_rgb) # 防止影响G
        detail_dis = y_e - (ske_img - out_G_rgb_stop)
        y_e_L1 = tf.reduce_sum(tf.abs(y_e))/tf.cast(tf.size(y_e),dtype=tf.float32)
        l2_detail_dis = tf.nn.l2_loss(detail_dis)/tf.cast(tf.size(detail_dis),dtype=tf.float32)
        tf.summary.histogram('y_e_L1',y_e_L1)
        tf.summary.histogram('l2_detail',l2_detail_dis)
        detail_loss = l2_detail_dis+a.detail_lambda*y_e_L1


    ema = tf.train.ExponentialMovingAverage(decay=0.99)

    # summary
    def summary_true():
        return output

    def summary_false():
        return out_G_rgb

    gen_img = tf.cond(global_step > is_pretrain, true_fn=summary_true, false_fn=summary_false)
    ## img
    tf.summary.image('gen_image', op.de_max_min_rgb(gen_img))
    tf.summary.image('org_image', op.de_max_min_rgb(org_img))
    tf.summary.image('ske_image', op.de_max_min_rgb(ske_img))
    tf.summary.image('ske-output', op.de_max_min_rgb(tf.abs(ske_img-output)))
    tf.summary.image('y_e', op.de_max_min_rgb(y_e))
    ## scalar
    tf.summary.scalar('gen_loss_2', gen_loss_GAN_2)
    tf.summary.scalar('l1_loss_2', gen_loss_L1_2)
    tf.summary.scalar('gen_loss_total', gen_loss_total)
    tf.summary.scalar('gen_loss_1', gen_loss_GAN_1)
    tf.summary.scalar('l1_loss_1', gen_loss_L1_1)
    tf.summary.scalar('disc_loss_2', discrim_loss_2)
    tf.summary.scalar('disc_loss_1', discrim_loss_1)
    tf.summary.scalar('detail_loss',detail_loss)
    evaling_tensors = {
        'gen_loss_2': gen_loss_GAN_2,
        'l1_loss_2': gen_loss_L1_2,
        'disc_loss_2': discrim_loss_2,
        'gen_loss_1': gen_loss_GAN_1,
        'l1_loss_1': gen_loss_L1_1,
        'gen_loss_total': gen_loss_total,
        'disc_loss_1': discrim_loss_1,
        'detail_loss':detail_loss
    }
    # evaling
    if mode == tf.estimator.ModeKeys.EVAL:
        metrics = {
            'gen_loss_2': tf.metrics.mean(gen_loss_GAN_2),
            'l1_loss_2': tf.metrics.mean(gen_loss_L1_2),
            'gen_loss_total': tf.metrics.mean(gen_loss_total),
            'disc_loss_2': tf.metrics.mean(discrim_loss_2),
            'gen_loss_1': tf.metrics.mean(gen_loss_GAN_1),
            'l1_loss_1': tf.metrics.mean(gen_loss_L1_1),
            'disc_loss_1': tf.metrics.mean(discrim_loss_1),
            'detail_loss':tf.metrics.mean(detail_loss)
        }
        eval_logging_hook = tf.train.LoggingTensorHook(tensors=evaling_tensors, at_end=True, every_n_iter=1)
        return tf.estimator.EstimatorSpec(mode, eval_metric_ops=metrics, loss=gen_loss_total_1 + gen_loss_total_2,
                                          evaluation_hooks=[eval_logging_hook])

    # trainning
    assert mode == tf.estimator.ModeKeys.TRAIN
    update_losses = ema.apply(
        [discrim_loss_1, gen_loss_GAN_1, gen_loss_L1_1, discrim_loss_2, gen_loss_GAN_2, gen_loss_L1_2])
    # 滑动learning rate
    d_lr = tf.train.exponential_decay(a.d_lr,global_step,5000,0.9,staircase=True)
    g_lr = tf.train.exponential_decay(a.g_lr,global_step,5000,0.9,staircase=True)
    with tf.name_scope("discriminator_train"):
        def train_fn():  # 非pretrain
            discrim_tvars = [var for var in tf.trainable_variables() if var.name.startswith("discriminator_2")]  # 更d2
            discrim_optim = tf.train.AdamOptimizer(d_lr, a.beta1)
            discrim_train = discrim_optim.minimize(loss=discrim_loss_2, var_list=discrim_tvars
                                                   , global_step=global_step)
            return discrim_train

        def pre_train_fn():  # pretrain
            discrim_tvars = [var for var in tf.trainable_variables() if var.name.startswith("discriminator_1")]  # 更d1
            discrim_optim = tf.train.AdamOptimizer(d_lr, a.beta1)
            discrim_train = discrim_optim.minimize(loss=discrim_loss_1, var_list=discrim_tvars
                                                   , global_step=global_step)
            return discrim_train

        discrim_train = tf.cond(global_step > is_pretrain, true_fn=train_fn, false_fn=pre_train_fn)

    with tf.name_scope("generator_train"):
        with tf.control_dependencies([discrim_train]):
            gen_tvars = [var for var in tf.trainable_variables() if var.name.startswith("generator")]
            gen_optim = tf.train.AdamOptimizer(g_lr, a.beta1)
            # g的训练开关
            gen_train = gen_optim.minimize(loss=gen_loss_total, var_list=gen_tvars,
                                           global_step=global_step)
    with tf.name_scope("confident_train"):
        with tf.control_dependencies([gen_train]):
            def train_fn():
                conf_vars = [var for var in tf.trainable_variables() if (('input_conv' in var.name)
                                                                         or ('detail_checker' in var.name))]
                conf_optim = tf.train.AdamOptimizer(g_lr,a.beta1)
                conf_train = conf_optim.minimize(loss=detail_loss,var_list=conf_vars,global_step=global_step)
                return conf_train
            def pre_train_fn():# 假更新
                conf_optim = tf.train.AdamOptimizer(0,a.beta1)
                conf_train = conf_optim.minimize(loss=detail_loss,var_list=None,global_step=global_step)
                return conf_train
            conf_train = tf.cond(global_step>is_pretrain,true_fn=train_fn,false_fn=pre_train_fn)

    train_op = tf.group(update_losses, gen_train,conf_train)
    training_logging_hook = tf.train.LoggingTensorHook(
        tensors=evaling_tensors,
        every_n_iter=10
    )

    return tf.estimator.EstimatorSpec(
        mode,
        training_hooks=[training_logging_hook],
        train_op=train_op,
        loss=gen_loss_total
    )