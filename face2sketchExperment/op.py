import time
from datetime import datetime
import tensorflow as tf
import args
import cv2
import numpy as np
from args import a  # import from output args



#### cgan
def discrim_conv(batch_input, out_channels, stride):
    padded_input = tf.pad(batch_input, [[0, 0], [1, 1], [1, 1], [0, 0]], mode="CONSTANT")
    return tf.layers.conv2d(padded_input, out_channels, kernel_size=4, strides=(stride, stride), padding="valid",
                            kernel_initializer=tf.random_normal_initializer(0, 0.02))


def gen_conv(batch_input, out_channels,kernel = 5):
    # [batch, in_height, in_width, in_channels] => [batch, out_height, out_width, out_channels]
    initializer = tf.random_normal_initializer(0, 0.02)
    if a.separable_conv:
        return tf.layers.separable_conv2d(batch_input, out_channels, kernel_size=kernel, strides=(2, 2), padding="same",
                                          depthwise_initializer=initializer, pointwise_initializer=initializer)
    else:
        return tf.layers.conv2d(batch_input, out_channels, kernel_size=kernel, strides=(2, 2), padding="same",
                                kernel_initializer=initializer)


def gen_deconv(batch_input, out_channels,kernel = 5):
    # [batch, in_height, in_width, in_channels] => [batch, out_height, out_width, out_channels]
    initializer = tf.random_normal_initializer(0, 0.02)
    if a.separable_conv:
        _b, h, w, _c = batch_input.shape
        resized_input = tf.image.resize_images(batch_input, [h * 2, w * 2],
                                               method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
        return tf.layers.separable_conv2d(resized_input, out_channels, kernel_size=kernel, strides=(1, 1),
                                          padding="same", depthwise_initializer=initializer,
                                          pointwise_initializer=initializer)
    else:
        return tf.layers.conv2d_transpose(batch_input, out_channels, kernel_size=kernel, strides=(2, 2), padding="same",
                                          kernel_initializer=initializer)


def batchnorm(inputs):
    return tf.layers.batch_normalization(inputs, axis=-1, epsilon=1e-5, momentum=0.1, training=True,
                                         gamma_initializer=tf.random_normal_initializer(1.0, 0.02))
def instance_norm(input,name="instance_norm"):
    with tf.variable_scope(name):
        depth = input.get_shape()[3]
        scale = tf.get_variable("scale", [depth],
                                    initializer=tf.random_normal_initializer(1.0, 0.02, dtype=tf.float32))
        offset = tf.get_variable("offset", [depth], initializer=tf.constant_initializer(0.0))
        mean, variance = tf.nn.moments(input, axes=[1, 2], keep_dims=True)
        epsilon = 1e-5
        inv = tf.rsqrt(variance + epsilon)
        normalized = (input - mean) * inv
        return scale * normalized + offset

def lrelu(x, a):
    with tf.name_scope("lrelu"):
        # adding these together creates the leak part and linear part
        # then cancels them out by subtracting/adding an absolute value term
        # leak: a*x/2 - a*abs(x)/2
        # linear: x/2 + abs(x)/2

        # this block looks like it has 2 inputs on the graph unless we do this
        x = tf.identity(x)
        return (0.5 * (1 + a)) * x + (0.5 * (1 - a)) * tf.abs(x)

####

def conv2d_with_pad(input, filter, padding, kernel_size, strides, name='', reuse=False):
    '''指定padding的conv2d,用于全卷积层downsampling
        pad 为一个Integer，定义了图像2,3维度pad的数量
    '''
    # 卷积
    conv = tf.layers.conv2d(input, filter, kernel_size, strides, name=name + '_conv', reuse=reuse)
    # padding
    pad_mat = [[0, 0], [padding, 0], [0, padding], [0, 0]]
    pad = tf.pad(conv, pad_mat, name=name + '_pad')
    return pad



def preprocess(image):
    with tf.name_scope("preprocess"):
        # [0, 1] => [-1, 1]
        return image * 2 - 1


def deprocess(image):
    with tf.name_scope("deprocess"):
        # [-1, 1] => [0, 1]
        return (image + 1) / 2


def preprocess_lab(lab):
    with tf.name_scope("preprocess_lab"):
        L_chan, a_chan, b_chan = tf.unstack(lab, axis=2)
        # L_chan: black and white with input range [0, 100]
        # a_chan/b_chan: color channels with input range ~[-110, 110], not exact
        # [0, 100] => [-1, 1],  ~[-110, 110] => [-1, 1]
        return [L_chan / 50 - 1, a_chan / 110, b_chan / 110]


def deprocess_lab(L_chan, a_chan, b_chan):
    with tf.name_scope("deprocess_lab"):
        # this is axis=3 instead of axis=2 because we process individual images but deprocess batches
        return tf.stack([(L_chan + 1) / 2 * 100, a_chan * 110, b_chan * 110], axis=3)




# based on https://github.com/torch/image/blob/9f65c30167b2048ecbe8b7befdc6b2d6d12baee9/generic/image.c
def rgb_to_lab(srgb):
    with tf.name_scope("rgb_to_lab"):
        srgb_pixels = tf.reshape(srgb, [-1, 3])

        with tf.name_scope("srgb_to_xyz"):
            linear_mask = tf.cast(srgb_pixels <= 0.04045, dtype=tf.float32)
            exponential_mask = tf.cast(srgb_pixels > 0.04045, dtype=tf.float32)
            rgb_pixels = (srgb_pixels / 12.92 * linear_mask) + (
                    ((srgb_pixels + 0.055) / 1.055) ** 2.4) * exponential_mask
            rgb_to_xyz = tf.constant([
                #    X        Y          Z
                [0.412453, 0.212671, 0.019334],  # R
                [0.357580, 0.715160, 0.119193],  # G
                [0.180423, 0.072169, 0.950227],  # B
            ])
            xyz_pixels = tf.matmul(rgb_pixels, rgb_to_xyz)

        # https://en.wikipedia.org/wiki/Lab_color_space#CIELAB-CIEXYZ_conversions
        with tf.name_scope("xyz_to_cielab"):
            # convert to fx = f(X/Xn), fy = f(Y/Yn), fz = f(Z/Zn)

            # normalize for D65 white point
            xyz_normalized_pixels = tf.multiply(xyz_pixels, [1 / 0.950456, 1.0, 1 / 1.088754])

            epsilon = 6 / 29
            linear_mask = tf.cast(xyz_normalized_pixels <= (epsilon ** 3), dtype=tf.float32)
            exponential_mask = tf.cast(xyz_normalized_pixels > (epsilon ** 3), dtype=tf.float32)
            fxfyfz_pixels = (xyz_normalized_pixels / (3 * epsilon ** 2) + 4 / 29) * linear_mask + (
                    xyz_normalized_pixels ** (1 / 3)) * exponential_mask

            # convert to lab
            fxfyfz_to_lab = tf.constant([
                #  l       a       b
                [0.0, 500.0, 0.0],  # fx
                [116.0, -500.0, 200.0],  # fy
                [0.0, 0.0, -200.0],  # fz
            ])
            lab_pixels = tf.matmul(fxfyfz_pixels, fxfyfz_to_lab) + tf.constant([-16.0, 0.0, 0.0])

        return tf.reshape(lab_pixels, tf.shape(srgb))


def lab_to_rgb(lab):
    with tf.name_scope("lab_to_rgb"):
        lab = check_image(lab)
        lab_pixels = tf.reshape(lab, [-1, 3])

        # https://en.wikipedia.org/wiki/Lab_color_space#CIELAB-CIEXYZ_conversions
        with tf.name_scope("cielab_to_xyz"):
            # convert to fxfyfz
            lab_to_fxfyfz = tf.constant([
                #   fx      fy        fz
                [1 / 116.0, 1 / 116.0, 1 / 116.0],  # l
                [1 / 500.0, 0.0, 0.0],  # a
                [0.0, 0.0, -1 / 200.0],  # b
            ])
            fxfyfz_pixels = tf.matmul(lab_pixels + tf.constant([16.0, 0.0, 0.0]), lab_to_fxfyfz)

            # convert to xyz
            epsilon = 6 / 29
            linear_mask = tf.cast(fxfyfz_pixels <= epsilon, dtype=tf.float32)
            exponential_mask = tf.cast(fxfyfz_pixels > epsilon, dtype=tf.float32)
            xyz_pixels = (3 * epsilon ** 2 * (fxfyfz_pixels - 4 / 29)) * linear_mask + (
                    fxfyfz_pixels ** 3) * exponential_mask

            # denormalize for D65 white point
            xyz_pixels = tf.multiply(xyz_pixels, [0.950456, 1.0, 1.088754])

        with tf.name_scope("xyz_to_srgb"):
            xyz_to_rgb = tf.constant([
                #     r           g          b
                [3.2404542, -0.9692660, 0.0556434],  # x
                [-1.5371385, 1.8760108, -0.2040259],  # y
                [-0.4985314, 0.0415560, 1.0572252],  # z
            ])
            rgb_pixels = tf.matmul(xyz_pixels, xyz_to_rgb)
            # avoid a slightly negative number messing up the conversion
            rgb_pixels = tf.clip_by_value(rgb_pixels, 0.0, 1.0)
            linear_mask = tf.cast(rgb_pixels <= 0.0031308, dtype=tf.float32)
            exponential_mask = tf.cast(rgb_pixels > 0.0031308, dtype=tf.float32)
            srgb_pixels = (rgb_pixels * 12.92 * linear_mask) + (
                    (rgb_pixels ** (1 / 2.4) * 1.055) - 0.055) * exponential_mask

        return tf.reshape(srgb_pixels, tf.shape(lab))


def random_crop(noise_mat,gt_mat,size):
    i_w = noise_mat.shape[0]
    i_h = noise_mat.shape[1]
    ram_x = np.random.randint(0,i_w-size)
    ram_y = np.random.randint(0,i_h-size)
    return (noise_mat[ram_x:ram_x+size,ram_y:ram_y+size,:],
           gt_mat[ram_x:ram_x+size,ram_y:ram_y+size,:])


def max_min(mat):
    mat = mat - 128
    return mat / 255


def resize_img_with_padding(img_block,size):
    '''
        等比例缩放图片，与某个宽高不同的等比例resize后 短边做0padding 补足
    :param img_block:
    :return:
    '''
    img_shape = np.shape(img_block)
    rot_flag = False
    # if size[0]<size[1]:
    #     size = np.rot90(size)
    # if img_shape[0]<img_shape[1] :
    #     img_block = np.rot90(img_block)
    #     rot_flag = True
    # size = np.array(size,dtype=np.float32)
    # img_size = np.array([img_block.shape[0],img_block.shape[1]],dtype=np.float32)
    # img_ratio = size/img_size
    # tag_ratio = 1.
    # if img_ratio.min()>1:   # 需要放大
    #     # 按小倍数扩张
    #     tag_ratio = img_ratio.min()
    # elif img_ratio.max()<1: # 需要缩小
    #     # 按大倍数缩小
    #     tag_ratio = img_ratio.max()
    # img_block = cv2.resize(img_block,None,fx=tag_ratio,fy=tag_ratio,interpolation=cv2.INTER_AREA)
    img_shape = np.array([img_block.shape[0],img_block.shape[1]])
    padding_mat = size - img_shape
    img_block = cv2.copyMakeBorder(img_block, 0, int(padding_mat[0]), 0, int(padding_mat[1]), borderType=cv2.BORDER_WRAP)
    if rot_flag is True:
        img_block = np.rot90(img_block,3) # 转回去
    return img_block

def calculate_psnr_tensor(orig, proc):
    diff = tf.cast(tf.subtract(orig,proc),tf.int32)  # 转int
    mse = tf.cast(tf.reduce_sum(diff * diff,axis=[1,2,3]),tf.float32) / tf.reduce_sum(orig,axis=[1,2,3]) # 通过reducesum得到一行长度
    psnr = 10 * tf.log(255.0 * 255.0 / mse)
    return psnr

def min_max_regular_rgb(pic_block): # 使用 min_max算法归一化
    with tf.variable_scope('min_max_regular'):
        max_data = 255
        min_data = 0
        pic_block = (pic_block - min_data) / (max_data - min_data)
        pic_block = pic_block * 2 - 1
    return pic_block


def de_max_min_rgb(mat):
    with tf.name_scope('de_min_max'):
        max_data = 255
        min_data = 0
        mat = (mat+1)/2
        mat = mat*(max_data-min_data)+min_data
    return mat