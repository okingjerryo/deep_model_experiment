import torch
import os
import cv2
import numpy as np
from torch.autograd import Variable
import face2ske_torch.model as model_repo
from torch.utils.data import DataLoader
import face2ske_torch.args as args
import face2ske_torch.data as data
from face2ske_torch.args import a
from face2ske_torch.op import denorm_to_rgb as drgb
import face2ske_torch.op as op
import face2ske_torch.print_util as pu
import face2ske_torch.fid_score as fid_util
import tqdm
import time


from tensorboardX import SummaryWriter
def data_pipe(mode):
    if mode is 'train':
        train_set = data.DatasetFromFolder(a, 256, infofile=a.infofile[0], forTrain=True,
                                      zeroPadding=a.zeroPadding)
        data_loader = DataLoader(dataset=train_set, num_workers=args.NUM_WORK, batch_size=a.batch_size,
                                          shuffle=True,drop_last=True)
        return data_loader
    elif mode is 'eval':
        test_set = data.DatasetFromFolder(a, 256, infofile=a.infofile[1], forTrain=False,
                                     zeroPadding=a.zeroPadding)
        data_loader = DataLoader(dataset=test_set, num_workers=args.NUM_WORK, batch_size=a.batch_size, shuffle=True,drop_last=True)
        return data_loader
    else:
        test_set = data.DatasetFromFolder(a, 256, infofile=a.infofile[1], forTrain=False,
                                          zeroPadding=a.zeroPadding)
        data_loader = DataLoader(dataset=test_set, num_workers=args.NUM_WORK, batch_size=20, shuffle=False)
        return data_loader



def print_result(result,epoch,mode,peroid,i_batch = None):
    d_loss = result['d_loss'].detach()
    g_loss_total = result['g_loss_total'].detach()
    g_loss_l1 = result['g_loss_l1'].detach()
    if mode is 'train':
        print("INFO:{} trainning [{}/5000]:{} D_loss: {},G_loss_total: {},G_loss_L1:{}".format(
            pu.to_red(peroid),pu.to_bule(epoch),pu.to_yellow(i_batch),
            pu.to_bule('{:0.4f}'.format(d_loss.item())),pu.to_red('{:0.4f}'.format(g_loss_total.item())),
            pu.to_bule('{:0.4f}'.format(g_loss_l1.item()))),
           )

    elif mode is 'eval':
        fid = result['fid']
        print("INFO:{} evaling result [{}/5000]:FID:{}  D_loss: {},G_loss_total: {},G_loss_L1:{}".format(
            pu.to_red(peroid),pu.to_bule(epoch),pu.to_yellow('{:0.4f}'.format(fid)),
            pu.to_bule('{:0.4f}'.format(d_loss.item())),pu.to_red('{:0.4f}'.format(g_loss_total.item())),
            pu.to_bule('{:0.4f}'.format(g_loss_l1.item())))
        )


def load_params(model):
    path = args.CHECKPOINT_PATH + 'best.pkl'
    if os.path.exists(path):
        model.load_state_dict(torch.load(path))
def save_params(model):
    if not os.path.exists(args.CHECKPOINT_PATH):
        os.makedirs(args.CHECKPOINT_PATH,True)
    path = args.CHECKPOINT_PATH + 'best.pkl'
    torch.save(model.state_dict(), path)


def save_pretrain(model):
    if not os.path.exists(args.CHECKPOINT_PATH):
        os.makedirs(args.CHECKPOINT_PATH,True)
    gen_path = args.CHECKPOINT_PATH + 'pretrain_gen.pkl'
    dis_path = args.CHECKPOINT_PATH + 'pretrain_dis.pkl'
    torch.save(model.generator.state_dict(),gen_path)
    torch.save(model.discrimnator.state_dict(),dis_path)

def load_pretrain(model):
    # 只load 两个组件
    gen_path = args.CHECKPOINT_PATH + 'pretrain_gen.pkl'
    dis_path = args.CHECKPOINT_PATH + 'pretrain_dis.pkl'
    if os.path.exists(gen_path):
        model.generator.load_state_dict(torch.load(gen_path))
    if os.path.exists(dis_path):
        model.discrimnator.load_state_dict(torch.load(dis_path))

def p2p_main():
    train_dataloader = data_pipe('train')
    eval_dataloader = data_pipe('eval')
    model = model_repo.pix2pix_our_in()
    writer = SummaryWriter(comment='train')
    eval_writer = SummaryWriter(comment='eval')
    test_writer = SummaryWriter(comment='test')
    best_fid = 5e10
    # 加载最新模型
    load_params(model)
    this_step = 0
    # pretrain
#     start_time = time.time()
#     # for epoch in range(650):
#     for epoch in range(10000):
#         model.train(True)  # 必须set这里！！
#         for i_batch, sample_batched in enumerate(train_dataloader):
#             org_image, ske_image = Variable(sample_batched[0]), Variable(sample_batched[1])
#             ske_image = ske_image.to(args.device_cuda)
#             org_image = org_image.to(args.device_cuda)
#             # set mode
#             result = model(org_image, ske_image)
#             if i_batch%30 is 0:
#                 print_result(result, epoch, i_batch=i_batch, mode='train', peroid='traing')
#                 end_time = time.time()
#                 time_delta = op.usedtime(start_time, end_time)
#                 print("used_time: {}".format(pu.to_yellow(time_delta)))
#             # summary
#             this_step += 1
#             if this_step % 100 == 0:
#                 g_loss_l1 = result['g_loss_l1']
#                 d_loss = result['d_loss']
#                 g_loss_total = result['g_loss_total']
#
#                 ske_image_re = result['ske_image']
#                 org_image_re = result['org_image']
#                 gen_image_re = result['gen_image']
#                 writer.add_scalar('loss/g_loss_l1', g_loss_l1, this_step)
#                 writer.add_scalar('loss/g_loss_total', g_loss_total, this_step)
#                 writer.add_scalar('loss/d_loss', d_loss, this_step)
#                 writer.add_image('train/gen_img', drgb(gen_image_re), this_step)
#                 writer.add_image('train/ske_img', drgb(ske_image_re), this_step)
#                 writer.add_image('train/org_img', drgb(org_image_re), this_step)
#
#         # eval and save checkpoint
#             if this_step % 1000 == 0:
#                 model.eval()
#                 d_loss = 0
#                 g_loss_total = 0
#                 g_loss_l1 = 0
#                 num_total = 0
#                 fid = 0
#                 for i_batch, sample_batched in enumerate(eval_dataloader):
#                     if i_batch is 10:
#                         break
#                     org_image, ske_image = Variable(sample_batched[0]), Variable(sample_batched[1])
#                     ske_image = ske_image.to(args.device_cuda)
#                     org_image = org_image.to(args.device_cuda)
#                     # set mode
#                     result = model(org_image, ske_image)
#                     print("INFO: evaling [{} - {}]".format(
#                         pu.to_magenta(i_batch * args.BATCH_SIZE), pu.to_cyan((i_batch + 1) * args.BATCH_SIZE)))
#                     num_total += 1
#                     d_loss += result['d_loss']
#                     g_loss_l1 += result['g_loss_l1']
#                     g_loss_total += result['g_loss_total']
#                     # calc fid
#
#                     gen_img = result['gen_image']
#                     t_fid = fid_util.calculate_fid_given_pics(org_mat=ske_image.to(args.device_cuda),
#                                                               gen_mat=gen_img.to(args.device_cuda))
#                     fid += t_fid
#
#                     ske_image_re = result['ske_image']
#                     org_image_re = result['org_image']
#                     gen_image_re = result['gen_image']
#                     eval_writer.add_image('eval/gen_img', drgb(gen_image_re), this_step)
#                     eval_writer.add_image('eval/ske_img', drgb(ske_image_re), this_step)
#                     eval_writer.add_image('eval/org_img', drgb(org_image_re), this_step)
#
#                 eval_result = {
#                     'd_loss': d_loss / num_total,
#                     'g_loss_total': g_loss_total / num_total,
#                     'g_loss_l1': g_loss_l1 / num_total,
#                     'fid': fid / num_total
#                 }
#
#                 eval_writer.add_scalar('loss/g_loss_l1', g_loss_l1 / num_total, this_step)
#                 eval_writer.add_scalar('loss/g_loss_total', g_loss_total / num_total, this_step)
#                 eval_writer.add_scalar('loss/d_loss', d_loss / num_total, this_step)
#                 eval_writer.add_scalar('loss/FID_score', fid / num_total, this_step)
#                 print_result(eval_result, epoch, mode='eval', peroid='training')
#                 if this_step is 5000:
#                     if os.path.exists(args.CHECKPOINT_PATH + 'best.pkl'):
#                         best_fid = fid / num_total
#                 if fid/num_total < best_fid :
#                 # 存储参数
#                     save_params(model)
#                     best_fid = fid / num_total
#                     print('INFO: new best model found!, now best loss {}'.format(pu.to_bule(best_fid)))
#     # 跑完第0个pretrain 把pretrain模型存下来
#
#
# # testting
#     writer.close()
#     eval_writer.close()
#     # 恢复前面验证集得到的最佳模型
#     load_params(model)
    test_dataloader = data_pipe('test')
    test_l = len(test_dataloader)
    model.eval()
    result_list = []
    fid = 0
    iter = 0
    for i, sample in enumerate(test_dataloader):
        print("testing ... [{}/{}]".format(i + 1, test_l))
        org_image, ske_image = Variable(sample[0]), Variable(sample[1])
        ske_image = ske_image.to(args.device_cuda)
        org_image = org_image.to(args.device_cuda)
        result = model(org_image, ske_image)
        ske_image_re = result['ske_image'].detach()
        org_image_re = result['org_image'].detach()
        gen_image_re = result['gen_image'].detach()
        t_fid = fid_util.calculate_fid_given_pics(org_mat=ske_image.to(args.device_cuda),
                                                  gen_mat=gen_image_re.to(args.device_cuda))
        fid += t_fid
        iter += 1
        test_writer.add_image('test/gen_img', drgb(gen_image_re), i)
        test_writer.add_image('test/ske_img', drgb(ske_image_re), i)
        test_writer.add_image('test/org_img', drgb(org_image_re), i)

        result_list.append(result)
    print('result FID avg:{}'.format(pu.to_red(fid / iter)))
    save_result_to_disk(result_list, 'result_pix2pix_ourin')

def SR_main():
    train_dataloader = data_pipe('train')
    eval_dataloader = data_pipe('eval')
    model = model_repo.SR_Dpath_without_Att()
    writer = SummaryWriter(comment='train')
    eval_writer = SummaryWriter(comment='eval')
    test_writer = SummaryWriter(comment='test')
    best_fid = 5e10
    # 加载最新模型
    load_params(model)
    model.set_pretrain(True)
    this_step = 0
    # pretrain
    trainning_epoch = [5000,5000,850]
    peroid = ['pre-train','train','fine_train']
    start_time = time.time()
    for state in range(0,3):
        model.g_optim = None    #每个阶段 训练的G都不相同
        if state is 0: # 三个训练阶段
            model.set_pretrain(True)
            model.set_finetrain(True)
        elif state is 1:
            model.set_pretrain(False)
        else:
            model.set_finetrain(True)
        for epoch in range(trainning_epoch[state]):
            model.set_train_mode('train')
            model.train(True) # 必须set这里！！
            for i_batch,sample_batched in enumerate(train_dataloader):
                org_image, ske_image = Variable(sample_batched[0]), Variable(sample_batched[1])
                ske_image = ske_image.to(args.device_cuda)
                org_image = org_image.to(args.device_cuda)
                # set mode
                result = model(org_image,ske_image)
                if this_step%30 is 0:
                    end_time = time.time()
                    time_delta = op.usedtime(start_time, end_time)
                    print_result(result,epoch,i_batch=i_batch,mode='train',peroid=peroid[state])
                    print("used_time: {}".format(pu.to_yellow(time_delta)))
                # summary
                this_step +=1
                if this_step%100 == 0:
                    g_loss_l1 =result['g_loss_l1']
                    d_loss =result['d_loss']
                    g_loss_total =result['g_loss_total']
                    enhance_loss =result['enhance_loss']

                    ske_image_re =result['ske_image']
                    org_image_re =result['org_image']
                    gen_image_re =result['gen_image']
                    enhance_image_re =result['enhance_image']
                    enhance_checker_re =result['enhance_checker']
                    writer.add_scalar('loss/g_loss_l1',g_loss_l1,this_step)
                    writer.add_scalar('loss/g_loss_total',g_loss_total,this_step)
                    writer.add_scalar('loss/d_loss',d_loss,this_step)
                    writer.add_scalar('loss/enhance_loss',enhance_loss,this_step)
                    writer.add_image('train/gen_img',drgb(gen_image_re),this_step)
                    writer.add_image('train/ske_img',drgb(ske_image_re),this_step)
                    writer.add_image('train/org_img',drgb(org_image_re),this_step)
                    writer.add_image('train/enhance_img',drgb(enhance_image_re),this_step)
                    writer.add_image('train/en_check_img',drgb(enhance_checker_re),this_step)

                # eval and save checkpoint
                if this_step % 1000 == 0:
                    model.eval()
                    model.set_train_mode('eval')
                    d_loss = 0
                    g_loss_total = 0
                    g_loss_l1 = 0
                    num_total = 0
                    enhance_total = 0
                    fid = 0
                    for i_batch, sample_batched in enumerate(eval_dataloader):
                        if i_batch is 3:
                            break
                        ske_image = sample_batched[1].to(args.device_cuda)
                        org_image = sample_batched[0].to(args.device_cuda)
                        # set mode
                        result = model(org_image, ske_image)
                        print("INFO: evaling [{} - {}]".format(
                            pu.to_magenta(i_batch*args.BATCH_SIZE),pu.to_cyan((i_batch+1)*args.BATCH_SIZE)))
                        num_total+=1
                        d_loss+=result['d_loss']
                        g_loss_l1+=result['g_loss_l1']
                        g_loss_total+=result['g_loss_total']
                        enhance_total += result['enhance_loss']
                        #calc fid
                        if state is 0:
                            gen_img = result['gen_image']
                        else:
                            gen_img = result['enhance_image']
                        t_fid = fid_util.calculate_fid_given_pics(org_mat=ske_image.to(args.device_cuda),
                                                                  gen_mat=gen_img.to(args.device_cuda))
                        fid+=t_fid

                        ske_image_re = result['ske_image']
                        org_image_re = result['org_image']
                        gen_image_re = result['gen_image']
                        enhance_image_re = result['enhance_image']
                        enhance_checker_re = result['enhance_checker']
                        eval_writer.add_image('eval/gen_img', drgb(gen_image_re), this_step)
                        eval_writer.add_image('eval/ske_img', drgb(ske_image_re), this_step)
                        eval_writer.add_image('eval/org_img', drgb(org_image_re), this_step)
                        eval_writer.add_image('eval/enhance_img', drgb(enhance_image_re), this_step)
                        eval_writer.add_image('eval/en_check_img', drgb(enhance_checker_re), this_step)

                    eval_result = {
                        'd_loss':d_loss/num_total,
                        'g_loss_total':g_loss_total/num_total,
                        'g_loss_l1':g_loss_l1/num_total,
                        'enhance_loss':enhance_total/num_total,
                        'fid':fid/num_total
                   }


                    eval_writer.add_scalar('loss/g_loss_l1',g_loss_l1/num_total,this_step)
                    eval_writer.add_scalar('loss/g_loss_total',g_loss_total/num_total,this_step)
                    eval_writer.add_scalar('loss/d_loss',d_loss/num_total,this_step)
                    eval_writer.add_scalar('loss/enhance_loss',enhance_total/num_total,this_step)
                    eval_writer.add_scalar('loss/FID_score',fid/num_total,this_step)
                    print_result(eval_result,epoch,mode='eval',peroid=peroid[state])
                    if epoch is 10:
                        if os.path.exists(args.CHECKPOINT_PATH + 'best.pkl'):
                            best_fid = fid/num_total
                    if fid/num_total < best_fid :
                        # 存储参数
                        save_params(model)
                        best_fid = fid/num_total
                        print('INFO: new best model found!, now best loss {}'.format(pu.to_bule(best_fid)))
        # 跑完第0个pretrain 把pretrain模型存下来
        if state is 0:
            save_pretrain(model)

    # testting
    writer.close()
    eval_writer.close()
    # 恢复前面验证集得到的最佳模型
    load_params(model)
    test_dataloader = data_pipe('test')
    test_l = len(test_dataloader)
    model.set_train_mode('test')
    model.eval()
    result_list = []
    fid = 0
    iter = 0
    for i, sample in enumerate(test_dataloader):
        print("testing ... [{}/{}]".format(i+1,test_l))
        ske_image = sample[1].to(args.device_cuda)
        org_image = sample[0].to(args.device_cuda)
        result = model(org_image, ske_image)
        ske_image_re = result['ske_image'].detach()
        org_image_re = result['org_image'].detach()
        if model.in_pre_train is True:
            gen_image_re = result['gen_image'].detach()
        else:
            gen_image_re = result['enhance_image'].detach()
        t_fid = fid_util.calculate_fid_given_pics(org_mat=ske_image.to(args.device_cuda),
                                                  gen_mat=gen_image_re.to(args.device_cuda))
        fid += t_fid
        iter +=1
        test_writer.add_image('test/gen_img', drgb(gen_image_re), i)
        test_writer.add_image('test/ske_img', drgb(ske_image_re), i)
        test_writer.add_image('test/org_img', drgb(org_image_re), i)

        result_list.append(result)
    print('result FID avg:{}'.format(pu.to_red(fid/iter)))
    save_result_to_disk(result_list,'result_sr_ue_oa')


def save_result_to_disk(result_list,path):
    gen_list = []
    org_list = []
    ske_list = []
    for dic in result_list:
        gen = op.denorm_to_rgb(dic['gen_image'])*255
        for i in range(gen.size()[0]):
            gen_list.append(gen[i].numpy())
        # gen_list.append(gen[1].numpy())
        org = op.denorm_to_rgb(dic['org_image'])*255
        for i in range(gen.size()[0]):
            org_list.append(org[i].numpy())
        # org_list.append(org[1].numpy())
        ske = op.denorm_to_rgb(dic['ske_image'])*255
        for i in range(gen.size()[0]):
            ske_list.append(ske[i].numpy())
        # ske_list.append(ske[1].numpy())
    for i in tqdm.trange(len(gen_list)):
        gen_image = gen_list[i]
        org_image = org_list[i]
        ske_image = ske_list[i]
        # de padding
        org_image = op.depadding(org_image,[28,3])
        ske_image = op.depadding(ske_image,[28,3])
        gen_image = op.depadding(gen_image,[28,3])
        # transpose to normal
        org_image = op.chw2hwc(org_image)
        ske_image = op.chw2hwc(ske_image)
        gen_image = op.chw2hwc(gen_image)
        # ske_gray2rgb
        ske_image = cv2.cvtColor(ske_image,cv2.COLOR_GRAY2RGB)
        gen_image = cv2.cvtColor(gen_image,cv2.COLOR_GRAY2RGB)
        org_image = cv2.cvtColor(org_image,cv2.COLOR_BGR2RGB)
        merge_img = np.zeros(shape=(250,600,3))
        merge_img[:,0:200] = org_image
        merge_img[:,200:400] = ske_image
        merge_img[:,400:600] = gen_image
        if not os.path.exists(path):
            os.makedirs(path,exist_ok=True)
        cv2.imwrite("{}/{}.jpg".format(path,i+1),merge_img)


if __name__ == '__main__':
    SR_main()
    # p2p_main()