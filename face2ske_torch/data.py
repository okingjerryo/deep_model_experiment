# -*- coding: utf-8 -*-
# @Author: JacobShi777

import cv2
import os
import random
import numpy as np
import torch
import random
import scipy.io as sio
from torch.autograd import Variable
import torchvision.transforms as transforms
import torch.utils.data as data


class DatasetFromFolder(data.Dataset):
	def __init__(self, opt, imgResol, infofile, forTrain, zeroPadding):
		super(DatasetFromFolder, self).__init__()

		self.opt = opt
		self.imgResol = imgResol
		self.infofile = infofile
		self.forTrain = forTrain
		self.lens = self.getLines()
		self.zeroPadding = zeroPadding

	def __getitem__(self, index):
		line = self.lines[index]
		inputs, targets = self.loadImg(line, self.imgResol)

		return inputs, targets


	def __len__(self):
		return len(self.lines)

	def getLines(self):
		self.lines = []
		with open(self.infofile, 'r') as f:
			for line in f:
				line = line.strip()
				self.lines.append(line)
		lens = len(self.lines)

		return lens

	def loadImg(self, line, imgResol):
		items = line.split('||')
		inPath1 = os.path.join(self.opt.root, items[0])
		img1 = cv2.imread(inPath1)
		inPath2 = os.path.join(self.opt.root, items[1])
		img2 = cv2.imread(inPath2, cv2.IMREAD_GRAYSCALE)
		#img2 = cv2.imread(inPath2)
		if self.forTrain:
			img1, img2 = self.preprocess(img1, img2, imgResol)
		else:
			img1, img2 = self.preprocessTest(img1, img2, imgResol)
		#print ('--->img2',img2.shape)
		img2 = img2.reshape(img2.shape[0], img2.shape[1], 1)
		transform1 = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5),(0.5, 0.5, 0.5))])
		transform2 = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5),(0.5, 0.5, 0.5))])
		img1 = transform1(img1)
		img2 = transform2(img2)

		return img1, img2
	def preprocess(self, img1, img2, imgResol):
		loadSize = int(np.ceil(imgResol*1.117))
		if loadSize%2 == 1:
			loadSize += 1
		if self.zeroPadding:
			paddingImg1, paddingImg2 = np.zeros((286, 286, 3)), np.zeros((286, 286))
			paddingSize1, paddingSize2 = (286 - 250)//2, (286 - 200)//2
			img1 = np.lib.pad(img1,((paddingSize1,paddingSize1),(paddingSize2,paddingSize2),(0,0)), 'edge')
			img2 = np.lib.pad(img2, ((paddingSize1,paddingSize1),(paddingSize2,paddingSize2)), 'edge')	
		img1 = cv2.resize(img1, (loadSize, loadSize))
		img2 = cv2.resize(img2, (loadSize, loadSize))

		h_offset = random.randint(0, loadSize - imgResol)
		w_offset = random.randint(0, loadSize - imgResol)
		img1 = img1[h_offset:h_offset+imgResol, w_offset:w_offset+imgResol, :]
		img2 = img2[h_offset:h_offset+imgResol, w_offset:w_offset+imgResol]

		return img1, img2

	def preprocessTest(self, img1, img2, imgResol):
		if self.zeroPadding:
			paddingImg1, paddingImg2 = np.zeros((256, 256, 3)), np.zeros((256, 256))
			paddingSize1, paddingSize2 = (256 - 250)//2, (256 - 200)//2
			#print (img1[47:203,47:203,:])
			img1 = np.lib.pad(img1,((paddingSize1,paddingSize1),(paddingSize2,paddingSize2),(0,0)), 'edge')
			img2 = np.lib.pad(img2, ((paddingSize1,paddingSize1),(paddingSize2,paddingSize2)), 'edge')
		img1 = cv2.resize(img1, (imgResol, imgResol))
		img2 = cv2.resize(img2, (imgResol, imgResol))

		return img1, img2

def checkpaths(opt):
	if not os.path.exists(opt.checkpoint):
		os.mkdir(opt.checkpoint)
	if not os.path.exists(opt.gen_root):
		os.mkdir(opt.gen_root)

def checkpoint(epoch, netD, netG, it):
	netD_out_path = "./checkpoint/netD_resol_{}_{}.weight".format(epoch,it+1)
	torch.save(netD.state_dict(), netD_out_path)
	netG_out_path = "./checkpoint/netG_resol_{}_{}.weight".format(epoch,it+1)
	torch.save(netG.state_dict(), netG_out_path)
	print("Checkpoint saved to {} and {}".format(netD_out_path, netG_out_path))



def usedtime(strat_time, end_time):
	delta = int(end_time - strat_time)
	hours = delta // 3600
	minutes = (delta-hours*3600)//60
	seconds = delta-hours*3600-minutes*60
	return ('%2d:%2d:%2d' %(hours,minutes,seconds))


def getBatchSize(info):
	dicts = {4:32, 8:32, 16:16, 32:8, 64:4, 128:2, 256:1}

	return dicts[info]




