import torch.optim as optim
import torch
import torchvision as tv
import torch.nn as nn
import face2ske_torch.component as compon
import face2ske_torch.args as args
from face2ske_torch.args import a
import face2ske_torch.op as op
import face2ske_torch.print_util as pu
import copy

def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm2d') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)

class pix2pix_org(nn.Module):
    # 使用类来定义torch的主要的原件
    def __init__(self, inchannel=args.IN_CHANNEL, outchannel=args.OUT_CHANNNEL):
        # model
        super(pix2pix_org,self).__init__()
        # self.generator = compon.UnetGenerator2(inchannel, outchannel, 8, a.ngf,
        #                                        norm_layer=nn.InstanceNorm2d
        #                                   , use_dropout=False)       #unet256
        self.generator = compon.cgan_unetG(inchannel, outchannel)
        self.generator.change_config(start_middle_end_kernel=[4, 4, 4], normalize_fn=nn.InstanceNorm2d)
        # v0.2 change all to instance
        self.discrimnator = compon.NLayerDiscriminator(inchannel+outchannel, a.ndf, n_layers=3, norm_layer=nn.InstanceNorm2d,
                                                    )
        # loss
        self.dis_loss_fn = nn.BCELoss()
        self.gen_adv_loss_fn = nn.BCELoss()
        self.gen_l1_loss_fn = nn.L1Loss(reduce=True)
        # optimazer
        self.d_optim = None
        self.g_optim = None
        # model vars
        ## for MSE loss
        self.d_true_label = None
        self.d_false_label = None
        self.g_label = None
        ## output_image
        self.gen_image = None
        self.org_image = None
        self.ske_image = None
        # other vars
        self.mode = 'train'
        self.device = args.device_cuda
        self.check_point = None
        self.__init_device()
        self.print_model_detail()

    def forward(self,x,y):
        self.ske_image = y
        self.org_image = x
        # change config
        self.gen_image,_ = self.generator(self.org_image)
        image = {
            'gen_image': self.gen_image.detach().cpu(),
            'ske_image': self.ske_image.detach().cpu(),
            'org_image': self.org_image.detach().cpu()
        }
        if self.mode == 'test':
            return image
        predict_true = self.discrimnator(self.org_image, self.ske_image)
        predict_false = self.discrimnator(self.org_image, self.gen_image)
        # constuct label
        if self.d_false_label is None:
           self.d_false_label = torch.zeros(predict_false.size()).to(self.device)
        if self.d_true_label is None:
           self.d_true_label = torch.ones(predict_false.size()).to(self.device)
        if self.g_label is None:
           self.g_label = torch.ones(predict_false.size()).to(self.device)
        # D_loss
        discrim_loss = ((self.dis_loss_fn(predict_true, self.d_true_label) +
                         self.dis_loss_fn(predict_false, self.d_false_label)) / 2)

        g_loss_adv = self.gen_adv_loss_fn(predict_false, self.g_label)
        g_loss_l1 = self.gen_l1_loss_fn(self.gen_image, y)
        g_loss_total = g_loss_adv * args.a.gen_weight + g_loss_l1 * args.a.l1_weight
        metric = {
            'd_loss': discrim_loss.detach().cpu(),
            'g_loss_adv': g_loss_adv.detach().cpu(),
            'g_loss_l1': g_loss_l1.detach().cpu(),
            'g_loss_total': g_loss_total.detach().cpu()
        }
        metric = op.merge_dics([image,metric])
        # G_loss
        if self.mode == 'eval':
            return metric

        assert self.mode == 'train'
        # D_train
        d_vars = self.discrimnator.parameters()

        g_vars = self.generator.parameters()
        if self.d_optim is None:
            self.d_optim = torch.optim.Adam(d_vars, lr=a.d_lr, betas=args.BATAS)
        if self.g_optim is None:
            self.g_optim = torch.optim.Adam(g_vars, lr=a.g_lr, betas=args.BATAS)

        self.g_optim.zero_grad()
        self.d_optim.zero_grad()
        discrim_loss.backward(retain_graph=True)
        self.d_optim.step()
        # G train
        g_loss_total.backward()
        self.g_optim.step()
        train = {
            'd_var': d_vars,
            'g_var': g_vars
        }
        train = op.merge_dics([train,metric])
        return train

    def change_config(self):
        pass

    def mode_change(self,mode_str):
        assert mode_str in ['train','eval','test']
        self.mode = mode_str
        print('INFO:Change Mode to {}'.format(pu.to_bule(mode_str)))
    def __init_device(self):
        self.generator.to(self.device)
        self.discrimnator.to(self.device)
    def print_model_detail(self):
        print('='*10+'model detail'+'='*10)
        print('G:')
        print(self.generator)
        print('D:')
        print(self.discrimnator)
        print('='*25)

class pix2pix_our_in(nn.Module):
    # 使用类来定义torch的主要的原件
    def __init__(self, inchannel=args.IN_CHANNEL, outchannel=args.OUT_CHANNNEL):
        # model
        super(pix2pix_our_in,self).__init__()
        # self.generator = compon.UnetGenerator2(inchannel, outchannel, 8, a.ngf,
        #                                        norm_layer=nn.InstanceNorm2d
        #                                   , use_dropout=False)       #unet256
        self.generator = (compon.Generator(inchannel,outchannel, 8, a.ngf, norm_layer=compon.get_norm_layer(), use_dropout=False) )      #unet256

        # v0.2 change all to instance
        self.discrimnator = compon.Discriminator(inchannel+outchannel, a.ndf, n_layers=1, norm_layer=compon.get_norm_layer(),
                                                 use_sigmoid=False)
        self.device = args.device_cuda
        self.__init_device()
        self.generator.apply(weights_init)
        self.discrimnator.apply(weights_init)

        # loss
        self.dis_loss_fn = compon.GANLoss(True)
        self.gen_adv_loss_fn = compon.GANLoss(True)
        self.gen_l1_loss_fn = nn.L1Loss()
        # optimazer
        self.d_optim = None
        self.g_optim = None
        # model vars
        ## for MSE loss
        self.d_true_label = None
        self.d_false_label = None
        self.g_label = None
        ## output_image
        self.gen_image = None
        self.org_image = None
        self.ske_image = None
        # other vars
        self.mode = 'train'

        self.check_point = None

        self.print_model_detail()

    def forward(self,x,y):
        self.ske_image = y.detach()
        self.org_image = x.detach()
        # change config
        param_dict = dict(self.generator.named_parameters())
        params = []
        # for key,value in param_dict.items():
        #     print(
        #         key,value.data.type()
        #     )
        self.gen_image = self.generator(self.org_image,256)
        image = {
            'gen_image': self.gen_image.detach().cpu(),
            'ske_image': self.ske_image.detach().cpu(),
            'org_image': self.org_image.detach().cpu()
        }
        if self.mode == 'test':
            return image
        predict_true = self.discrimnator(self.org_image, self.ske_image)
        predict_false_d = self.discrimnator(self.org_image, self.gen_image.detach())
        predict_false_g = self.discrimnator(self.org_image, self.gen_image)# 核心! 更新g时候adv要回传到g 否则 只有l1 loss在更新
        # constuct label

        # D_loss
        discrim_loss = ((self.dis_loss_fn(predict_true, True) +
                         self.dis_loss_fn(predict_false_d, False)) / 2)

        g_loss_adv = self.gen_adv_loss_fn(predict_false_g, True)
        g_loss_l1 = self.gen_l1_loss_fn(self.gen_image, y)
        g_loss_total = g_loss_adv * args.a.gen_weight + g_loss_l1 * args.a.l1_weight
        metric = {
            'd_loss': discrim_loss.detach().cpu(),
            'g_loss_adv': g_loss_adv.detach().cpu(),
            'g_loss_l1': g_loss_l1.detach().cpu(),
            'g_loss_total': g_loss_total.detach().cpu()
        }
        metric = op.merge_dics([image,metric])
        # G_loss
        if self.mode == 'eval':
            return metric

        assert self.mode == 'train'
        # D_train
        d_vars = self.discrimnator.parameters()

        g_vars = self.generator.parameters()
        if self.d_optim is None:
            self.d_optim = torch.optim.Adam(d_vars, lr=a.d_lr, betas=args.BATAS)
        if self.g_optim is None:
            self.g_optim = torch.optim.Adam(g_vars, lr=a.g_lr, betas=args.BATAS)


        self.d_optim.zero_grad()
        discrim_loss.backward()
        self.d_optim.step()
        # G train
        self.g_optim.zero_grad()
        g_loss_total.backward()
        self.g_optim.step()
        train = {
            'd_var': d_vars,
            'g_var': g_vars
        }
        train = op.merge_dics([train,metric])
        return train

    def change_config(self):
        pass

    def mode_change(self,mode_str):
        assert mode_str in ['train','eval','test']
        self.mode = mode_str
        print('INFO:Change Mode to {}'.format(pu.to_bule(mode_str)))
    def __init_device(self):
        self.generator.to(self.device)
        self.discrimnator.to(self.device)
    def print_model_detail(self):
        print('='*10+'model detail'+'='*10)
        print('G:')
        print(self.generator)
        print('D:')
        print(self.discrimnator)
        print('='*25)


class SR_Dpath_without_Att(nn.Module):
    def __init__(self,inchannel=args.IN_CHANNEL, outchannel=args.OUT_CHANNNEL):
        super(SR_Dpath_without_Att,self).__init__()
        # model
        # self.generator = compon.UnetGenerator(inchannel, outchannel, 8, a.ngf,
        #                                        norm_layer=nn.InstanceNorm2d
        #                                        , use_dropout=False)  # unet256
        self.generator = compon.cgan_unetG(inchannel,outchannel)
        self.generator.change_config(start_middle_end_kernel=[4,4,1],normalize_fn=nn.InstanceNorm2d,has_rgb_converter=True)

        # v0.2 change all to instance
        self.discrimnator = compon.Discriminator(inchannel + outchannel, a.ndf, n_layers=1,
                                                       norm_layer=nn.InstanceNorm2d,
                                                       use_sigmoid=False)
        self.enhancer = compon.cgan_unetG(inchannel,args.G_FILTER_NUM)
        eh_enfilter = [
            (args.G_FILTER_NUM, args.G_FILTER_NUM * 2),  # E1
            (args.G_FILTER_NUM * 2, args.G_FILTER_NUM * 4),  # E2
            (args.G_FILTER_NUM * 4, args.G_FILTER_NUM * 8),  # E3
        ]
        eh_defilter = [
            (args.G_FILTER_NUM * 8, args.G_FILTER_NUM * 4, 0),  # D3
            (args.G_FILTER_NUM * 4, args.G_FILTER_NUM * 2, 0),  # D2-with dp    # no dp?
            (args.G_FILTER_NUM * 2, args.G_FILTER_NUM, 0),  # D1
        ]
        self.enhancer.change_config(normalize_fn=nn.InstanceNorm2d,en_filters=eh_enfilter,de_filters=eh_defilter)   #no sigmod no dropout!
        self.rgb_convetor_1 = compon.rgb_convertor(64,outchannel)
        self.rgb_convetor_2 = compon.rgb_convertor(64,outchannel)

        # Loss
        self.g_adv_loss_fn = nn.MSELoss()
        self.d_loss_fn = nn.MSELoss()
        self.en_matloss_fn = nn.L1Loss()
        self.en_l2_loss_fn = nn.MSELoss()
        self.g_l1_loss_fn = nn.L1Loss()
        # optimazer
        self.d_optim = None
        self.g_optim = None
        self.eh_optim = None
        # model vars
        ## for MSE loss
        self.d_true_label = None
        self.d_false_label = None
        self.g_label = None
        self.en_l1_T = None
        ## output_image
        self.gen_image = None
        self.org_image = None
        self.ske_image = None
        self.enhance_image = None
        self.enhance_cheak = None
        # other vars
        self.mode = 'train'
        self.device = args.device_cuda
        self.check_point = None
        self.in_pre_train = True
        self.in_fine_train = False
        self.__init_device()
        self.print_model_detail()
    def __init_device(self):
        self.generator.to(self.device)
        self.discrimnator.to(self.device)
        self.enhancer.to(self.device)
        self.rgb_convetor_2.to(self.device)
        self.rgb_convetor_1.to(self.device)
    def set_pretrain(self,mode=True):
        self.in_pre_train = mode
    def set_train_mode(self,mode):
        self.mode = mode
        if mode is 'train':
            self.train()
        else:
            self.eval()
        print('INFO:Change Mode to {}'.format(pu.to_bule(mode)))
    def print_model_detail(self):
        print(self.generator)
        print(self.discrimnator)
        print(self.enhancer)
        print(self.rgb_convetor_1)
        print(self.rgb_convetor_2)
    def set_finetrain(self,mode=True):
        self.in_pre_train = mode
    def forward(self, x,y):
        self.ske_image = y.detach()
        self.org_image = x.detach()
        self.gen_image,gen_feature = self.generator(self.org_image)
        pre_image = {
            'gen_image': self.gen_image.detach().cpu(),
            'ske_image': self.ske_image.detach().cpu(),
            'org_image': self.org_image.detach().cpu()
        }
        input_feature,_ = self.enhancer(self.org_image)
        #enhansor_check
        self.enhance_cheak = self.rgb_convetor_2(input_feature)
        if not self.in_fine_train:
            feature_mixer = gen_feature.detach()+input_feature
        else:
            feature_mixer = gen_feature+input_feature   # 最后finetrain 允许G做跟着D传播
        self.enhance_image = self.rgb_convetor_1(feature_mixer)
        enhance_image = {
            'enhance_checker':abs(self.enhance_cheak).detach().cpu(),
            'enhance_image':self.enhance_image.detach().cpu()
        }
        if self.mode is 'test':
            return op.merge_dics([pre_image,enhance_image])


        if self.in_pre_train:
            predict_true = self.discrimnator(self.org_image, self.ske_image)
            predict_false_d = self.discrimnator(self.org_image, self.gen_image.detach())
            predict_false_g = self.discrimnator(self.org_image, self.gen_image)

        else:
            predict_true = self.discrimnator(self.org_image, self.ske_image)
            predict_false_d = self.discrimnator(self.org_image, self.enhance_image.detach())
            predict_false_g = self.discrimnator(self.org_image, self.enhance_image)

        # constuct label
        if self.d_false_label is None:
            self.d_false_label = torch.zeros(predict_false_d.size()).to(self.device)
        if self.d_true_label is None:
            self.d_true_label = torch.ones(predict_false_d.size()).to(self.device)
        if self.g_label is None:
            self.g_label = torch.ones(predict_false_g.size()).to(self.device)

        if self.en_l1_T is None:
            self.en_l1_T = torch.zeros(self.enhance_cheak.size()).to(self.device)
        # enhance_loss

        enhance_mat_loss = self.en_matloss_fn(self.enhance_cheak,self.en_l1_T)
        enhance_distance = self.gen_image.detach() - self.ske_image.detach()
        enhance_dis_l2 = self.en_l2_loss_fn(self.enhance_cheak,enhance_distance)
        enhance_loss = a.enhance_lambda*enhance_mat_loss+enhance_dis_l2

        # Gloss

        g_loss_adv = self.g_adv_loss_fn(predict_false_g, self.g_label)
        if self.in_pre_train:
            g_loss_l1 = self.g_l1_loss_fn(self.gen_image, y)
        else:
            g_loss_l1 = self.g_l1_loss_fn(self.enhance_image,y)
        g_loss_total = g_loss_adv * args.a.gen_weight + g_loss_l1 * args.a.l1_weight

        # Dloss
        discrim_loss = ((self.d_loss_fn(predict_true, self.d_true_label) +
                         self.d_loss_fn(predict_false_d, self.d_false_label)) / 2)

        loss = {
            'enhance_loss':enhance_loss.detach().cpu(),
            'd_loss':discrim_loss.detach().cpu(),
            'g_loss_l1':g_loss_l1.detach().cpu(),
            'g_loss_total':g_loss_total.detach().cpu(),
            'g_loss_adv':g_loss_adv.detach().cpu()
        }
        if self.mode is 'eval':
            return op.merge_dics([pre_image,enhance_image,loss])
        # optimazer
        d_vars = self.discrimnator.parameters()
        g_vars = self.generator.parameters()
        enhance_vars = self.enhancer.parameters()
        rgb_2_vars = self.rgb_convetor_2.parameters()
        rgb_1_vars = self.rgb_convetor_1.parameters()
        if self.d_optim is None:
            self.d_optim = torch.optim.Adam(d_vars, lr=a.d_lr, betas=args.BATAS)

        if self.in_pre_train:
            if self.g_optim is None:
                self.g_optim = torch.optim.Adam(g_vars, lr=a.g_lr, betas=args.BATAS)
        elif self.in_fine_train is False:
            if self.g_optim is None:
                self.g_optim = torch.optim.Adam([{'params':rgb_1_vars},{'params':enhance_vars}], lr=a.g_lr, betas=args.BATAS)
        else:
            if self.g_optim is None:
                self.g_optim = torch.optim.Adam([{'params':rgb_1_vars},{'params':enhance_vars},{'params':g_vars}], lr=a.g_lr, betas=args.BATAS)

        if self.eh_optim is None:
            self.eh_optim = torch.optim.Adam([{'params':enhance_vars},{'params':rgb_2_vars}],lr=a.g_lr,betas=args.BATAS)


        self.d_optim.zero_grad()
        # pretrain 更新 D G 此时是自动根据情况更新
        discrim_loss.backward()
        self.d_optim.step()
        self.g_optim.zero_grad()
        g_loss_total.backward()
        self.g_optim.step()
        if not self.in_pre_train:
            self.eh_optim.zero_grad()
            enhance_loss.backward()
            self.eh_optim.step()

        return op.merge_dics([pre_image,enhance_image,loss])


class SR_Dpath_with_Att(nn.Module):
    def __init__(self,inchannel=args.IN_CHANNEL, outchannel=args.OUT_CHANNNEL):
        super(SR_Dpath_with_Att, self).__init__()
        # model
        self.generator = compon.UnetGenerator(inchannel, outchannel, 8, a.ngf,
                                              norm_layer=nn.InstanceNorm2d
                                              , use_dropout=False)  # unet256
        # v0.2 change all to instance
        self.discrimnator = compon.NLayerDiscriminator(inchannel + outchannel, a.ndf, n_layers=1,
                                                       norm_layer=nn.InstanceNorm2d,
                                                       use_sigmoid=False)
        self.enhancer = compon.cgan_unetG(inchannel, args.G_FILTER_NUM)
        eh_enfilter = [
            (args.G_FILTER_NUM, args.G_FILTER_NUM * 2),  # E1
            (args.G_FILTER_NUM * 2, args.G_FILTER_NUM * 4),  # E2
            (args.G_FILTER_NUM * 4, args.G_FILTER_NUM * 8),  # E3
        ]
        eh_defilter = [
            (args.G_FILTER_NUM * 8, args.G_FILTER_NUM * 4, 0),  # D3
            (args.G_FILTER_NUM * 4, args.G_FILTER_NUM * 2, 0),  # D2-with dp    # no dp?
            (args.G_FILTER_NUM * 2, args.G_FILTER_NUM, 0),  # D1
        ]
        self.enhancer.change_config(normalize_fn=nn.InstanceNorm2d, en_filters=eh_enfilter,
                                    de_filters=eh_defilter)  # no sigmod no dropout!
        self.rgb_convetor_1 = compon.rgb_convertor(64, outchannel)
        self.rgb_convetor_2 = compon.rgb_convertor(64, outchannel)

        # Loss
        self.g_adv_loss_fn = nn.MSELoss(reduce=True)
        self.d_loss_fn = nn.MSELoss(reduce=True)
        self.en_matloss_fn = nn.L1Loss(reduce=True)
        self.en_l2_loss_fn = nn.MSELoss(reduce=True)
        self.g_l1_loss_fn = nn.L1Loss(reduce=True)
        # optimazer
        self.d_optim = None
        self.g_optim = None
        self.eh_optim = None
        # model vars
        ## for MSE loss
        self.d_true_label = None
        self.d_false_label = None
        self.g_label = None
        self.en_l1_T = None
        ## output_image
        self.gen_image = None
        self.org_image = None
        self.ske_image = None
        self.enhance_image = None
        self.enhance_cheak = None
        # other vars
        self.mode = 'train'
        self.device = args.device_cuda
        self.check_point = None
        self.in_pre_train = True
        self.in_fine_train = False
        self.__init_device()
        self.print_model_detail()

    def __init_device(self):
        self.generator.to(self.device)
        self.discrimnator.to(self.device)
        self.enhancer.to(self.device)
        self.rgb_convetor_2.to(self.device)
        self.rgb_convetor_1.to(self.device)

    def set_pretrain(self, mode=True):
        self.in_pre_train = mode

    def set_train_mode(self, mode):
        self.mode = mode
        if mode is 'train':
            self.train()
        else:
            self.eval()
        print('INFO:Change Mode to {}'.format(pu.to_bule(mode)))

    def print_model_detail(self):
        print(self.generator)
        print(self.discrimnator)
        print(self.enhancer)
        print(self.rgb_convetor_1)
        print(self.rgb_convetor_2)

    def set_finetrain(self, mode=True):
        self.in_pre_train = mode

    def forward(self, x, y):
        self.ske_image = y
        self.org_image = x
        self.gen_image, gen_feature = self.generator(self.org_image)
        pre_image = {
            'gen_image': self.gen_image.detach().cpu(),
            'ske_image': self.ske_image.detach().cpu(),
            'org_image': self.org_image.detach().cpu()
        }
        input_feature, _ = self.enhancer(self.org_image)
        # enhansor_check
        self.enhance_cheak = self.rgb_convetor_2(input_feature)
        if not self.in_fine_train:
            feature_mixer = gen_feature.detach() + input_feature
        else:
            feature_mixer = gen_feature + input_feature  # 最后finetrain 允许G做跟着D传播
        self.enhance_image = self.rgb_convetor_1(feature_mixer)
        enhance_image = {
            'enhance_checker': abs(self.enhance_cheak).detach().cpu(),
            'enhance_image': self.enhance_image.detach().cpu()
        }
        if self.mode is 'test':
            return op.merge_dics([pre_image, enhance_image])

        if self.in_pre_train:
            predict_true = self.discrimnator(self.org_image, self.ske_image)
            predict_false = self.discrimnator(self.org_image, self.gen_image.detach())
        else:
            predict_true = self.discrimnator(self.org_image, self.ske_image)
            predict_false = self.discrimnator(self.org_image, self.enhance_image.detach())

        # constuct label
        if self.d_false_label is None:
            self.d_false_label = torch.zeros(predict_false.size()).to(self.device)
        if self.d_true_label is None:
            self.d_true_label = torch.ones(predict_false.size()).to(self.device)
        if self.g_label is None:
            self.g_label = torch.ones(predict_false.size()).to(self.device)

        if self.en_l1_T is None:
            self.en_l1_T = torch.zeros(self.enhance_cheak.size()).to(self.device)
        # enhance_loss

        enhance_mat_loss = self.en_matloss_fn(self.enhance_cheak, self.en_l1_T)
        enhance_distance = self.gen_image.detach() - self.ske_image.detach()
        enhance_dis_l2 = self.en_l2_loss_fn(self.enhance_cheak, enhance_distance)
        enhance_loss = a.enhance_lambda * enhance_mat_loss + enhance_dis_l2

        # Gloss

        g_loss_adv = self.g_adv_loss_fn(predict_false, self.g_label)
        if self.in_pre_train:
            g_loss_l1 = self.g_l1_loss_fn(self.gen_image, y)
        else:
            g_loss_l1 = self.g_l1_loss_fn(self.enhance_image, y)
        g_loss_total = g_loss_adv * args.a.gen_weight + g_loss_l1 * args.a.l1_weight

        # Dloss
        discrim_loss = ((self.d_loss_fn(predict_true, self.d_true_label) +
                         self.d_loss_fn(predict_false, self.d_false_label)) / 2)

        loss = {
            'enhance_loss': enhance_loss.detach().cpu(),
            'd_loss': discrim_loss.detach().cpu(),
            'g_loss_l1': g_loss_l1.detach().cpu(),
            'g_loss_total': g_loss_total.detach().cpu(),
            'g_loss_adv': g_loss_adv.detach().cpu()
        }
        if self.mode is 'eval':
            return op.merge_dics([pre_image, enhance_image, loss])
        # optimazer
        d_vars = self.discrimnator.parameters()
        g_vars = self.generator.parameters()
        enhance_vars = self.enhancer.parameters()
        rgb_2_vars = self.rgb_convetor_2.parameters()
        rgb_1_vars = self.rgb_convetor_1.parameters()
        if self.d_optim is None:
            self.d_optim = torch.optim.Adam(d_vars, lr=a.d_lr, betas=args.BATAS)

        if self.in_pre_train:
            if self.g_optim is None:
                self.g_optim = torch.optim.Adam(g_vars, lr=a.g_lr, betas=args.BATAS)
        elif self.in_fine_train is False:
            if self.g_optim is None:
                self.g_optim = torch.optim.Adam([{'params': rgb_1_vars}, {'params': enhance_vars}], lr=a.g_lr,
                                                betas=args.BATAS)
        else:
            if self.g_optim is None:
                self.g_optim = torch.optim.Adam([{'params': rgb_1_vars}, {'params': enhance_vars}, {'params': g_vars}],
                                                lr=a.g_lr, betas=args.BATAS)

        if self.eh_optim is None:
            self.eh_optim = torch.optim.Adam([{'params': enhance_vars}, {'params': rgb_2_vars}], lr=a.g_lr,
                                             betas=args.BATAS)

        self.g_optim.zero_grad()
        self.d_optim.zero_grad()
        self.eh_optim.zero_grad()
        # pretrain 更新 D G 此时是自动根据情况更新
        discrim_loss.backward(retain_graph=True)
        self.d_optim.step()
        g_loss_total.backward(retain_graph=True)
        self.g_optim.step()
        if not self.in_pre_train:
            enhance_loss.backward()
            self.eh_optim.step()

        return op.merge_dics([pre_image, enhance_image, loss])


class SR_GRU_Enhance_htFront(nn.Module):
    pass


class SR_GRU_Enhance_htBack(nn.Module):
    pass









