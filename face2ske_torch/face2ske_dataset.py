import cv2
from torch.utils.data import Dataset, DataLoader
import numpy as np
import face2ske_torch.op as op
from torchvision import transforms, utils
import random
from face2ske_torch.print_util import *
from PIL import Image
import face2ske_torch.args as args
import traceback

default_transforms = transforms.Compose([
    transforms.Pad(padding=(43,18)),# 250,200 -> 256,256
    transforms.ToTensor(),
    transforms.Normalize((0.5,0.5,0.5), (0.5,0.5,0.5)),
])
default_test_transforms = transforms.Compose([
    transforms.Pad(padding=(28,3)),# 250,200 -> 256,256
    transforms.ToTensor(),
    transforms.Normalize((0.5,0.5,0.5), (0.5,0.5,0.5)),
])

default_test_gray_transforms = transforms.Compose([
    transforms.Grayscale(),
    transforms.Pad(padding=(28,3)),  # 250,200 -> 256,256
    transforms.ToTensor(),
    # transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),

])
default_gray_transforms = transforms.Compose([
    transforms.Grayscale(),
    transforms.Pad(padding=(43,18)),  # 250,200 -> 286,286
    transforms.ToTensor(),
    # transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),

])
def default_loader(path):
    try:
        img = Image.open(path)
        img = img.convert('RGB')
        return img
    except:
        print("Can not read Image: path{}".format(to_red(path)))

# ske Dataset


class Fack2skeDataset(Dataset):
    def __init__(self,data_list,org_transform=default_transforms,gray_transform =default_gray_transforms ,mode = 'train',loader = default_loader):
        self.org_transform = org_transform
        self.gray_transform = gray_transform
        self.ske_img_path = []
        self.org_img_path = []
        self.loader = loader
        self.mode = mode
        #获得两个文件的path
        ske_img_path,org_img_path = op.read_path_list(data_list,mode)
        # if shuffle:
        #     list_len = len(ske_img_path)
        #     randomIndex = random.sample(range(list_len), list_len)
        #     for i in range(len(randomIndex)):
        #         self.ske_img_path.append(ske_img_path[randomIndex[i]])
        #         self.org_img_path.append(org_img_path[randomIndex[i]])
        # else:
        self.ske_img_path = ske_img_path
        self.org_img_path = org_img_path


    def __len__(self):
        assert len(self.ske_img_path)==len(self.org_img_path)
        return len(self.ske_img_path)

    def __getitem__(self, item):
        # cv2 读
        ske_path = self.ske_img_path[item]
        org_path = self.org_img_path[item]

        ske_image = self.loader(ske_path)
        org_image = self.loader(org_path)
        # transfrom
        try:
            h_offset = random.randint(0, 285 - 256)
            w_offset = random.randint(0, 285 - 256)

            if self.gray_transform is not None:
                ske_image = self.gray_transform(ske_image)
                if self.mode is 'train':
                    ske_image = ske_image[:, h_offset:h_offset + 256, w_offset:w_offset + 256]

            if self.org_transform is not None:
                org_image = self.org_transform(org_image)
                if self.mode is 'train':
                    org_image = org_image[:, h_offset:h_offset + 256, w_offset:w_offset + 256]
        except Exception as e:
                print("Can not transfrom image: {}".format(to_red(ske_path)))
                print(e)
        return {'ske_image':ske_image,
                'org_image':org_image}

if __name__ == '__main__':
    dataset = Fack2skeDataset(args.TRAIN_DATA_LIST)
    for i in range(len(dataset)):
        if i > 3:
            break
        sample = dataset[i]
        print("ske_img_size {}".format(to_yellow(sample['ske_image'].size())))
        print("ske_img_max {}".format(to_yellow(sample['ske_image'].max())))
        print("ske_img_min {}".format(to_yellow(sample['ske_image'].min())))

        print("org_img_size {}".format(to_yellow(sample['org_image'].size())))
        print("org_img_max {}".format(to_yellow(sample['org_image'].max())))
        print("org_img_min {}".format(to_yellow(sample['org_image'].min())))