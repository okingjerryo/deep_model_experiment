import torch
import torch.nn as nn
import numpy as np
from face2ske_torch.args import G_FILTER_NUM, D_FILTER_NUM
import torchvision as tv
import torch.nn.functional as F
import functools
import face2ske_torch.args as args
from torch.autograd import Variable


class cgan_unetG(nn.Module):

    def __init__(self, inchannel, outchannel):
        # init param
        super(cgan_unetG, self).__init__()
        self.layers = []
        self.start_middle_end_kernel = [4, 4, 4]  # [first layer,middle layer,end layer] kernel size
        # if this is on,origenal unet last layer filter change to a.ngf,and add a rgb converter layer
        self.has_rgb_converter = False
        # filter seq
        self.en_filters = [
            (G_FILTER_NUM, G_FILTER_NUM * 2),  # E1
            (G_FILTER_NUM * 2, G_FILTER_NUM * 4),  # E2
            (G_FILTER_NUM * 4, G_FILTER_NUM * 8),  # E3
            (G_FILTER_NUM * 8, G_FILTER_NUM * 8),  # E4
            (G_FILTER_NUM * 8, G_FILTER_NUM * 8),  # E5
            (G_FILTER_NUM * 8, G_FILTER_NUM * 8),  # E6
            (G_FILTER_NUM * 8, G_FILTER_NUM * 8),  # E7

        ]

        self.de_filters = [
            (G_FILTER_NUM * 8, G_FILTER_NUM * 8, 0),  # D7
            (G_FILTER_NUM * 8, G_FILTER_NUM * 8, 0),  # D6 dp
            (G_FILTER_NUM * 8, G_FILTER_NUM * 8, 0),  # D5     dp
            (G_FILTER_NUM * 8, G_FILTER_NUM * 8, 0),  # D4     dp
            (G_FILTER_NUM * 8, G_FILTER_NUM * 4, 0),  # D3
            (G_FILTER_NUM * 4, G_FILTER_NUM * 2, 0),  # D2
            (G_FILTER_NUM * 2, G_FILTER_NUM, 0),  # D1
        ]
        self.normalize_fn = nn.BatchNorm2d
        self.encode_act = nn.LeakyReLU(0.2, True)
        self.decode_act = nn.ReLU(True)
        self.decode_feature = None  # if rgb converter is on,this will output decoder_1 output feature
        self.pooling = None  # if not none , use pooling for downsample
        self.stride = 2
        self.out_channel = outchannel
        #
        self.sequence = None
        self.model = None
        self.conv_1 = nn.Conv2d
        self.inchannel = inchannel
        # self.__model_builer()

    # 创建层
    def __model_builer(self):
        st = self.stride
        ker = self.start_middle_end_kernel

        self.en_conv1 = nn.Conv2d(self.inchannel, G_FILTER_NUM, kernel_size=ker[0], stride=st, padding=1)
        # encoder
        for encoder_layer, (i_c, o_c) in enumerate(self.en_filters):
            name = 'en_conv{}_conv'.format(encoder_layer)
            setattr(self, name, nn.Conv2d(i_c, o_c, kernel_size=ker[1], stride=st, padding=1))  # 自动创建变量名
            # 使用pool降维度
            if self.pooling is not None:
                name = 'en_conv{}_pool'.format(encoder_layer)
                setattr(self, name, self.pooling(kernel_size=ker[1], stride=2, padding=1))
            if encoder_layer is not len(self.en_filters) - 1:
                name = 'en_conv{}_norm'.format(encoder_layer)
                setattr(self, name, self.normalize_fn(num_features=o_c, affine=False))

        # decoder
        for decoder_layer, (i_c, o_c, dp) in enumerate(self.de_filters):
            # concat
            if decoder_layer == 0:
                name = 'de_conv{}_conv'.format(decoder_layer)
                setattr(self, name, nn.ConvTranspose2d(i_c, o_c, kernel_size=ker[1], stride=2, padding=1))
            else:
                name = 'de_conv{}_conv'.format(decoder_layer)
                setattr(self, name, nn.ConvTranspose2d(i_c * 2, o_c, kernel_size=ker[1], stride=2, padding=1))
            name = 'de_conv{}_norm'.format(decoder_layer)
            setattr(self, name, self.normalize_fn(o_c, affine=False))

        if self.has_rgb_converter is True:
            # feature layer
            self.last_conv = nn.ConvTranspose2d(G_FILTER_NUM * 2, G_FILTER_NUM, kernel_size=ker[1], stride=2, padding=1)
            self.last_norm = self.normalize_fn(G_FILTER_NUM, affine=False)
            # rgb converter
            self.rgb_conv = nn.Conv2d(G_FILTER_NUM, self.out_channel, stride=1, kernel_size=ker[2],
                                      padding=(ker[2] - 1) // 2)
        else:
            self.last_conv = nn.ConvTranspose2d(G_FILTER_NUM * 2, self.out_channel, kernel_size=ker[2], stride=2,
                                                padding=(ker[2] - 1) // 2)

    # change hipoparam when build
    def change_config(self, start_middle_end_kernel=None,
                      has_rgb_converter=False,
                      normalize_fn=nn.BatchNorm2d,
                      encode_act=nn.LeakyReLU(negative_slope=0.2, inplace=True),
                      decode_act=nn.ReLU(True),
                      pooling=None,
                      en_filters=None,
                      de_filters=None):
        if start_middle_end_kernel is None:
            start_middle_end_kernel = [4, 4, 4]
        if en_filters is None:
            en_filters = [
                (G_FILTER_NUM, G_FILTER_NUM * 2),  # E1
                (G_FILTER_NUM * 2, G_FILTER_NUM * 4),  # E2
                (G_FILTER_NUM * 4, G_FILTER_NUM * 8),  # E3
                (G_FILTER_NUM * 8, G_FILTER_NUM * 8),  # E4
                (G_FILTER_NUM * 8, G_FILTER_NUM * 8),  # E5
                (G_FILTER_NUM * 8, G_FILTER_NUM * 8),  # E6
                (G_FILTER_NUM * 8, G_FILTER_NUM * 8),  # E7

            ]
        if de_filters is None:
            de_filters = [
                (G_FILTER_NUM * 8, G_FILTER_NUM * 8, 0),  # D7
                (G_FILTER_NUM * 8, G_FILTER_NUM * 8, 0),  # D6 dp
                (G_FILTER_NUM * 8, G_FILTER_NUM * 8, 0),  # D5     dp
                (G_FILTER_NUM * 8, G_FILTER_NUM * 8, 0),  # D4     dp
                (G_FILTER_NUM * 8, G_FILTER_NUM * 4, 0),  # D3
                (G_FILTER_NUM * 4, G_FILTER_NUM * 2, 0),  # D2
                (G_FILTER_NUM * 2, G_FILTER_NUM, 0),  # D1
            ]

        self.start_middle_end_kernel = start_middle_end_kernel
        self.has_rgb_converter = has_rgb_converter
        self.normalize_fn = normalize_fn
        self.encode_act = encode_act
        self.decode_act = decode_act
        self.pooling = pooling
        self.de_filters = de_filters
        self.en_filters = en_filters
        self.layers = []
        if self.pooling is not None:
            self.stride = 1
        self.__model_builer()

    def forward(self, x):
        # first
        self.layers = []
        first_layer = self.en_conv1(x)  # E0  downC ->
        self.layers.append(first_layer)

        # middle
        for encoder_layer in range(len(self.en_filters)):
            rectifiled = self.encode_act(self.layers[-1])  # downR->downC->downN
            var_name = 'en_conv{}_conv'.format(encoder_layer)
            conv = getattr(self, var_name)(rectifiled)
            # 使用pool降维度
            if self.pooling is not None:
                var_name = 'en_conv{}_pool'.format(encoder_layer)
                conv = getattr(self, var_name)(conv)
            if encoder_layer is not len(self.en_filters) - 1:
                var_name = 'en_conv{}_norm'.format(encoder_layer)
                conv = getattr(self, var_name)(conv)
            self.layers.append(conv)

        num_encoder_layers = len(self.layers)
        # decoder
        for decoder_layer, (i_c, o_c, dp) in enumerate(self.de_filters):
            skip_layer = num_encoder_layers - decoder_layer - 1
            # concat
            if decoder_layer == 0:
                input = self.layers[-1]
            else:
                input = torch.cat([self.layers[skip_layer], self.layers[-1]], dim=1)  # dp 位置问题 和cat 位置问题

            rectifiled = self.decode_act(input)
            name = 'de_conv{}_conv'.format(decoder_layer)
            de_conv = getattr(self, name)(rectifiled)
            name = 'de_conv{}_norm'.format(decoder_layer)
            output = getattr(self, name)(de_conv)
            if dp > 0:
                output = nn.Dropout2d(p=dp)(output)
            self.layers.append(output)

        # decoder 1
        input = torch.cat([self.layers[-1], self.layers[0]], dim=1)
        # decoder 0
        output = self.decode_act(input)  # D0 Ur->uC->Th
        if self.has_rgb_converter is True:
            # feature layer
            output = self.last_conv(output)
            output = self.last_norm(output)
            self.decode_feature = output
            # rgb converter
            output = self.rgb_conv(output)
            output = nn.Tanh()(output)
        else:
            output = self.last_conv(output)
            output = nn.Tanh()(output)

        return output, self.decode_feature


class cgan_patch_d(nn.Module):
    def __init__(self, input_channel):
        super(cgan_patch_d, self).__init__()
        self.n_layers = 3
        self.normalize_fn = nn.BatchNorm2d
        self.pooling = None
        self.normal_act = nn.LeakyReLU(negative_slope=0.2, inplace=True)
        self.stride = 2
        self.output_act = nn.Sigmoid()
        self.kernel = 4
        self.input_channel = input_channel
        self.sequence = []
        self.model = None
        self.use_bias = (self.normal_act == nn.InstanceNorm2d) or (args.BATCH_SIZE == 1)
        # self.__build_model()

    def change_config(self,
                      n_layers=3,
                      normalize_fn=nn.BatchNorm2d,
                      pooling=None,
                      normal_act=nn.LeakyReLU(negative_slope=0.2),
                      output_act=nn.Sigmoid(),
                      kernel=4,
                      use_bias=None
                      ):
        if use_bias is None:
            use_bias = normal_act == nn.InstanceNorm2d
        self.n_layers = n_layers
        self.normalize_fn = normalize_fn
        self.pooling = pooling
        if pooling is not None:
            self.stride = 1
        self.normal_act = normal_act
        self.output_act = output_act
        self.kernel = kernel
        self.use_bias = use_bias
        self.__build_model()

    def __build_model(self):
        ker = self.kernel
        st = self.stride
        # first_layer
        self.sequence += [nn.Conv2d(self.input_channel, D_FILTER_NUM, kernel_size=ker, stride=st, padding=1)]
        if self.pooling is not None:
            self.sequence += [self.pooling(kernel_size=ker, stride=2, padding=1)]
        self.sequence += [self.normal_act]

        # middle
        for i in range(self.n_layers):
            in_channel = D_FILTER_NUM * min(2 ** (i), 8)
            out_channels = D_FILTER_NUM * min(2 ** (i + 1), 8)
            stride = 1 if i == self.n_layers - 1 else st

            self.sequence += [nn.Conv2d(in_channel, out_channels, kernel_size=ker, stride=stride,
                                        padding=1, bias=self.use_bias),
                              self.normalize_fn(out_channels, affine=True, track_running_stats=False),
                              self.normal_act]

        # last layer
        self.sequence += [nn.Conv2d(D_FILTER_NUM * 2, 1, kernel_size=ker, stride=1, padding=1)]

        if self.output_act is not None:
            self.sequence += [self.output_act]
        self.model = nn.Sequential(*self.sequence)

    def forward(self, x, y):

        input = torch.cat([x, y], dim=1)
        return self.model(input)


class rgb_convertor(nn.Module):
    def __init__(self, inchannel, outchannel):
        super(rgb_convertor, self).__init__()
        self.kernel = 1
        self.in_channel = inchannel
        self.out_channel = outchannel
        self.act = nn.ReLU()
        self.__build_model()

    def __build_model(self):
        self.conv = nn.Conv2d(self.in_channel, self.out_channel, kernel_size=self.kernel,
                              padding=(self.kernel - 1) // 2)

    def forward(self, x):
        net = self.act(x)
        net = self.conv(net)
        output = nn.Tanh()(net)
        nn.TripletMarginLoss
        return output

    def change_config(self, kernel=1, act=nn.ReLU):
        self.kernel = kernel
        self.act = act


class Discriminator(nn.Module):
    def __init__(self, input_nc, ndf=64, n_layers=3, norm_layer=nn.BatchNorm2d, use_sigmoid=False):
        super(Discriminator, self).__init__()
        kw = 4
        padw = int(np.ceil((kw - 1) / 2))  # padw=1
        sequence = [nn.Conv2d(input_nc, ndf, kernel_size=kw, stride=2, padding=padw), nn.LeakyReLU(0.2, True)]

        nf_mult = 1
        nf_mult_prev = 1
        for n in range(1, n_layers):
            nf_mult_prev = nf_mult
            nf_mult = min(2 ** n, 8)
            sequence += [nn.Conv2d(ndf * nf_mult_prev, ndf * nf_mult, kernel_size=kw, stride=2, padding=padw), \
                         norm_layer(ndf * nf_mult), nn.LeakyReLU(0.2, True)]

        nf_mult_prev = nf_mult  # 1
        nf_mult = min(2 ** n_layers, 8)  # 2
        sequence += [nn.Conv2d(ndf * nf_mult_prev, ndf * nf_mult, kernel_size=kw, stride=1, padding=padw), \
                     norm_layer(ndf * nf_mult), nn.LeakyReLU(0.2, True)]

        sequence += [nn.Conv2d(ndf * nf_mult, 1, kernel_size=kw, stride=1, padding=padw)]

        if use_sigmoid:
            sequence += [nn.Sigmoid()]

        self.model = nn.Sequential(*sequence)

    def forward(self, input,y):
        input = torch.cat([input, y], dim=1)
        return self.model(input)


class Generator(nn.Module):
    def __init__(self, input_nc, output_nc, num_downs, ngf=64, norm_layer=nn.BatchNorm2d, use_dropout=False):
        super(Generator, self).__init__()

        self.encoder = nn.ModuleList()
        self.decoder = nn.ModuleList()
        # self.fromRGB = nn.ModuleList()
        # self.toRGB = nn.ModuleList()
        self.num_downs = num_downs

        '''Encoder Blocks'''
        layers = []
        layers += [nn.Conv2d(input_nc, 64, kernel_size=4, stride=2, padding=1)]  # 128
        self.encoder.append(nn.Sequential(*layers))
        for i in range(num_downs - 1):
            ic, oc = min(2 ** (i + 6), 512), min(2 ** (i + 7), 512)
            if i == num_downs - 1 - 1:
                layers = self.G_Encoder_Blocks([], ic, oc)
            else:
                layers = self.G_Encoder_Blocks([], ic, oc, norm_layer)
            self.encoder.append(nn.Sequential(*layers))
        # self.fromRGB.append(self.convertFromRGB([], 3, ic))

        '''Decoder Blocks and toRGB'''
        layers = []
        layers = [nn.ReLU(True)] + [nn.ConvTranspose2d(oc, ic, kernel_size=4, stride=2, padding=1)] + [norm_layer(512)]
        self.decoder.append(nn.Sequential(*layers))
        # self.toRGB.append(self.convertToRGB([], ic, 1))         # no use
        for i in range(num_downs - 3, -1, -1):  # 5,4,3,2,1,0
            ic, oc = min(2 ** (i + 8), 1024), min(2 ** (i + 6), 512)
            layers = self.G_Decoder_Blocks([], ic, oc, norm_layer)
            self.decoder.append(nn.Sequential(*layers))
        # self.toRGB.append(self.convertToRGB([], oc, 1))
        layers = []
        layers += [nn.ReLU(True)] + [nn.ConvTranspose2d(128, 1, kernel_size=4, stride=2, padding=1)] + [nn.Tanh()]
        self.decoder.append(nn.Sequential(*layers))

    def forward(self, x, tar_resol):

        R = int(np.log2(tar_resol))

        temp = []
        _start, _end = self.num_downs - R, self.num_downs
        # if tar_resol != 256:
        # 	x = self.fromRGB[_start-1](x)
        for i in range(_start, _end):
            x = self.encoder[i](x)
            temp.append(x)

        _start, _end = 0, R
        x = self.decoder[0](x)
        for i in range(_start + 1, _end):
            x = torch.cat([x, temp[_end - 1 - i]], 1)
            x = self.decoder[i](x)
        # if i != 7:
        # 	x = self.toRGB[i](x)

        return x

    def G_Encoder_Blocks(self, incoming, ic, oc, norm_layer=None):
        layers = incoming
        layers += [nn.LeakyReLU(0.2, True)]
        layers += [nn.Conv2d(ic, oc, kernel_size=4, stride=2, padding=1)]
        if norm_layer:
            layers += [norm_layer(oc)]

        return layers

    def G_Decoder_Blocks(self, incoming, ic, oc, norm_layer=None):
        layers = incoming
        layers += [nn.ReLU(True)]
        layers += [nn.ConvTranspose2d(ic, oc, kernel_size=4, stride=2, padding=1)]
        layers += [norm_layer(oc)]

        return layers





def get_norm_layer(norm_type='instance'):
    if norm_type == 'batch':
        norm_layer = functools.partial(nn.BatchNorm2d, affine=True)
    elif norm_type == 'instance':
        norm_layer = functools.partial(nn.InstanceNorm2d, affine=False)
    else:
        raise NotImplementedError('normalization layer [%s] is not found' % norm_type)
    return norm_layer


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm2d') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)


# Total number of parameters
def print_network(net):
    num_params = 0
    for param in net.parameters():
        num_params += param.numel()
    print(net)
    print('Total number of parameters: %d' % num_params)


def localLossL1(fake_s, real_s, real_p, criterionL1):
    sh0, s0 = dot_product(fake_s, real_s, real_p[:, 3, :, :])
    sh1, s1 = dot_product(fake_s, real_s, real_p[:, 4, :, :])
    sh2, s2 = dot_product(fake_s, real_s, real_p[:, 5, :, :])
    sh3, s3 = dot_product(fake_s, real_s, real_p[:, 6, :, :])
    sh4, s4 = dot_product(fake_s, real_s, real_p[:, 7, :, :])
    sh5, s5 = dot_product(fake_s, real_s, real_p[:, 8, :, :])
    sh6, s6 = dot_product(fake_s, real_s, real_p[:, 9, :, :])
    sh7, s7 = dot_product(fake_s, real_s, real_p[:, 10, :, :])

    l0 = criterionL1(sh0, s0)
    l1 = criterionL1(sh1, s1)
    l2 = criterionL1(sh2, s2)
    l3 = criterionL1(sh3, s3)
    l4 = criterionL1(sh4, s4)
    l5 = criterionL1(sh5, s5)
    l6 = criterionL1(sh6, s6)
    l7 = criterionL1(sh7, s7)

    loss = l0 + l1 + l2 + l3 + l4 + l5 + l6 + l7

    return loss


def localLossL1_2(fake_s, real_s, real_p, criterionL1):
    parsing = real_p[:, 3:, :, :].data
    parsing = torch.max(parsing, 1)
    probs = []
    for i in range(8):
        index = (torch.ones(parsing[1].size()) * i)
        prob = torch.eq(parsing[1].float(), index).float()
        probs.append(prob)

    sh, s = [], []
    for i in range(8):
        sh0, s0 = dot_product(fake_s, real_s, Variable(probs[i]))
        sh.append(sh0)
        s.append(s0)

    loss = 0.
    for i in range(8):
        l0 = criterionL1(sh[i], s[i])
        loss += l0

    return loss


def dot_product(fake_s, real_s, parsing):
    sh = torch.mul(fake_s, parsing)
    s = torch.mul(real_s, parsing)
    return sh, s


def avgpoolLoss(fake_s, real_s, criterionL1):
    avgpool = nn.AvgPool2d(kernel_size=2, stride=2)
    fake_s = avgpool(avgpool(fake_s))
    real_s = avgpool(avgpool(real_s))

    loss = criterionL1(fake_s, real_s)

    return loss


def maxpoolLoss(fake_s, real_s, criterionL1):
    maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
    fake_s = maxpool(maxpool(fake_s))
    real_s = maxpool(maxpool(real_s))

    # fake_s = maxpool(fake_s)
    # real_s = maxpool(real_s)

    loss = criterionL1(fake_s, real_s)

    return loss


def separate2(inputs, real_p):
    parsing = real_p[:, 3:, :, :].data
    parsing = torch.max(parsing, 1)
    index = (torch.ones(parsing[1].size()) * 7).cuda()
    prob7 = torch.eq(parsing[1].float(), index).float()

    prob7_ = torch.ones(prob7.size()).cuda() - prob7
    if inputs.size(1) > 2:
        prob7_ = torch.cat([prob7_, prob7_, prob7_], 1)
        prob7 = torch.cat([prob7, prob7, prob7], 1)
    hair_no = torch.mul(inputs, Variable(prob7_))
    hair_yes = torch.mul(inputs, Variable(prob7))

    return hair_no, hair_yes

# Defines the GAN loss which uses either LSGAN or the regular GAN.
# When LSGAN is used, it is basically same as MSELoss,
# but it abstracts away the need to create the target label tensor
# that has the same size as the input
class GANLoss(nn.Module):
    def __init__(self, use_lsgan=True, target_real_label=1.0, target_fake_label=0.0,
                 tensor=torch.FloatTensor):
        super(GANLoss, self).__init__()
        self.real_label = target_real_label
        self.fake_label = target_fake_label
        self.real_label_var = None
        self.fake_label_var = None
        self.Tensor = tensor
        if use_lsgan:
            self.loss = nn.MSELoss()
        else:
            self.loss = nn.BCELoss()

    def get_target_tensor(self, input, target_is_real):
        target_tensor = None
        if target_is_real:
            create_label = ((self.real_label_var is None) or
                            (self.real_label_var.numel() != input.numel()))
            if create_label:
                real_tensor = self.Tensor(input.size()).fill_(self.real_label)
                self.real_label_var = Variable(real_tensor, requires_grad=False)
            target_tensor = self.real_label_var
        else:
            create_label = ((self.fake_label_var is None) or
                            (self.fake_label_var.numel() != input.numel()))
            if create_label:
                fake_tensor = self.Tensor(input.size()).fill_(self.fake_label)
                self.fake_label_var = Variable(fake_tensor, requires_grad=False)
            target_tensor = self.fake_label_var
        return target_tensor

    def __call__(self, input, target_is_real):
        target_tensor = self.get_target_tensor(input, target_is_real)
        target_tensor = target_tensor.to(args.device_cuda)
        return self.loss(input, target_tensor)

