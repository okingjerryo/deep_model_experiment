import face2ske_torch.args as args
import random
import numpy as np


def read_path_list(data_list, mode):
    ske_img = []
    org_img = []
    f = open(data_list, 'r')
    for line in f.readlines():
        arr = line.split('||')
        t_ske_img = args.PATH2TXTLIST + '/' + arr[1]
        t_org_img = args.PATH2TXTLIST + '/' + arr[0]
        ske_img.append(t_ske_img)
        org_img.append(t_org_img)
    f.close()
    if mode is not 'test':
        list_len = len(ske_img)
        randomIndex = random.sample(range(list_len), list_len)
        ske_img_tmp = []
        org_img_tmp = []
        for i in range(len(randomIndex)):
            ske_img_tmp.append(ske_img[randomIndex[i]])
            org_img_tmp.append(org_img[randomIndex[i]])
        ske_img = ske_img_tmp
        org_img = org_img_tmp

    # if mode is 'train':
    #
    #     end = round(len(ske_img)*0.8)
    #     return ske_img[:end],org_img[:end]
    # elif mode is 'eval':
    #     # 取后 20%
    #     start = round(len(ske_img)*0.8)
    #     return ske_img[start:],org_img[start:]
    # else:
    # 测试集 全取得
    return ske_img, org_img


def merge_dics(dics: list()):
    """Given two dicts, merge them into a new dict as a shallow copy."""
    dic_length = len(dics)
    assert dic_length > 0
    re = dics[0].copy()
    for i in range(1, dic_length):
        re.update(dics[i])
    return re


def denorm_to_rgb(input):
    return (input / 2) + 0.5


if __name__ == '__main__':
    pass


def depadding(image, padlist):
    '''
    恢复为原来图片大小
    :param org_image:
    :param padlist: [LR,UD]
    :return:
    '''
    l = padlist[0]
    u = padlist[1]
    try:
        if len(image.size()) is 3:
            result = image[:, u:-u, l:-l]
        else:
            result = image[:, :, u:-u, l:-l]
    except Exception:
        result = image[:,u:-u, l:-l]
    return result



def chw2hwc(image):
    return np.transpose(image, axes=(1, 2, 0))


def usedtime(strat_time, end_time):
    delta = int(end_time - strat_time)
    hours = delta // 3600
    minutes = (delta - hours * 3600) // 60
    seconds = delta - hours * 3600 - minutes * 60
    return ('%2d:%2d:%2d' % (hours, minutes, seconds))
