import argparse
import torch
from face2ske_torch.inception import InceptionV3
# TRAIN_DATA_LIST = '/data/uryuo/db/photosketch/list_train.txt'
# TRAIN_DATA_LIST = '/home/data_disk/uryuo/db/photosketch/list_train.txt'
# TEST_DATA_LIST = '/data/uryuo/db/photosketch/list_test.txt'
# TEST_DATA_LIST = '/home/data_disk/uryuo/db/photosketch/list_test.txt'
# PATH2TXTLIST = '/home/jacob/dataset/photosketch'
# PATH2TXTLIST = '/home/data_disk/uryuo/db/photosketch'
# alienw
TRAIN_DATA_LIST = '/home/uryuo/db/photosketch/list_train.txt'
TEST_DATA_LIST = '/home/uryuo/db/photosketch/list_test.txt'
PATH2TXTLIST = '/home/uryuo/db/photosketch'

parser = argparse.ArgumentParser()
# tf args
parser.add_argument("--ngf",default=64,type=int ,help="g filter")
parser.add_argument("--ndf",default=64,type=int ,help="d filter")
parser.add_argument("--batch_size",default=20,type=int ,help="batch_size")
parser.add_argument("--gen_weight",default=1,type=float ,help="gen_weight use to g_loss")
parser.add_argument("--l1_weight",default=10,type=float ,help="l1 weight use to g_loss")
parser.add_argument("--d_lr",default=0.0002,type=float ,help="d lr")
parser.add_argument("--g_lr",default=0.0002,type=float ,help="g lr")
# parser.add_argument("--checkpoint",default='model_pix2pix_ourin/' ,help="")
parser.add_argument("--checkpoint",default='model_SR_UE_NA/' ,help="")
parser.add_argument("--enhance_lambda",default=1,type=float ,help="")
parser.add_argument('--dims', type=int, default=2048,
                    choices=list(InceptionV3.BLOCK_INDEX_BY_DIM),
                    help=('Dimensionality of Inception features to use. '
                          'By default, uses pool3 features'))
parser.add_argument('--root', type=str, default=PATH2TXTLIST, help='image source folder')
parser.add_argument('--gen_root', type=str, default='./Gen_images', help='images generated to')
parser.add_argument('--lr', type=float, default=0.0002, help='initial learning rate for adam')
parser.add_argument('--threads', type=int, default=4, help='number of threads for data loader to use')
parser.add_argument('--input_nc', type=int, default=3, help='# of input image channels')
parser.add_argument('--output_nc', type=int, default=1, help='# of output image channels')
parser.add_argument('--no_dropout', action='store_true', help='no dropout for the generator')
parser.add_argument('--gpu_ids', default=[0], help='gpu ids: e.g. 0  0,1,2, 0,2. use -1 for CPU')
parser.add_argument('--cuda', action='store_true', default=True, help='use cuda?')
parser.add_argument('--no_lsgan', action='store_true', help='do *not* use least square GAN, if false, use vanilla GAN')
parser.add_argument('--lambda1', type=float, default=10.0, help='weight for cycle loss (A -> B -> A)')
parser.add_argument('--lambda_g', type=float, default=5, help='-')
parser.add_argument('--beta1', type=float, default=0.5, help='momentum term of adam')
# parser.add_argument('--n_epoch', type=int, default=700, help='training epoch')
parser.add_argument('--fineSize', type=int, default=256, help='then crop to this size')
parser.add_argument('--loadSize', type=int, default=286, help='scale images to this size')
parser.add_argument('--infofile', default=[TRAIN_DATA_LIST,TEST_DATA_LIST], help='infofile')
# parser.add_argument('--infofile', default=['./data/list_train_F.txt', './data/list_test_F.txt'], help='infofile')
parser.add_argument('--batchSize', type=int, default=1, help='training batchSize')
parser.add_argument('--test_period', type=int, default=100, help='test_period')
parser.add_argument('--save_period', type=int, default=700, help='save_period')
parser.add_argument('--myGpu', default='0', help='GPU Number')

parser.add_argument('--zeroPadding', default=True, help='preprocess with method resizing or zero padding')
parser.add_argument('--target_resol', default=256, type=int, help='target resolution')
parser.add_argument('--from_resol', default=256, type=int, help='start resolution')
# parser.add_argument('--imgEveryResol', default=300, type=int, help='start resolution')
parser.add_argument('--imgEveryResol', default=187600, type=int, help='start resolution')
a = parser.parse_args()




# 数据级设定

# model
G_FILTER_NUM = a.ngf
D_FILTER_NUM = a.ndf
BATCH_SIZE = a.batch_size
NUM_WORK = 8
IN_CHANNEL = 3
OUT_CHANNNEL = 1
CHECKPOINT_PATH = a.checkpoint
# device
device_cuda = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
device_cpu = torch.device("cpu")
BATAS = (0.5,0.999)


