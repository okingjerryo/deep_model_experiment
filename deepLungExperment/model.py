import est_component as estimator
import tensorflow as tf
import collections  # 定义容器
from train_args import a
import train_args as args
from op import calculate_pt,tflog10
def logist_conv3d_model(features, labels, mode, params):
    inputX = features['ct_data']
    label = features['label']
    inputX = tf.expand_dims(inputX,axis=-1)

    if len(inputX.shape)<5:
        inputX = tf.expand_dims(inputX,axis=0)
    with tf.variable_scope('nodule_classifier'):
        logist = estimator.conv3d_classifier(inputX,mode=mode)
        tf.summary.histogram('predict',logist)
        class_id = tf.argmax(logist,axis=1)

    with tf.variable_scope('loss'):
        with tf.variable_scope('entropy'):
            entropy_loss = tf.losses.sparse_softmax_cross_entropy(labels=label,logits=logist)
            logist_softmax = tf.nn.softmax(logist)
            # predict output
            if mode == tf.estimator.ModeKeys.PREDICT:
                predictions = {
                    'class_id': class_id,
                    'label': label,
                    'logist': logist
                }
                return tf.estimator.EstimatorSpec(mode, predictions=predictions)
            one_hot = tf.one_hot(label, depth=3)
            pt = calculate_pt(logist_softmax,one_hot)
            label = tf.stop_gradient(label)
            focal_loss = -a.loss_alpha* tf.pow((1-pt),a.loss_gamma)*tflog10(pt+args.EPS)
            focal_loss_mean = tf.reduce_mean(focal_loss)
            entropy_loss_mean = tf.reduce_mean(entropy_loss)
        with tf.variable_scope('l2_regular'):
            train_vars = [var for var in tf.trainable_variables() if var.name.startswith("nodule_classifier")]
            l2_regularizer = tf.contrib.layers.l2_regularizer(args.L2_decay)
            l2_loss = tf.contrib.layers.apply_regularization(l2_regularizer,train_vars)
            fc_vars = tf.trainable_variables('nodule_classifier/fc_logist')
            tf.summary.histogram('fc_vars',fc_vars[0])

        total_loss = entropy_loss+l2_loss
        total_loss_mean = tf.reduce_mean(total_loss)
        accuracy = tf.metrics.accuracy(labels=label,
                                       predictions=class_id,
                                       name='acc_op')
        metics = {'accuracy':accuracy,
                  'entropy_loss':tf.metrics.mean(focal_loss_mean),
                  'l2_loss':tf.metrics.mean(l2_loss),
                  'total_loss':tf.metrics.mean(total_loss)
                  }


    tf.summary.scalar('entropy', focal_loss_mean)
    tf.summary.scalar('l2_loss', l2_loss)
    tf.summary.scalar('total_loss', total_loss_mean)
    tf.summary.scalar('accuracy', accuracy[1])
    # 控制台输出hook
    logging_hook = tf.train.LoggingTensorHook({'loss':total_loss_mean,'accuracy':accuracy[1],'predicted_class':class_id,'true_label':label}
                                              ,every_n_iter=1)
    if mode == tf.estimator.ModeKeys.EVAL:
        return tf.estimator.EstimatorSpec(
            mode,
            loss=total_loss,
            eval_metric_ops=metics,
            evaluation_hooks=[logging_hook]
        )
    with tf.variable_scope('optimazer'):
        optimazer = tf.train.AdamOptimizer(learning_rate=a.lr,beta1=a.adam_beta1)
        train_op = optimazer.minimize(loss=total_loss,
                                      global_step=tf.train.get_global_step(),
                                      var_list=train_vars)

    return tf.estimator.EstimatorSpec(
        mode,
        train_op=train_op,
        loss=total_loss,
        training_hooks=[logging_hook]
    )
