import tensorflow as tf
import collections
from op import *
import op
# normalize interface
E_Component = collections.namedtuple("E_Component", "output_conv,conv_detail")


# from cGAN
def cgan_unet_g(input_X, final_output_channals):
    layers = []

    # encoder_1: [batch, 256, 256, in_channels] => [batch, 128, 128, ngf]
    with tf.variable_scope("encoder_1"):
        output = gen_conv(input_X, a.ngf)
        layers.append(output)

    layer_specs = [
        a.ngf * 2,  # encoder_2: [batch, 128, 128, ngf] => [batch, 64, 64, ngf * 2]
        a.ngf * 4,  # encoder_3: [batch, 64, 64, ngf * 2] => [batch, 32, 32, ngf * 4]
        a.ngf * 8,  # encoder_4: [batch, 32, 32, ngf * 4] => [batch, 16, 16, ngf * 8]
        a.ngf * 8,  # encoder_5: [batch, 16, 16, ngf * 8] => [batch, 8, 8, ngf * 8]
        a.ngf * 8,  # encoder_6: [batch, 8, 8, ngf * 8] => [batch, 4, 4, ngf * 8]
        a.ngf * 8,  # encoder_7: [batch, 4, 4, ngf * 8] => [batch, 2, 2, ngf * 8]
        a.ngf * 8,  # encoder_8: [batch, 2, 2, ngf * 8] => [batch, 1, 1, ngf * 8]
    ]


    for out_channels in layer_specs:
        with tf.variable_scope("encoder_%d" % (len(layers) + 1)):
            rectified = lrelu(layers[-1], 0.2)
            # [batch, in_height, in_width, in_channels] => [batch, in_height/2, in_width/2, out_channels]
            convolved = gen_conv(rectified, out_channels)
            output = batchnorm(convolved)
            layers.append(output)

    layer_specs = [
        (a.ngf * 8, 0.5),  # decoder_8: [batch, 1, 1, ngf * 8] => [batch, 2, 2, ngf * 8 * 2]
        (a.ngf * 8, 0.5),  # decoder_7: [batch, 2, 2, ngf * 8 * 2] => [batch, 4, 4, ngf * 8 * 2]
        (a.ngf * 8, 0.5),  # decoder_6: [batch, 4, 4, ngf * 8 * 2] => [batch, 8, 8, ngf * 8 * 2]
        (a.ngf * 8, 0.0),  # decoder_5: [batch, 8, 8, ngf * 8 * 2] => [batch, 16, 16, ngf * 8 * 2]
        (a.ngf * 4, 0.0),  # decoder_4: [batch, 16, 16, ngf * 8 * 2] => [batch, 32, 32, ngf * 4 * 2]
        (a.ngf * 2, 0.0),  # decoder_3: [batch, 32, 32, ngf * 4 * 2] => [batch, 64, 64, ngf * 2 * 2]
        (a.ngf, 0.0),  # decoder_2: [batch, 64, 64, ngf * 2 * 2] => [batch, 128, 128, ngf * 2]
    ]

    num_encoder_layers = len(layers)
    for decoder_layer, (out_channels, dropout) in enumerate(layer_specs):
        skip_layer = num_encoder_layers - decoder_layer - 1
        with tf.variable_scope("decoder_%d" % (skip_layer + 1)):
            if decoder_layer == 0:
                # first decoder layer doesn't have skip connections
                # since it is directly connected to the skip_layer
                input = layers[-1]
            else:
                input = tf.concat([layers[-1], layers[skip_layer]], axis=3)

            rectified = tf.nn.relu(input)
            # [batch, in_height, in_width, in_channels] => [batch, in_height*2, in_width*2, out_channels]
            output = gen_deconv(rectified, out_channels)
            output = batchnorm(output)

            if dropout > 0.0:
                output = tf.nn.dropout(output, keep_prob=1 - dropout)

            layers.append(output)

    # decoder_1: [batch, 128, 128, ngf * 2] => [batch, 256, 256, generator_outputs_channels]
    with tf.variable_scope("decoder_1"):
        input = tf.concat([layers[-1], layers[0]], axis=3)
        rectified = tf.nn.relu(input)
        output = gen_deconv(rectified, final_output_channals)
        output = tf.tanh(output)
        layers.append(output)

    return E_Component(
        output_conv=layers[-1],
        conv_detail=layers
    )


# from cGAN
def cgan_patch_d(input_X, input_Y):
    n_layers = 3
    layers = []

    # 2x [batch, height, width, in_channels] => [batch, height, width, in_channels * 2]
    input = tf.concat([input_X, input_Y], axis=3)

    # layer_1: [batch, 256, 256, in_channels * 2] => [batch, 128, 128, ndf]
    with tf.variable_scope("layer_1"):
        convolved = discrim_conv(input, a.ndf, stride=2)
        rectified = lrelu(convolved, 0.2)
        layers.append(rectified)

    # layer_2: [batch, 128, 128, ndf] => [batch, 64, 64, ndf * 2]
    # layer_3: [batch, 64, 64, ndf * 2] => [batch, 32, 32, ndf * 4]
    # layer_4: [batch, 32, 32, ndf * 4] => [batch, 31, 31, ndf * 8]
    for i in range(n_layers):
        with tf.variable_scope("layer_%d" % (len(layers) + 1)):
            out_channels = a.ndf * min(2 ** (i + 1), 8)
            stride = 1 if i == n_layers - 1 else 2  # last layer here has stride 1
            convolved = discrim_conv(layers[-1], out_channels, stride=stride)
            normalized = batchnorm(convolved)
            rectified = lrelu(normalized, 0.2)
            layers.append(rectified)

    # layer_5: [batch, 31, 31, ndf * 8] => [batch, 30, 30, 1]
    with tf.variable_scope("layer_%d" % (len(layers) + 1)):
        convolved = discrim_conv(rectified, out_channels=1, stride=1)
        output = tf.sigmoid(convolved)
        layers.append(output)

    return E_Component(
        output_conv=layers[-1],
        conv_detail=layers
    )

def conv3d_classifier(input_X,mode):
    flag = False
    if mode == tf.estimator.ModeKeys.TRAIN:
        flag = True
    # 32*32*32*1 -> 32*32*32*64
    with tf.variable_scope('conv1'):
        # net = op.conv3d(input_X,1,64)
        net = tf.layers.conv3d(input_X,64,3,padding='same')
    # 32*32*32*64 -> 32*32*32*320
    with tf.variable_scope('conv2'):
        net = tf.layers.conv3d(net, 256, 3, padding='same',activation=tf.nn.relu)
    # 32*32*32*320 -> 16*16*16*320

        net = tf.layers.max_pooling3d(net,3,[2,2,2],padding='same')
    # 16*16*16*320 -> 16*16*16*672
    with tf.variable_scope('conv3'):
        net = tf.layers.conv3d(net, 512, 3, padding='same',activation=tf.nn.relu)
    # 16*16*16*672 -> 8*8*8*1528
        net = tf.layers.max_pooling3d(net,3,[2,2,2],padding='same')
    with tf.variable_scope('conv4'):
        net = tf.layers.conv3d(net, 1024, 3, padding='same',activation=tf.nn.relu)
    # 8*8*8*1528 -> 4*4*4*2560
        net = tf.layers.max_pooling3d(net,3,[2,2,2],padding='same')
    with tf.variable_scope('conv5'):
        net = tf.layers.conv3d(net, 2048, 3, padding='same',activation=tf.nn.relu)
    # 4*4*4*2560 -> 2560
        net = tf.layers.average_pooling3d(net,3,[4,4,4],padding='same')
    with tf.variable_scope('reshpe2fc'):
        net = tf.reshape(net,shape=[-1,2048])
        net = tf.layers.dropout(net,rate=0.5,training=flag)
    # fc2
    output = tf.layers.dense(net,3,name='fc_logist')
    return output
