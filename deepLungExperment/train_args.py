import tensorflow as tf
import argparse

# limit gpu mem useage
def gpu_option():
    gpu_options = tf.GPUOptions(allow_growth=True)
    return tf.ConfigProto(gpu_options=gpu_options)


# tf args
parser = argparse.ArgumentParser()
parser.add_argument("--mode", choices=["train", "test", "export"])

parser.add_argument("--checkpoint", default=None,
                    help="directory with checkpoint to resume training from or use for testing")
parser.add_argument("--save_freq", type=int, default=5000, help="save model every save_freq steps, 0 to disable")

parser.add_argument("--display_freq", type=int, default=0,
                    help="write current training images every display_freq steps")

parser.add_argument("--image_size", default=512, type=int, help="input img size")
parser.add_argument("--scale_size", type=int, default=286, help="scale images to this size before cropping to 256x256")
parser.add_argument("--img_crop_size", default=256, type=int, help="crop target size")

parser.add_argument("--lr", type=float, default=0.0001, help="initial learning rate for adam")
parser.add_argument("--adam_beta1", type=float, default=0.5, help="initial learning rate for adam")

parser.add_argument("--batch_size", type=int, default=40, help="number of images in batch")
parser.add_argument("--max_epochs", type=int, help="number of training epochs")
# hiper parm
parser.add_argument("--loss_alpha", type=float, default=0.45 , help="focal loss alpha")
parser.add_argument("--loss_gamma", type=int, default=2,help="focal loss gamma")
# dataset options
parser.add_argument("--input_data_type",default="tfrecord",choices=["image_list","tfrecord"]
                    ,help="data input type when init data handle")
parser.add_argument("--path_to_record", default="/home/data_disk/uryuo/db/LIDC-IDRI/tfrecord",
                    help="the path navgate to tfrecord dir")

parser.add_argument("--record_train_name", default="LIDC_train", help="train tfrecord file name")
parser.add_argument("--record_vaild_name", default="LIDC_vaild", help="vaild tfrecord file name")
parser.add_argument("--record_test_name", default="LIDC_test", help="test tfrecord file name")
a = parser.parse_args()
# 数据集相关
PATH2RECORD = a.path_to_record  # 将数据集转 tfrecord 位置

# tfrecord 命名
RECORD_TRAIN_NAME = a.record_train_name
RECORD_VAILD_NAME = a.record_vaild_name
RECORD_TEST_NAME = a.record_test_name
# record验证图片存储LUJING
RECORD_ALIABLE_PATH = 'resource/'
# network
EPS = 1e-12
L2_decay = 1e-4 # l2 正则化 的decay
TRAIN_BATCH = a.batch_size
# 裁剪block大小
CROP_BLOCK_SIZE = 32
# dataSet api 参数
NUM_PARALLEL_READERS = 12
NUM_PARALLEL_CALLS = 12
NUM_PREFATCH = 500
# running
STEPS = 6400
W_DTYPE = tf.float32