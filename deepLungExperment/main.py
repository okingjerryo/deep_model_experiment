import dataset
import train_args
import model
import train_args as args
import tensorflow as tf
import deepLungExperment.op as op


# trainning pipeline
def training_input_fx():
    data_op = dataset._init_handle(dataset.TRAIN_DATASET_TYPE)
    return data_op['data_set']
def eval_input_fx():
    data_op = dataset._init_handle(dataset.VAILD_DATASET_TYPE)
    return data_op['data_set']
def test_input_fx():
    data_op = dataset._init_handle(dataset.TEST_DATASET_TYPE)
    return data_op['data_set']
def program_estimator():
    checkpointing_config = tf.estimator.RunConfig(
        save_checkpoints_secs=10 * 60,  # 10min save
        keep_checkpoint_max=3,  # 存30min内的 占用2G附近
        save_summary_steps=10,
        log_step_count_steps=50
    )
    return tf.estimator.Estimator(
        model_fn=model.logist_conv3d_model,
        model_dir="model_avgpool/",
        config=checkpointing_config
    )


def write_to_file(result, path):
    prdict_line = ''
    label_line = ''
    path_name = path.split('.')
    logist_name = path_name[0] + '_logist.' + path_name[1]
    with open(logist_name, 'w') as f:
        for one_re in result:
            prdict = one_re['class_id']
            label = one_re['label']
            logist = one_re['logist']
            prdict_line+=str(prdict)+' '
            label_line+=str(label)+' '
            linestr = str(logist[0]) + ' ' + str(logist[1]) + ' ' + str(logist[2]) + '\n'
            f.write(linestr)
    prdict_line = prdict_line[:-1]+'\n'
    label_line = label_line[:-1]+'\n'
    with open(path,'w') as f:
        f.writelines([prdict_line,label_line])





def main(argv):
    estimator = program_estimator()
    with tf.Session(config=args.gpu_option()):
        # for i in range(100):
        #     print("==============epoch {} trainning===============".format(i))
        #     estimator.train(
        #         input_fn=training_input_fx,
        #         steps=args.STEPS
        #     )
        #     estimator.evaluate(
        #         input_fn=eval_input_fx,
        #         steps=10
        #     )
        print("==============perdicting===============")
        result = list(estimator.predict(input_fn=test_input_fx))
        print("==============write result to file===============")
        write_to_file(result, 'focal_avg_result.txt')


if __name__ == '__main__':
    # tf.logging.set_verbosity(tf.logging.DEBUG)
    # tf.app.run(main)
    op.socast_result_file('focal_avg_result.txt')
