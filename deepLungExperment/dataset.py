import tensorflow as tf
import op
import train_args
from tqdm import tqdm
import cv2
from train_args import a
import numpy as np
import os
import random
import nodule as nodule
import nodule_structs as nodule_s
import glob

# 用于识别不同的操作图片集
TRAIN_DATASET_TYPE = 0
VAILD_DATASET_TYPE = 1
TEST_DATASET_TYPE = 2
# 可以feed的dataset handle
DATA_HANDEL = tf.placeholder(dtype=tf.string, shape=[], name='dataset_handle')



def _img_effect_process(img_mat, ram_seed=tf.set_random_seed(op.get_timestamp_now())):

    return img_mat


def _get_tfrecord_structure():
    """
        定义tfrecord 以及trainning set features的结构
    :return: features 给tf.parse_single_example用
    """
    return {
        'pic_block': tf.FixedLenFeature((), tf.string, default_value=''),
        'class_label': tf.FixedLenFeature((),tf.string),
        'start_pix': tf.FixedLenFeature((),dtype=tf.string),
        'end_pix': tf.FixedLenFeature((),dtype=tf.string),
        'block_size': tf.FixedLenFeature((),dtype=tf.string),
        'label_length': tf.FixedLenFeature((),dtype=tf.int64)
    }

def _bytes_feature(bytes):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[bytes]))

def _int64_features(bytes):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[bytes]))
def _int64_list_features(list_bytes):
    return  tf.train.Feature(int64_list=tf.train.Int64List(value=[bytes for bytes in list_bytes]))


def __data_effect(pic_block):
    # 使用 min_max算法归一化
    with tf.variable_scope('min_max_regular'):
        max_data = 4000
        min_data = -2000
        pic_block = (pic_block - min_data)/(max_data-min_data)
        pic_block = pic_block*2-1
    return pic_block


def _read_img_process(img_example,data_type):
    features = None
    # 注意注意！ features中的key value对一定要与tfrecord中的k-v相关对应！
    noise_img = None
    gt_img = None
    if data_type is 'image_list':
        pass
    elif data_type is 'tfrecord':
        features = _get_tfrecord_structure()
        parse_features = tf.parse_single_example(img_example, features)
        # raw
        with tf.variable_scope('data_read'):
            pic_block_raw = tf.decode_raw(parse_features['pic_block'],tf.float64)
            pic_block_raw = tf.cast(pic_block_raw,dtype=tf.float32)
            label_list_raw = tf.decode_raw(parse_features['class_label'],tf.int64)
            start_pix_raw = tf.decode_raw(parse_features['start_pix'],tf.int64)
            end_pix_raw = tf.decode_raw(parse_features['end_pix'],tf.int64)
            block_size_raw = tf.decode_raw(parse_features['block_size'],tf.int64)
            label_length = tf.cast(parse_features['label_length'],tf.int32)
        # block struct
        with tf.variable_scope('data_struct_rebuild'):
            label_list = None
            l = train_args.CROP_BLOCK_SIZE
            pic_block = tf.reshape(pic_block_raw,[l,l,l],name='pic_block_reshape')
            pic_block = tf.transpose(pic_block,perm=[2,0,1])    #从[h,w,d]转为[d,h,w]
            pic_block = __data_effect(pic_block)
            label_list = tf.reshape(label_list_raw,shape=[label_length],name='label_list_reshape')
            start_plist = tf.reshape(start_pix_raw,shape=[label_length,3],name='start_list_reshape')
            end_plist = tf.reshape(end_pix_raw,shape=[label_length,3],name='end_list_reshape')
            block_list = tf.reshape(block_size_raw,shape=[label_length],name='block_list_reshape')
        # 用于本次任务的检测
            label = tf.reduce_max(label_list)   # 取得是有最大肿瘤的情况
            # label = tf.one_hot(label,3,on_value=0.1,off_value=0.9)
    # 这里关联着inputFun
        return {
            'ct_data':pic_block,
            'label':label
        }

# 组装数据
def __reading_data(now_pix,crop_l,label_list,data_block) -> dict:

    # 组装当前区域
    this_zone = nodule_s.nodule()
    this_zone.nodule_pix_s.row= now_pix[0]
    this_zone.nodule_pix_s.col = now_pix[1]
    this_zone.nodule_pix_s.depth = now_pix[2]
    this_zone.nodule_pix_e.row = now_pix[0]+crop_l
    this_zone.nodule_pix_e.col = now_pix[1]+crop_l
    this_zone.nodule_pix_e.depth = now_pix[2]+crop_l
    domain_label = []
    if len(domain_label)!=0:
        domain_label.clear()
    for label in label_list:
        # 检测是否有肿瘤
        detect_n = label.detect_nodule_inblock(this_zone)
        if detect_n.nodule_class==0:
            continue
        elif detect_n.nodule_class == 1:
            detect_n.nodule_pix_s = label.nodule_pix_s
            detect_n.nodule_pix_e = label.nodule_pix_e
        domain_label.append(detect_n)

    data_raw = data_block.tostring()
    return {
        'data_block_raw':data_raw,
        'label_info':domain_label
    }
def write_img_freamwork(dir_list,dataset_type,record_path = None):
    print('file convert start')
    if record_path is None:
        if dataset_type == TRAIN_DATASET_TYPE:
            record_path = train_args.PATH2RECORD +'/'+ train_args.RECORD_TRAIN_NAME+'.tfrecord'
        elif dataset_type == TEST_DATASET_TYPE:
            record_path = train_args.PATH2RECORD +'/'+ a.record_test_name+'.tfrecord'
        elif dataset_type == VAILD_DATASET_TYPE:
            record_path = train_args.PATH2RECORD +'/'+ a.record_vaild_name+'.tfrecord'

    writer = tf.python_io.TFRecordWriter(record_path)
    counter = 1
    total_data = 0
    for path in tqdm(dir_list):
        try:
            if counter%100 == 0:
                index = int(counter / 100)
                i_str = str(index)

                if index<10:
                    i_str = '0'+i_str
                # new record file
                writer.close()

                # reconstract tfrecord name - train
                record_path = train_args.PATH2RECORD +'/'+ train_args.RECORD_TRAIN_NAME+i_str+'.tfrecord'
                writer = tf.python_io.TFRecordWriter(record_path)

            this_nodule_sum = __write_img_record(path,writer,only_nodule=True)
            total_data +=this_nodule_sum
            print("nodule total count: {}, all total:{}".format(this_nodule_sum,total_data))
            counter+=1
        except KeyError:
            print('error path: {}\n'.format(path))
            continue
        except IndexError:
            continue
    writer.close()

    print('save complete,path:', record_path)

def __crop_and_write(writer,crop_x,crop_y,crop_z,l,label_list,data_block,only_no_nodule = False):
    feature = __reading_data([crop_x, crop_y, crop_z], l, label_list, data_block)
    t_label_list = feature['label_info']  # 空的时候就是没有肿瘤
    class_list = []
    start_p_list = []
    end_p_list = []
    block_size = []
    # 注意b大小为0 时候 证明为小的肿瘤 只有一个pix
    for label in t_label_list:
        class_list.append(label.nodule_class)
        start_p_list.append(label.get_pix_ind('start'))
        end_p_list.append(label.get_pix_ind('end'))
        block_size.append(label.nodule_d)
    if len(t_label_list) is 0:
        class_list = [0]
        start_p_list = [[0,0,0]]
        end_p_list = [[0,0,0]]
        block_size = [0]
    else:
        if only_no_nodule:
            t_label_list.clear()
            return False
    example = tf.train.Example(features=tf.train.Features(feature={
        'pic_block': _bytes_feature(feature['data_block_raw']),
        'class_label': _bytes_feature(np.array(class_list, dtype=np.int64).tostring()),  # 记住要deraw恢复为数组
        'start_pix': _bytes_feature(np.array(start_p_list).tostring()),
        'end_pix': _bytes_feature(np.array(end_p_list).tostring()),
        'block_size': _bytes_feature(np.array(block_size).tostring()),
        'label_length': _int64_features(len(class_list))
    }))
    writer.write(example.SerializeToString())
    return True

def __nodule_specify_write(label,full_block,writer,label_list):
    def write_block(block,t_x,t_y,t_z,l):
        __crop_and_write(writer, t_x, t_y, t_z, l, label_list, block)
        # 翻转(x,y,z轴各两个方向)
        dir_block = op.rot_3d(block, 0, 1)
        __crop_and_write(writer, t_x, t_y, t_z, l, label_list, dir_block)
        dir_block = op.rot_3d(block, 0, -1)
        __crop_and_write(writer, t_x, t_y, t_z, l, label_list, dir_block)
        dir_block = op.rot_3d(block, 1, 1)
        __crop_and_write(writer, t_x, t_y, t_z, l, label_list, dir_block)
        dir_block = op.rot_3d(block, 1, -1)
        __crop_and_write(writer, t_x, t_y, t_z, l, label_list, dir_block)
        dir_block = op.rot_3d(block, 2, 1)
        __crop_and_write(writer, t_x, t_y, t_z, l, label_list, dir_block)
        dir_block = op.rot_3d(block, 2, -1)
        __crop_and_write(writer, t_x, t_y, t_z, l, label_list, dir_block)

    start_pix = label.nodule_pix_s
    end_pix = label.nodule_pix_e
    l = train_args.CROP_BLOCK_SIZE
    counter = 0
    # 密集采样(n/2)
    for t_x in range(start_pix.row,end_pix.row,8):
        for t_y in range(start_pix.col,end_pix.col,8):
            for t_z in range(start_pix.depth,end_pix.depth,8):
                t_x = min(full_block.shape[0]-l,t_x)
                t_y = min(full_block.shape[1]-l,t_y)
                t_z = min(full_block.shape[2]-l,t_z)
                this_block = full_block[t_x:t_x+l,t_y:t_y+l,t_z:t_z+l]
                write_block(this_block,t_x,t_y,t_z,l)
                counter += 7

    if counter is 0:
        # 此处为小肿瘤，只有一个 采样7个
        t_x = min(full_block.shape[0] - l, start_pix.row)
        t_y = min(full_block.shape[1] - l, start_pix.col)
        t_z = min(full_block.shape[2] - l, start_pix.depth)
        this_block = full_block[t_x:t_x + l, t_y:t_y + l,t_z:t_z + l]
        write_block(this_block,t_x,t_y,t_z,l)
        counter += 7
    return counter




def __write_img_record(dir_path,writer,only_nodule = False,max_count = 0):

    # 整个文件和label列表
    full_pix_data,label_list = nodule.load_data_dir(dir_path)
    data_shape = full_pix_data.shape
    l = train_args.CROP_BLOCK_SIZE
    counter = 0

    if only_nodule is False:
        for s_x in range(0,data_shape[0],16):
            if counter > max_count and max_count>0:
                break
            for s_y in range(0,data_shape[1],16):
                if counter > max_count and max_count>0:
                    break
                for s_z in range(0,data_shape[2],16):
                    if counter>max_count and max_count>0:
                        break
                    crop_x = min(s_x,data_shape[0]-l)
                    crop_y = min(s_y,data_shape[1]-l)
                    crop_z = min(s_z,data_shape[2]-l)
                    data_block = full_pix_data[crop_x:crop_x+l,crop_y:crop_y+l,crop_z:crop_z+l]
                    __crop_and_write(writer,crop_x,crop_y,crop_z,l,label_list,data_block)
                    counter+=1
    else:
            # 先查出肿瘤，
            for label in label_list:
                if label.nodule_class is 0:
                    continue    # 暂时等候
                count = __nodule_specify_write(label,full_pix_data,writer,label_list)   # 这块区域密集采样
                counter+=count
            max_normal = max(counter,10)
            for i in range(max_normal):
                # 取得肿瘤后再取普通样本
                if counter>max_count and max_count != 0:
                    break
                # 随机取相同数量的肿瘤切片位置
                no_nodule_flag = False
                for _ in range(50):
                    if no_nodule_flag:
                        break
                    crop_x = random.randint(0, data_shape[0]-l-1)
                    crop_y = random.randint(0, data_shape[1]-l-1)
                    crop_z = random.randint(0, data_shape[2]-l-1)
                    data_block = full_pix_data[crop_x:crop_x+l,crop_y:crop_y+l,crop_z:crop_z+l]
                    no_nodule_flag = __crop_and_write(writer,crop_x,crop_y,crop_z,l,label_list,data_block,True)# 强制写入无肿瘤
                counter+=1

            return counter







def _init_handle(dataset_type,record_path=None, data_type=None,get_itor = None):
    '''
        初始化DataSet Handle
    :return:
    '''
    # 默认参数
    if get_itor is None:
        get_itor = False
    if data_type is None:
        data_type = a.input_data_type

    if dataset_type is TRAIN_DATASET_TYPE:
        if record_path is None:
             record_path = tf.data.Dataset.list_files(train_args.PATH2RECORD + '/' + train_args.RECORD_TRAIN_NAME + '*.tfrecord')
    elif dataset_type is VAILD_DATASET_TYPE:
        if record_path is None:
            record_path = tf.data.Dataset.list_files(train_args.PATH2RECORD + '/' + train_args.RECORD_VAILD_NAME + '*.tfrecord')
    elif dataset_type is TEST_DATASET_TYPE:
        if record_path is None:
            record_path = tf.data.Dataset.list_files(train_args.PATH2RECORD + '/' + train_args.RECORD_TEST_NAME + '*.tfrecord')
    with tf.variable_scope('dataset_builder'):
        train_dataset = record_path.interleave(tf.data.TFRecordDataset, cycle_length=train_args.NUM_PARALLEL_READERS)
        train_dataset = train_dataset.map(
                map_func=lambda x: _read_img_process(x,'tfrecord'),
                num_parallel_calls=train_args.NUM_PARALLEL_CALLS
            )
        if dataset_type!=TEST_DATASET_TYPE:
            train_dataset = train_dataset.shuffle(buffer_size=1000)
            train_dataset = train_dataset.repeat()  # 组成一个epoch
        train_dataset = train_dataset.batch(train_args.TRAIN_BATCH)
        # 优化 预处理pipeline
        train_dataset = train_dataset.prefetch(buffer_size=train_args.NUM_PREFATCH)
        if get_itor is False:
            return {
                'data_set': train_dataset
            }
    with tf.variable_scope('itor_builder'):
        # init handle
        iterator = tf.data.Iterator.from_string_handle(DATA_HANDEL, train_dataset.output_types,
                                                       train_dataset.output_shapes)
        get_next_batch = iterator.get_next()

        train_itor = train_dataset.make_initializable_iterator()
    return {
        'train_itor': train_itor,
        'get_next_batch': get_next_batch,
        'data_set': train_dataset
    }

def vaildate_data_useable(noise_img_list=None, input_type=None, action_type=None):
    '''
    获取一个batch数据，并取出头第一对图片。看其是否有效
    :return: null
    '''
    # 默认参数区
    if input_type is None:
        input_type = a.input_data_type
    if action_type is None:
        action_type = TRAIN_DATASET_TYPE

    data_op = _init_handle(dataset_type=action_type,data_type=input_type,get_itor=True)

    get_batch = data_op['get_next_batch']
    train_itor = data_op['train_itor']

    with tf.Session(config=train_args.gpu_option()) as sess:

        print('vailding data...')
        data_handle = sess.run(train_itor.string_handle())

        for i in range(2):
            train_feed = {DATA_HANDEL: data_handle}
            sess.run(train_itor.initializer, feed_dict=train_feed)
            batch_feature = sess.run(get_batch, feed_dict=train_feed)
            print(batch_feature['label'])


# brige feature to estimator
def get_feature_columns():
    feature_columns = [tf.feature_column.numeric_column(key="noise_image")]
    return feature_columns

def resize_resource():
    namelist = glob.glob('/home/uryuo/Project/deep_model_experiment/face2sketchExperment/resource/resource/*/*.*')
    org_list = []
    ske_list = []
    gen_list = []
    for path in namelist:
        if '/gen/' in path:
            gen_list.append(path)
        elif '/org/' in path:
            org_list.append(path)
        elif '/ske/' in path:
            ske_list.append(path)

    output = np.zeros(shape=[250,3*200,3])
    for i in range(len(org_list)):
        org_img=cv2.imread(org_list[i])[:250,:200,:]
        ske_img=cv2.imread(ske_list[i],cv2.IMREAD_GRAYSCALE)[:250,:200]
        gen_img=cv2.imread(gen_list[i],cv2.IMREAD_GRAYSCALE)[:250,:200]
        output[:,:200] = org_img
        output[:,200:200*2] = cv2.cvtColor(ske_img,cv2.COLOR_GRAY2RGB)
        output[:,200*2:] = cv2.cvtColor(gen_img,cv2.COLOR_GRAY2RGB)

        name = org_list[i].split('/')[-1]
        name = name.split('.')[0]

        # crop
        cv2.imwrite('resource/{}.jpg'.format(name),output)
if __name__ == '__main__':
    # rootdir = '/home/data_disk/uryuo/db/LIDC-IDRI/DOI'
    # list = os.listdir(rootdir)
    # dir_list = []
    #
    # for line in list:
    #     filepath = os.path.join(rootdir, line)
    #     if os.path.isdir(filepath):
    #         dir_list.append(filepath+'/')
    # start = round(len(dir_list)*0.7)
    # vaild = round(len(dir_list)*0.9)
    # end = len(dir_list)
    # itor = None
    # dir_list = dir_list[vaild:end]
    # write_img_freamwork(dir_list,TEST_DATASET_TYPE)
    # _init_handle(TRAIN_DATASET_TYPE)
    vaildate_data_useable(input_type='tfrecord',action_type=TEST_DATASET_TYPE)
