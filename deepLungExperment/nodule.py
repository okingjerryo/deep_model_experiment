"""
Modified from https://github.com/zhwhong/lidc_nodule_detection/
"""
import os
import argparse
import pydicom as dicom
import pickle
from annotation import parse_dir
from annotation import find_all_files
from tqdm import tqdm
import numpy as np

import collections
import nodule_structs as nodules

DATA_PATH = '/home/uryuo/db/LIDC-IDRI/DOI/LIDC-IDRI-0001/'
OUT_PATH = 'resource/lidc_mat/'


def build_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--srcpath', type=str, default=DATA_PATH, help='the dcm image path')
    parser.add_argument('-d', '--dstpath', type=str, default=OUT_PATH, help='the output path')
    return parser


def constract_array_from_raw(data : bytes,shape):
    '''
        将byte型数据转成np数组，
    :param data: byte型数据
    :param shape: 目标数组的shape
    :return: 转换后的np数组
    '''
    np_raw = np.frombuffer(data,dtype = np.int16)   # dicom 数据类型为int16灰度 需要后期转为0~255
    np_raw = np.reshape(np_raw,newshape=shape)
    return np_raw


# 数据解析并读取
def annotation(srcpath, dstpath):
    parse_dir(srcpath, dstpath)
    name = srcpath.split('/')[-2]
    f = open(os.path.join(dstpath, name+'_annotation_flatten.pkl'),'rb')
    info = pickle.load(f)
    f.close()

    slicers = 0
    rows = 512
    cols = 512
    data_type = 4
    slice_thickness = 0
    pixel_spacing_x = 0
    pixel_spacing_y = 0
    flag = False

    pixel_data_list={}  # 刚解析出来的数据
    instance_list = {}
    for f_dcm in find_all_files(srcpath, suffix='.dcm'):
        dcm = dicom.read_file(f_dcm)
        if dcm[0x0008,0x0060].value != 'CT':
            continue
        slicers += 1

        if not flag:
            flag = True
            rows = dcm[0x0028, 0x0010].value
            cols = dcm[0x0028, 0x0011].value
            slice_thickness = dcm[0x0018, 0x0050].value
            pixel_spacing_x, pixel_spacing_y = dcm[0x0028, 0x0030].value

        sop_uid = dcm[0x08, 0x18].value
        z = dcm[0x0020, 0x0013].value  # instance number
        data = dcm[0x7fe0,0x0010].value
        data_arr = constract_array_from_raw(data,[rows,cols,1])
        instance_list[z] = sop_uid
        pixel_data_list[z] = data_arr
    # sort by z position
    len_z = len(instance_list)
    instances = []
    ct_data_block = np.zeros(shape=[rows,cols,len_z])
    # 数组
    for i in sorted(instance_list.keys(), reverse=True):
        instances.append(instance_list[i])
        ct_data_block[:,:,i-1] = np.reshape(pixel_data_list[i],[rows,cols])

    map_uid = {}

    for i in range(len(instances)):
        map_uid[instances[i]] = i

    label_list = []
    # construct labelblock
    for nodule in info['nodules']:
        t_nodule = nodules.nodule()
        t_nodule.nodule_class = 2
        for point in nodule:
            b_nodule_list = []
            for i in point['pixels']:
                nodule_p = nodules.nodule_pix()
                nodule_p.row = i[0]
                nodule_p.col = i[1]
                nodule_p.depth = map_uid[point['sop_uid']]
                b_nodule_list.append(nodule_p)

            t_nodule.input_bignode_by_list(b_nodule_list)
        label_list.append(t_nodule)

    for nodule in info['small_nodules']:
        t_nodule = nodules.nodule()
        t_nodule.nodule_class = 1
        for point in nodule:
            b_nodule_list = []
            for i in point['pixels']:
                nodule_p = nodules.nodule_pix()
                nodule_p.row = i[0]
                nodule_p.col = i[1]
                nodule_p.depth = map_uid[point['sop_uid']]
                b_nodule_list.append(nodule_p)

            t_nodule.input_bignode_by_list(b_nodule_list)
        label_list.append(t_nodule)
    
    for nodule in info['non_nodules']:
        t_nodule = nodules.nodule()
        t_nodule.nodule_class = 0
        for point in nodule:
            b_nodule_list = []
            for i in point['pixels']:
                nodule_p = nodules.nodule_pix()
                nodule_p.row = i[0]
                nodule_p.col = i[1]
                nodule_p.depth = map_uid[point['sop_uid']]
                b_nodule_list.append(nodule_p)

            t_nodule.input_bignode_by_list(b_nodule_list)
        label_list.append(t_nodule)
    return (ct_data_block,label_list)

def load_data_dir(dirpath):
    parser = build_parser()
    options = parser.parse_args()
    if dirpath != None:
        options.srcpath = dirpath
    assert os.path.exists(options.srcpath)

    if not os.path.exists(options.dstpath):
        os.mkdir(options.dstpath)

    return annotation(options.srcpath, options.dstpath)


if __name__ == '__main__':
    rootdir = '/home/uryuo/db/LIDC-IDRI/DOI'
    list = os.listdir(rootdir)
    dir_list = []
    for line in list:
        filepath = os.path.join(rootdir, line)
        if os.path.isdir(filepath):
            dir_list.append(filepath+'/')
    for path in tqdm(dir_list):
        try:
            load_data_dir(path)
        except KeyError:
            print(path)
            continue
